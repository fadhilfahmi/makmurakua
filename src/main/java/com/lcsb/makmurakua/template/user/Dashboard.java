/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.template.user;

import com.lcsb.makmurakua.dao.administration.om.UserDAO;
import com.lcsb.makmurakua.util.model.LoginProfile;

/**
 *
 * @author fadhilfahmi
 */
public class Dashboard {
    
    public static String viewDashboard(LoginProfile log) throws Exception{
        
        String renderDashboard = "<script type=\"text/javascript\" charset=\"utf-8\">\n" +
                                "\n" +
                                "    $(document).ready(function () {\n" +
                                "\n";
                                if (UserDAO.getDashboardAccess(log, log.getUserID(), "020903")) {
                renderDashboard += "        $.ajax({\n" +
                                "            url: \"PathController?moduleid=020903&process=summaryinvoice\",\n" +
                                "            success: function (result) {\n" +
                                "                $('#datatable-render-inv').empty().html(result).hide().fadeIn(300);\n" +
                                "            }\n" +
                                "        });\n";
                                }
                                if (UserDAO.getDashboardAccess(log, log.getUserID(), "020908")) {
                renderDashboard += "        $.ajax({\n" +
                                "            url: \"PathController?moduleid=020908&process=summarydnote\",\n" +
                                "            success: function (result) {\n" +
                                "                $('#datatable-render-dnote').empty().html(result).hide().fadeIn(300);\n" +
                                "            }\n" +
                                "        });\n";
                                }if (UserDAO.getDashboardAccess(log, log.getUserID(), "020909")) {
                renderDashboard += "        $.ajax({\n" +
                                "            url: \"PathController?moduleid=020909&process=summarycnote\",\n" +
                                "            success: function (result) {\n" +
                                "                $('#datatable-render-cnote').empty().html(result).hide().fadeIn(300);\n" +
                                "            }\n" +
                                "        });\n";
                                }
                renderDashboard +=  "    });\n" +
                                "\n" +
                                "</script>\n" +
                                "<div class=\"row\">\n";
                                if (UserDAO.getDashboardAccess(log, log.getUserID(), "020903")) {
                renderDashboard += "    <div class=\"col-lg-6\">\n" +
                                "        <div class=\"x_panel\">\n" +
                                "            <div class=\"x_title\">\n" +
                                "                <h2>Sales Invoice <small>Summary of Transaction</small></h2>\n" +
                                "                <ul class=\"nav navbar-right panel_toolbox\">\n" +
                                "                    <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n" +
                                "                    </li>\n" +
                                "                    <li class=\"dropdown\">\n" +
                                "                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n" +
                                "                        <ul class=\"dropdown-menu\" role=\"menu\">\n" +
                                "                            <li><a href=\"#\">Settings 1</a>\n" +
                                "                            </li>\n" +
                                "                            <li><a href=\"#\">Settings 2</a>\n" +
                                "                            </li>\n" +
                                "                        </ul>\n" +
                                "                    </li>\n" +
                                "                    <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n" +
                                "                    </li>\n" +
                                "                </ul>\n" +
                                "                <div class=\"clearfix\"></div>\n" +
                                "            </div>\n" +
                                "\n" +
                                "            <div class=\"dataTable_wrapper\" id=\"datatable-render-inv\"> \n" +
                                "                <div style=\"width:100px; margin:0 auto; color:#000 !important;\"><span style=\"color:#000 !important;\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></span><span class=\"sr-only\">Loading...</span></div>\n" +
                                "            </div>\n" +
                                "\n" +
                                "\n" +
                                "        </div>\n" +
                                "    </div>\n";
                                }if (UserDAO.getDashboardAccess(log, log.getUserID(), "020908")) {
                   renderDashboard +="    <div class=\"col-lg-6\">\n" +
                                "        <div class=\"x_panel\">\n" +
                                "            <div class=\"x_title\">\n" +
                                "                <h2>Sales Debit Note <small>Summary of Transaction</small></h2>\n" +
                                "                <ul class=\"nav navbar-right panel_toolbox\">\n" +
                                "                    <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n" +
                                "                    </li>\n" +
                                "                    <li class=\"dropdown\">\n" +
                                "                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n" +
                                "                        <ul class=\"dropdown-menu\" role=\"menu\">\n" +
                                "                            <li><a href=\"#\">Settings 1</a>\n" +
                                "                            </li>\n" +
                                "                            <li><a href=\"#\">Settings 2</a>\n" +
                                "                            </li>\n" +
                                "                        </ul>\n" +
                                "                    </li>\n" +
                                "                    <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n" +
                                "                    </li>\n" +
                                "                </ul>\n" +
                                "                <div class=\"clearfix\"></div>\n" +
                                "            </div>\n" +
                                "\n" +
                                "            <div class=\"dataTable_wrapper\" id=\"datatable-render-dnote\"> \n" +
                                "                <div style=\"width:100px; margin:0 auto; color:#000 !important;\"><span style=\"color:#000 !important;\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></span><span class=\"sr-only\">Loading...</span></div>\n" +
                                "            </div>\n" +
                                "        </div>\n" +
                                "    </div>\n" +
                                "</div>\n" +
                                "<div class=\"row\">\n";
                                }if (UserDAO.getDashboardAccess(log, log.getUserID(), "020909")) {
                renderDashboard += "    <div class=\"col-lg-6\">\n" +
                                "        <div class=\"x_panel\">\n" +
                                "            <div class=\"x_title\">\n" +
                                "                <h2>Sales Credit Note <small>Summary of Transaction</small></h2>\n" +
                                "                <ul class=\"nav navbar-right panel_toolbox\">\n" +
                                "                    <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>\n" +
                                "                    </li>\n" +
                                "                    <li class=\"dropdown\">\n" +
                                "                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>\n" +
                                "                        <ul class=\"dropdown-menu\" role=\"menu\">\n" +
                                "                            <li><a href=\"#\">Settings 1</a>\n" +
                                "                            </li>\n" +
                                "                            <li><a href=\"#\">Settings 2</a>\n" +
                                "                            </li>\n" +
                                "                        </ul>\n" +
                                "                    </li>\n" +
                                "                    <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>\n" +
                                "                    </li>\n" +
                                "                </ul>\n" +
                                "                <div class=\"clearfix\"></div>\n" +
                                "            </div>\n" +
                                "\n" +
                                "            <div class=\"dataTable_wrapper\" id=\"datatable-render-cnote\"> \n" +
                                "                <div style=\"width:100px; margin:0 auto; color:#000 !important;\"><span style=\"color:#000 !important;\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></span><span class=\"sr-only\">Loading...</span></div>\n" +
                                "            </div>\n" +
                                "\n" +
                                "\n" +
                                "        </div>\n" +
                                "    </div>\n";
                                }
                renderDashboard += "</div>";
        
        return renderDashboard;
    }
    
}
