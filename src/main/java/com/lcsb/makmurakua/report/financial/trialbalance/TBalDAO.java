/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.trialbalance;

import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TBalDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020112");
        mod.setModuleDesc("Financial Report");
        mod.setMainTable("gl_jv");
        mod.setReferID_Master("JVrefno");
        return mod;

    }
    
    
    public static TBalMaster getMaster(LoginProfile log, TBParam prm) throws Exception{
        
        TBalMaster tb = new TBalMaster();
        
        tb.setListCore(TBalDAO.getListCode(log));
        tb.setTotalDebit(getTotal(log, "debit"));
        tb.setTotalCredit(getTotal(log, "credit"));
        tb.setTbtitle("TRIAL BALANCE FOR PERIOD "+prm.getPeriod()+" YEAR "+prm.getYear());
        
        return tb;
        
    }

    public static void generateTrialBalance(LoginProfile log, TBParam prm) throws Exception {

        String table1 = "";
        String table2 = "";
        String rule1 = "";

        switch (prm.getOtype()) {
            case "Opening Balance":
                table1 = "gl_openingbalance";
                table2 = "gl_closingbalance";
                break;
            case "This Month":
                table1 = "gl_openingbalance";
                table2 = "gl_openingbalance_detail";
                rule1 = "and t1.coacode not like  '3103%'";
                break;
            case "To Date":
                table1 = "gl_closingbalance";
                table2 = "gl_closingbalance";
                break;
            default:
                break;
        }

        deleteTrial(log);//    -- 1
        ifRuleTrue(log,prm);//  -- 2
        defaultTrialDetail(log, prm, table1, rule1);

//        GListMaster gl = new GListMaster();
//        
//        gl.setListCore(GListDAO.getListCode(prm));
//        gl.setTotalDebit(getTotal("debit", prm));
//        gl.setTotalCredit(getTotal("credit", prm));
//        gl.setGltitle("GENERAL LEDGER LISTING FOR PERIOD "+prm.getFperiod()+"-"+prm.getFyear()+" TO PERIOD "+prm.getTperiod()+"-"+prm.getTyear()+"");
//        gl.setGlcodetocode("FROM ACCOUNT "+prm.getFcode()+" TO "+prm.getTcode());
    }

    private static void deleteTrial(LoginProfile log) throws SQLException, Exception {


        String deleteQuery_2 = "delete from gl_rpt_trial_balance";
        try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
            ps_2.executeUpdate();
            ps_2.close();
        }

    }

    private static void ifRuleTrue(LoginProfile log, TBParam prm) throws Exception {

        int cnt = 0;

        double debit = 0;
        double credit = 0;
        String xxc = "gl_openingbalance";
        String oblby = " and loccode like concat('" + prm.getLoccode() + "','%')";
        ResultSet rs = null;

        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select count(*) as cnt from  gl_openingbalance_detail where coacode like '3103%' and concat(year,period)=concat('" + prm.getYear() + "','" + prm.getPeriod() + "') " + oblby + "  group by coacode");

        rs = stmt.executeQuery();
        if (rs.next()) {
            if (rs.getInt("cnt") > 0) {

                xxc = "gl_closingbalance";

            }
        }

        PreparedStatement stmt1 = con.prepareStatement("select coacode,coadesc,ifnull(sum(debit),0) as sdebit, ifnull(sum(credit),0) as scredit   from  " + xxc + " where coacode like '3103%' and concat(year,period)=concat('" + prm.getYear() + "','" + prm.getPeriod() + "') " + oblby + "  group by coacode");

        rs = stmt1.executeQuery();
        while (rs.next()) {

            double debitx = rs.getDouble("sdebit");
            double creditx = rs.getDouble("scredit");

            double beza = debitx - creditx;
            if (beza > 0) {
                debitx = beza;
                creditx = 0;
            } else if (beza < 0) {
                debitx = 0;
                creditx = (beza * (-1));
            } else {
                debitx = 0;
                creditx = 0;
            }

            saveTrialBalance(log, rs.getString("coacode"), rs.getString("coadesc"), "--", "--", "--", debitx, creditx);

        }

    }

    private static void defaultTrialDetail(LoginProfile log, TBParam prm, String table, String rule1) throws Exception {

        Connection con = log.getCon();
        ResultSet rs = null;

        boolean in = false;
        
        
        PreparedStatement stmt = con.prepareStatement("select coacode,coadesc,applevel,satype,sacode,sadesc from  " + table + " where coacode not like  '3103%' and year='" + prm.getYear() + "' and period='" + prm.getPeriod() + "'  group by coacode,applevel,satype,sacode");

        rs = stmt.executeQuery();
        while (rs.next()) {

            in = false;

            ResultSet rs1 = null;
            
            Logger.getLogger(TBalDAO.class.getName()).log(Level.INFO, "-------" + "select ifnull(sum(debit),0) as sdebit,ifnull(sum(credit),0) as scredit,satype,sacode,sadesc from ".concat(table).concat(" as t1 ").concat(" where concat(year,period)='" + prm.getYear().concat(prm.getPeriod()) + "'").concat(" and loccode like concat('" + prm.getLoccode() + "','%') ").concat(" and coacode='" + rs.getString("coacode") + "' and satype='" + rs.getString("satype") + "' and sacode='" + rs.getString("sacode") + "' " + rule1 + " and applevel='Company' group by  coacode,satype,sacode"));
            
            PreparedStatement stmt1 = con.prepareStatement("select ifnull(sum(debit),0) as sdebit,ifnull(sum(credit),0) as scredit,satype,sacode,sadesc from ".concat(table).concat(" as t1 ").concat(" where concat(year,period)='" + prm.getYear().concat(prm.getPeriod()) + "'").concat(" and loccode like concat('" + prm.getLoccode() + "','%') ").concat(" and coacode='" + rs.getString("coacode") + "' and satype='" + rs.getString("satype") + "' and sacode='" + rs.getString("sacode") + "' " + rule1 + " and applevel='Company' group by  coacode,satype,sacode"));

            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                in = true;

                if ((rs1.getDouble("sdebit") != 0) || (rs1.getDouble("scredit") != 0)) {
                    double debitx = rs1.getDouble("sdebit");
                    double creditx = rs1.getDouble("scredit");
                    Logger.getLogger(TBalDAO.class.getName()).log(Level.INFO, "-------debit : " + debitx + " -------cebit : " + creditx);
                    if ((rs1.getDouble("sdebit") != 0) && (rs1.getDouble("scredit") != 0)) {
                        double beza = debitx - creditx;
                        if (beza > 0) {
                            debitx = beza;
                            creditx = 0;
                        } else if (beza < 0) {
                            debitx = 0;
                            creditx = (beza * (-1));
                        } else {
                            debitx = 0;
                            creditx = 0;
                        }
                    }
                    //debit += debitx;
                    //credit += creditx;
//Logger.getLogger(TBalDAO.class.getName()).log(Level.INFO, "-------debit : " + debitx + " -------cebit : " + creditx);
                    saveTrialBalance(log, rs.getString("coacode"), rs.getString("coadesc"), rs1.getString("satype"), rs1.getString("sacode"), rs1.getString("sadesc"), debitx, creditx);

                }

            }

            if (!in) {
                
                saveTrialBalance(log, rs.getString("coacode"), rs.getString("coadesc"), "--","--","--", 0.0,0.0);
            }

        }

    }

    private static void saveTrialBalance(LoginProfile log, String coacode, String coadesc, String satype, String sacode, String sadesc, double debit, double credit) throws Exception {


        try {
            String q = ("INSERT INTO gl_rpt_trial_balance(acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) values (?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, coacode);
                ps.setString(2, coadesc);
                ps.setString(3, "--");
                ps.setString(4, "--");
                ps.setString(5, "--");
                ps.setString(6, "--");
                ps.setString(7, satype);
                ps.setString(8, sacode);
                ps.setString(9, sadesc);
                ps.setDouble(10, debit);
                ps.setDouble(11, credit);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static List<TBalDetail> getListCode(LoginProfile log) throws Exception {

        List<TBalDetail> lt = new ArrayList();


        String query = "select * from gl_rpt_trial_balance group by  acccode,stgcode,entcode,satype,sacode order by acccode,stgcode,entcode,satype,sacode";
       
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement(query);
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            TBalDetail cv = new TBalDetail();

            cv.setAcccode(rs.getString("acccode"));
            cv.setAccdesc(rs.getString("accdesc"));
            cv.setCredit(rs.getDouble("credit"));
            cv.setDebit(rs.getDouble("debit"));
            cv.setEntcode(rs.getString("entcode"));
            cv.setEntdesc(rs.getString("entdesc"));
            cv.setSacode(rs.getString("sacode"));
            cv.setSadesc(rs.getString("sadesc"));
            cv.setSatype(rs.getString("satype"));
            cv.setStgcode(rs.getString("stgcode"));
            cv.setStgdesc(rs.getString("stgdesc"));
            
            lt.add(cv);

        }

        stmt.close();
        rs.close();

        return lt;
    }
    
    private static double getTotal(LoginProfile log, String type) throws Exception{
        
        
        double bal = 0.00;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum("+type+"),0) as "+type+" from gl_rpt_trial_balance");
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            bal = rs.getDouble(type);
        }
        
        stmt.close();
        rs.close();
        
        return bal;
        
    }

}
