/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.trialbalance;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class TBalMaster {
    
    private List<TBalDetail> listCore;
    private double totalDebit;
    private double totalCredit;
    private String tbtitle;

    public List<TBalDetail> getListCore() {
        return listCore;
    }

    public void setListCore(List<TBalDetail> listCore) {
        this.listCore = listCore;
    }

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public String getTbtitle() {
        return tbtitle;
    }

    public void setTbtitle(String tbtitle) {
        this.tbtitle = tbtitle;
    }
    
    
    
}
