/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.report.financial.PnL;

import com.lcsb.makmurakua.general.GeneralTerm;
import com.lcsb.makmurakua.report.financial.balancesheet.BSParam;
import com.lcsb.makmurakua.report.financial.balancesheet.BalanceSheetDAO;
import com.lcsb.makmurakua.report.financial.balancesheet.BsItem;
import com.lcsb.makmurakua.report.financial.balancesheet.BsMaster;
import com.lcsb.makmurakua.util.ext.ParseSafely;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ProfitLossDAO {
    
    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020112");
        mod.setModuleDesc("Financial Report");
        mod.setMainTable("gl_jv");
        mod.setReferID_Master("JVrefno");
        return mod;

    }
    
//    public static PnLMaster generatePnL(BSParam b, LoginProfile log) throws Exception {
//
//        Connection con = log.getCon();
//
//        PnLMaster mt = new PnLMaster();
//
//        List<PnLItem> lt = new ArrayList();
//
//        ResultSet rs = null;
//        int inc = 0;
//        double totalLvl2_cur = 0.0;
//        double totalLvl2_pre = 0.0;
//        PreparedStatement stmt = con.prepareStatement("select * from gl_config_pnl WHERE level = '1' order by code");
//        //stmt.setString(1, "12");
//        rs = stmt.executeQuery();
//        while (rs.next()) {
//
//            inc++;
//
//            if (inc == 2) {
//
//                PnLItem m1 = new PnLItem();
//                m1.setChild(true);
//                m1.setLevel(2);
//                m1.setCoacode("0000");
//                m1.setCoadescp(" ");
//                m1.setCurrentAmount("");
//                m1.setPrevAmount("");
//                lt.add(m1);
//
//                BalanceSheetDAO.setTotal1(totalLvl2_cur);
//                BalanceSheetDAO.setTotalpre1(totalLvl2_pre);
//                totalLvl2_cur = 0.0;
//
//            }
//
//            lt.add(getBsDetail(log, b, rs.getString("code"), rs.getString("descp"), rs.getString("level"), rs.getBoolean("reverse")));
//
//            if (b.getVby() == 2 || b.getVby() == 3) {
//
//                ResultSet rs1 = null;
//                PreparedStatement stmt1 = con.prepareStatement("select * from gl_config_balancesheet WHERE level = '2' and id like '" + rs.getString("id") + "%'  order by code");
//                //stmt1.setInt(1, 2);
//                // stmt1.setString(2, rs.getString("code") + "%");
//                rs1 = stmt1.executeQuery();
//                while (rs1.next()) {
//
//                    BsItem m2 = getBsDetail(log, b, rs1.getString("code"), rs1.getString("descp"), rs1.getString("level"), rs1.getBoolean("reverse"));
//                    lt.add(m2);
//                    totalLvl2_cur += ParseSafely.parseDoubleSafely(m2.getCurrentAmount());
//                    totalLvl2_pre += ParseSafely.parseDoubleSafely(m2.getPrevAmount());
//                    if (b.getVby() == 3) {
//
//                        double totalLvl3_cur = 0.0;
//                        double totalLvl3_pre = 0.0;
//                        ResultSet rs2 = null;
//                        PreparedStatement stmt2 = con.prepareStatement("select * from gl_config_balancesheet WHERE (level = ? or sub = ?) and id like ?  order by code");
//                        stmt2.setInt(1, 3);
//                        stmt2.setBoolean(2, true);
//                        stmt2.setString(3, rs1.getString("id") + "%");
//                        rs2 = stmt2.executeQuery();
//                        while (rs2.next()) {
//
//                            if (rs2.getBoolean("sub") && rs2.getString("level").equals("2")) {
//
//                                List<BsItem> listAll = (List<BsItem>) getSubCode(log, rs2.getString("code"), b, rs2.getBoolean("reverse"));
//                                for (BsItem j : listAll) {
//
//                                    totalLvl3_cur += ParseSafely.parseDoubleSafely(j.getCurrentAmount());
//                                    totalLvl3_pre += ParseSafely.parseDoubleSafely(j.getPrevAmount());
//                                    lt.add(j);
//
//                                }
//
//                            } else if (rs2.getString("level").equals("3")) {
//
//                                BsItem m1 = getBsDetail(log, b, rs2.getString("code"), rs2.getString("descp"), rs2.getString("level"), rs2.getBoolean("reverse"));
//                                lt.add(m1);
//                                totalLvl3_cur += ParseSafely.parseDoubleSafely(m1.getCurrentAmount());
//                                totalLvl3_pre += ParseSafely.parseDoubleSafely(m1.getPrevAmount());
//
//                            }
//
//                            if (rs2.isLast()) {
//                                BsItem m1 = new BsItem();
//                                m1.setChild(true);
//                                m1.setLevel(2);
//                                m1.setCoacode("0000");
//                                m1.setCoadescp("");
//                                m1.setCurrentAmount("__________________");
//                                m1.setPrevAmount("__________________");
//                                lt.add(m1);
//
//                                BsItem m5 = new BsItem();
//                                m5.setChild(true);
//                                m5.setLevel(2);
//                                m5.setCoacode("0000");
//                                m5.setCoadescp("TOTAL " + rs1.getString("descp"));
//                                m5.setCurrentAmount(GeneralTerm.normalCredit(totalLvl3_cur));
//                                m5.setPrevAmount(GeneralTerm.normalCredit(totalLvl3_pre));
//                                lt.add(m5);
//                            }
//
//                        }
//                        totalLvl2_cur += totalLvl3_cur;
//                        totalLvl2_pre += totalLvl3_pre;
//                        Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, "totalLvl2 - " + totalLvl2_cur + "** totalLvl3 - " + totalLvl3_cur);
//                        rs2.close();
//                        stmt2.close();
//
//                    }
//
//                    if (rs1.isLast()) {
//                        BsItem m1 = new BsItem();
//                        m1.setChild(true);
//                        m1.setLevel(1);
//                        m1.setCoacode("0000");
//                        m1.setCoadescp("");
//                        m1.setCurrentAmount("__________________");
//                        m1.setPrevAmount("__________________");
//                        lt.add(m1);
//
//                        BsItem m4 = new BsItem();
//                        m4.setChild(true);
//                        m4.setLevel(1);
//                        m4.setCoacode("0000");
//                        m4.setCoadescp("TOTAL " + rs.getString("descp"));
//                        m4.setCurrentAmount(GeneralTerm.normalCredit(totalLvl2_cur));
//                        m4.setPrevAmount(GeneralTerm.normalCredit(totalLvl2_pre));
//                        lt.add(m4);
//                    }
//
//                    BalanceSheetDAO.setTotal2(totalLvl2_cur);
//                    BalanceSheetDAO.setTotalpre2(totalLvl2_pre);
//
//                }
//
//                rs1.close();
//                stmt1.close();
//
//            }
//
//        }
//
//        BsItem m1 = new BsItem();
//        m1.setChild(true);
//        m1.setLevel(1);
//        m1.setCoacode("0000");
//        m1.setCoadescp("");
//        m1.setCurrentAmount("__________________");
//        m1.setPrevAmount("__________________");
//        lt.add(m1);
//        
//        BsItem m2 = new BsItem();
//        m2.setChild(true);
//        m2.setLevel(1);
//        m2.setCoacode("0000");
//        m2.setCoadescp("NET ASSETS ");
//        m2.setCurrentAmount(GeneralTerm.normalCredit(BalanceSheetDAO.getTotal1() - BalanceSheetDAO.getTotal2()));
//        m2.setPrevAmount(GeneralTerm.normalCredit(BalanceSheetDAO.getTotalpre1()- BalanceSheetDAO.getTotalpre2()));
//        lt.add(m2);
//        
//        BsItem m3 = new BsItem();
//        m3.setChild(true);
//        m3.setLevel(1);
//        m3.setCoacode("0000");
//        m3.setCoadescp("");
//        m3.setCurrentAmount("=================");
//        m3.setPrevAmount("=================");
//        lt.add(m3);
//
//        rs.close();
//        stmt.close();
//
//        mt.setListItem(lt);
//
//        return mt;
//
//    }
    
//    private static BsItem getBsDetail(LoginProfile log, BSParam b, String code, String descp, String level, boolean reverse) throws Exception {
//
//        BsItem m = new BsItem();
//
//        //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, "level - "+level+"** vby - "+b.getVby());
//        int lvl = Integer.parseInt(level);
//        String cAmt = "";
//        String pAmt = "";
//
//        if (b.getVby() != lvl) {
//            m.setHeader(true);
//            m.setLevel(lvl);
//            cAmt = "";
//            pAmt = "";
//        } else {
//            m.setHeader(false);
//            m.setChild(true);
//            m.setLevel(lvl);
//
//            if (reverse) {
//
//                cAmt = GeneralTerm.normalDebit(getTotal(log, b, code, "now", "00"));
//                pAmt = GeneralTerm.normalDebit(getTotal(log, b, descp, "then", "00"));
//
//            } else {
//
//                cAmt = GeneralTerm.normalCredit(getTotal(log, b, code, "now", "00"));
//                pAmt = GeneralTerm.normalCredit(getTotal(log, b, descp, "then", "00"));
//
//            }
//
//        }
//
//        m.setCoacode(code);
//        m.setCoadescp(descp);
//        m.setCurrentAmount(cAmt);
//        m.setPrevAmount(pAmt);
//
//        return m;
//
//    }
    
  
    
    public static List<PnLCodes> getListCodes(LoginProfile log) throws Exception {

        List<PnLCodes> CVi;
        CVi = new ArrayList();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM chartofacccount a, gl_config_pnl b WHERE a.code = b.code AND LENGTH(a.code) = '2'");
            //stmt.setString(1, sessionid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                PnLCodes p = new PnLCodes();
                
                p.setCode(rs.getString("a.code"));
                p.setDescp(rs.getString("a.descp"));
                p.setLevel(rs.getInt("b.level"));
                p.setFinallvl(rs.getString("a.finallvl"));
                p.setSubListCode(ProfitLossDAO.getSubListCodes(log, rs.getString("a.code"), 2));
                
                CVi.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<PnLCodes> getSubListCodes(LoginProfile log, String code, int length) throws Exception {

        List<PnLCodes> CVi;
        CVi = new ArrayList();
        
        length = length + 2;

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM chartofacccount WHERE code like ? AND LENGTH(code) = ?");
            stmt.setString(1, code + "%");
            stmt.setInt(2, length);
            rs = stmt.executeQuery();
            //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
            while (rs.next()) {
                
                PnLCodes p = new PnLCodes();
                
                p.setCode(rs.getString("code"));
                p.setDescp(rs.getString("descp"));
                //p.setLevel(rs.getInt("b.level"));
                p.setFinallvl(rs.getString("finallvl"));
                p.setTotalActualMonth(0.0);
                
                //if(p.getFinallvl().equals("No")){
                Logger.getLogger(ProfitLossDAO.class.getName()).log(Level.INFO, String.valueOf(p.getCode().length()) + "--" + String.valueOf(getMaxCOALength(log, p.getCode())));
                int l = p.getCode().length();
                int x = getMaxCOALength(log, p.getCode());
                if(p.getCode().length() < x){
                    p.setSubListCode(ProfitLossDAO.getSubListCodes(log, rs.getString("code"), length));
                }
                
                
                
                
                CVi.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static String renderRow(int level, List<PnLCodes> ls) {

        String indent = "";
        String row = "";

        for (int i = 0; i < level; i++) {
            indent += "&nbsp;&nbsp;&nbsp;";
        }

        if (ls != null) {
            for (PnLCodes m : ls) {
                row += "<tr>\n"
                        + "     <td>" + indent + "<span class=\"title_row_font_sub\">" + m.getCode() + " " + m.getDescp() + "</span></td>\n"
                        + "     <td align=\"right\">&nbsp;<span class=\"total_row_font\"></span></td>\n"
                        + "     <td align=\"right\">&nbsp;<span class=\"total_row_font\"></span></td>\n"
                        + "  </tr>";
            }
        }

        return row;

    }
    
    public static int getMaxCOALength(LoginProfile log, String code){
        

        int max = 0;

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT MAX(LENGTH(code)) as tot FROM chartofacccount WHERE code like ?");
            stmt.setString(1, code + "%");
            //stmt.setString(2, colType);
            rs = stmt.executeQuery();
            if (rs.next()) {
                max = rs.getInt("tot");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return max;
        
    }
    
    
    public ArrayList getCOA(){
        ArrayList<String> x;
        x = new ArrayList<String>();
        x.add("20");
        x.add("22");
        return x;
        
    } public ArrayList getTaxCode(){
        ArrayList<String> x;
        x = new ArrayList<String>();
        x.add("2908");
        x.add("135107");
                
        return x;
    }
    
    public static ArrayList AnalyseCOA(LoginProfile log, String code,int main) throws SQLException {
        ArrayList<String> x;
        x = new ArrayList<String>();
        boolean check;
        
        String ext = "";
        if(main==1){
            ext = "select * from chartofacccount a, gl_config_pnl b where a.code = b.code and length(a.code) = '2'";
        }else if(main==2){
            
           /* if(filterCode(code)==true){
                 ext = "select * from chartofacccount where code like '"+code+"%' and code <> '"+code+"'";
            }else{
                
                 ext = "select * from chartofacccount where code like '"+code+"%' and code <> '"+code+"' and length(code) = '"+getMaxLength(code)+"'";
            }*/
           
        }
        
        try{
        Statement statement;
        statement = log.getCon().createStatement();
        ResultSet rs = statement.executeQuery(ext);
         //stmt.setString(1, code);
         while (rs.next()) {
             String m = rs.getString("code");
              x.add(m);
         }
        rs.close();
        statement.close();
        }
          catch (SQLException e) {
              
            }
       
        
        return x;
    }
    
    public static Double getTotal(LoginProfile log,String year,String period,String otype, String vby, String code, String time){
        Double total = 0.00;
        
        String table = "";
        if(otype.equalsIgnoreCase("1"))	
	{
            if(time.equalsIgnoreCase("now")){
                table=" from gl_openingbalance ";
            }else if(time.equalsIgnoreCase("then")){
                table=" from gl_closingbalance ";
                year=String.valueOf(Integer.parseInt(year)-1);
            }
            else if(time.equalsIgnoreCase("thenend")){
                table=" from gl_closingbalance ";
                year=String.valueOf(Integer.parseInt(year)-1);
                period="12";
            }
            
            
	}
	else if(otype.equalsIgnoreCase("2"))	
	{
             if(time.equalsIgnoreCase("now")){
                table=" from gl_openingbalance_detail ";
            }else if(time.equalsIgnoreCase("then")){
                table=" from gl_openingbalance_detail ";
                year=String.valueOf(Integer.parseInt(year)-1);
            }
             else if(time.equalsIgnoreCase("thenend")){
                table=" from gl_openingbalance_detail ";
                year=String.valueOf(Integer.parseInt(year)-1);
                period="12";
            }
	}
        
        try{
        Statement statement;
        statement = log.getCon().createStatement();
        ResultSet rs = statement.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as total  "+table+" where concat(year,period)='"+year.concat(period)+"' and coacode like '"+code+"%'");
         while (rs.next()) {
           total = rs.getDouble("total");
         }
         
         statement.close();
         rs.close();
        }
          catch (SQLException e) {
              
            }
        
        return total;
    }
    
    public static double getSales(LoginProfile log,String year,String period,String acccode, String type, String range) throws Exception {
        
        String location="";
        String table = "";
        String periodRange = "";
        String columnsyntax = "";
        double sumsales=0;
        
        if(type.equalsIgnoreCase("actual")){
            
            columnsyntax = "ifnull(credit-debit,0) as sumsales";
            
            if(range.equalsIgnoreCase("thismonth")){
                table = "gl_openingbalance_detail";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("todate")){
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("prevyear")){
                int pyear=Integer.parseInt(year);
		pyear--;
		year=String.valueOf(pyear);
                
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("prevallyear")){
                int pyear=Integer.parseInt(year);
		pyear--;
		year=String.valueOf(pyear);
                
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+ year +"','12')";
            }    
            
        }else if(type.equalsIgnoreCase("budget")){
            
             columnsyntax = "ifnull(amount,0) as sumsales";
             
             table = "gl_budget";
             if(range.equalsIgnoreCase("thismonth")){
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("todate")){
                periodRange = "year='"+ year +"' and (period between 1 and "+ period +")";
            }else if(range.equalsIgnoreCase("year")){
                periodRange = "year='"+ year +"'";
            }  
             
        }
        
        Statement stment = log.getCon().createStatement();
        //test+="select ifnull(credit-debit,0) as sumsales from gl_openingbalance_detail as t1 left join product_info as t2 on t2.entercode=right(t1.loccode,2) left join enterprise_info as t3 on t3.code=right(t1.loccode,2) where t1.coacode like '"+ acccode +"%' "+ location +" and concat(year,period)=concat('"+ year +"','"+ period +"')  "+ tempt1 +" "+ tempt2 +" "+ syarat +" group by t1.itemid<br>";
        //ResultSet setent =stment.executeQuery("select ifnull(credit-debit,0) as sumsales from gl_openingbalance_detail as t1 left join product_info as t2 on t2.entercode=right(t1.loccode,2) left join enterprise_info as t3 on t3.code=right(t1.loccode,2) where t1.coacode like '"+ acccode +"%' "+ location +" and concat(year,period)=concat('"+ year +"','"+ period +"')  "+ tempt1 +" "+ tempt2 +" "+ syarat +" group by t1.itemid");
        ResultSet setent =stment.executeQuery(" select "+columnsyntax+" from "+table+" where coacode like '"+acccode+"%' and "+periodRange+"  group by itemid");
       
        while(setent.next()) {
            sumsales+=setent.getDouble("sumsales");
        }
        
        stment.close();
        setent.close();
        return sumsales;
    }
    
     public int getLength(LoginProfile log, String code){
        int x = 0;
        
        try{
        Statement statement;
        statement = log.getCon().createStatement();
        ResultSet rs = statement.executeQuery("SELECT LENGTH(code) as length FROM chartofacccount where code = '"+code+"'");
         //stmt.setString(1, code);
            if (rs.next()) {
                x = rs.getInt("length");
            }
        statement.close();
        rs.close();
        }catch (SQLException e) {
              
        }
        return x;
    }
     
    public int haveChild(LoginProfile log,String code){
        int x = 0;
        
        try{
        Statement statement;
        statement = log.getCon().createStatement();
        ResultSet rs = statement.executeQuery("SELECT count(*) as cnt FROM chartofacccount where code like '"+code+"%' and code <> '"+code+"'");
         //stmt.setString(1, code);
            if (rs.next()) {
                x = rs.getInt("cnt");
            }
        statement.close();
        rs.close();
        }catch (SQLException e) {
              
        }
        return x;
    }
    
}
