/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.gl;

/**
 *
 * @author fadhilfahmi
 */
public class GListParam {
    
    private String fcode, tcode, fyear, fperiod, tyear, tperiod, debitopt, creditopt, sacode, satype, sadesc, locdesc, loccode, loclevel,viewby;

    public String getViewby() {
        return viewby;
    }

    public void setViewby(String viewby) {
        this.viewby = viewby;
    }

    public String getFcode() {
        return fcode;
    }

    public void setFcode(String fcode) {
        this.fcode = fcode;
    }

    public String getTcode() {
        return tcode;
    }

    public void setTcode(String tcode) {
        this.tcode = tcode;
    }

    public String getFyear() {
        return fyear;
    }

    public void setFyear(String fyear) {
        this.fyear = fyear;
    }

    public String getFperiod() {
        return fperiod;
    }

    public void setFperiod(String fperiod) {
        this.fperiod = fperiod;
    }

    public String getTyear() {
        return tyear;
    }

    public void setTyear(String tyear) {
        this.tyear = tyear;
    }

    public String getTperiod() {
        return tperiod;
    }

    public void setTperiod(String tperiod) {
        this.tperiod = tperiod;
    }

    public String getDebitopt() {
        return debitopt;
    }

    public void setDebitopt(String debitopt) {
        this.debitopt = debitopt;
    }

    public String getCreditopt() {
        return creditopt;
    }

    public void setCreditopt(String creditopt) {
        this.creditopt = creditopt;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }
    
}
