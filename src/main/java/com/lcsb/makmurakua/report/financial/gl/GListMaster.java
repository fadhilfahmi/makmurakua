/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.gl;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class GListMaster {
    
    private List<GListHeader> listCore;
    private double totalDebit;
    private double totalCredit;
    private String gltitle;
    private String glcodetocode;

    public String getGltitle() {
        return gltitle;
    }

    public void setGltitle(String gltitle) {
        this.gltitle = gltitle;
    }

    public String getGlcodetocode() {
        return glcodetocode;
    }

    public void setGlcodetocode(String glcodetocode) {
        this.glcodetocode = glcodetocode;
    }
    

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public List<GListHeader> getListCore() {
        return listCore;
    }

    public void setListCore(List<GListHeader> listCore) {
        this.listCore = listCore;
    }
    
}
