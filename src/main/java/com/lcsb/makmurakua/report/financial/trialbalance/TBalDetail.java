/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.trialbalance;

/**
 *
 * @author fadhilfahmi
 */
public class TBalDetail {
    
    private String acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc;
    private double debit, credit;

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdesc() {
        return accdesc;
    }

    public void setAccdesc(String accdesc) {
        this.accdesc = accdesc;
    }

    public String getStgcode() {
        return stgcode;
    }

    public void setStgcode(String stgcode) {
        this.stgcode = stgcode;
    }

    public String getStgdesc() {
        return stgdesc;
    }

    public void setStgdesc(String stgdesc) {
        this.stgdesc = stgdesc;
    }

    public String getEntcode() {
        return entcode;
    }

    public void setEntcode(String entcode) {
        this.entcode = entcode;
    }

    public String getEntdesc() {
        return entdesc;
    }

    public void setEntdesc(String entdesc) {
        this.entdesc = entdesc;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
    
    
    
}
