/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.PnL;

/**
 *
 * @author fadhilfahmi
 */
public class PnLItem {
    
    private String coacode;
    private String coadescp;
    private String currentAmount;
    private String prevAmount;
    private boolean header;
    private boolean child;
    private int level;

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(String currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getPrevAmount() {
        return prevAmount;
    }

    public void setPrevAmount(String prevAmount) {
        this.prevAmount = prevAmount;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public boolean isChild() {
        return child;
    }

    public void setChild(boolean child) {
        this.child = child;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    
    
    
}
