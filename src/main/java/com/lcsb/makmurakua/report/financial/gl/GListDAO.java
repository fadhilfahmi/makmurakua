/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.gl;

import com.lcsb.makmurakua.general.GeneralTerm;
import com.lcsb.makmurakua.report.financial.balancesheet.BalanceSheetDAO;
import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class GListDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020112");
        mod.setModuleDesc("Financial Report");
        mod.setMainTable("gl_jv");
        mod.setReferID_Master("JVrefno");
        return mod;

    }
    
    
    
    public static List<GListDetail> getListDetail(LoginProfile log, GListParam prm, String code, String satype, String sacode, String oblby, double initbalance) throws Exception {

        Connection con = log.getCon();
        List<GListDetail> lt = new ArrayList();
        
        String tableGL = "";
        if(prm.getViewby().equals("realtime")){
            tableGL = "gl_posting_temp";
        }else if(prm.getViewby().equals("final")){
            tableGL = "gl_posting_distribute";
        }

        String query = "select year,period,tarikh,source,remark,novoucher,ifnull(sum(debit),0) as debit,ifnull(sum(credit),0) as credit, sacode from "+tableGL+" where coacode='"+ code +"' and satype='"+ satype +"' and sacode='"+ sacode +"'  "+ oblby +" and (concat(year,lpad(period,2,'0'))  between concat('"+ prm.getFyear() +"',lpad('"+ prm.getFperiod() +"',2,'0')) and concat('"+ prm.getTyear() +"',lpad('"+ prm.getTperiod() +"',2,'0')))  group by novoucher,coacode,sacode,remark  order by year,period,tarikh"; 
        
        Logger.getLogger(GListDAO.class.getName()).log(Level.INFO, " ---- " +query);
        double netchange = 0.00;
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement(query);
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            GListDetail cv = new GListDetail();
            
            if(rs.getDouble("debit")==0){
                cv.setDebit("");
            }else{
                cv.setDebit(GeneralTerm.normalCredit(rs.getDouble("debit")));
            }
            
            if(rs.getDouble("credit")==0){
                cv.setCredit("");
            }else{
                cv.setCredit(GeneralTerm.normalCredit(rs.getDouble("credit")));
            }

            cv.setNovoucher(rs.getString("novoucher"));
            cv.setPeriod(rs.getString("period"));
            
            //for remark-------------
            if(rs.getString("novoucher").substring(0, 3).equals("DNE") || rs.getString("novoucher").substring(0, 3).equals("CNE")){
                cv.setRemark(getNoteRemark(log, rs.getString("novoucher")));
            }else if((rs.getString("novoucher").substring(0, 3).equals("ORN") || rs.getString("novoucher").substring(0, 3).equals("PVN")) && rs.getString("sacode").equals("00")){
                //cv.setRemark(GListDAO.getChequeRelatedTo(log, rs.getString("novoucher").substring(0, 3), rs.getString("novoucher")));
            }else{
                cv.setRemark(rs.getString("remark"));
            }
            //for remark-------------
            
            cv.setSource(rs.getString("source"));
            cv.setTarikh(rs.getString("tarikh"));
            cv.setYear(rs.getString("year"));
            netchange+=(rs.getDouble("debit")-rs.getDouble("credit"));
            if(rs.isLast()){
               cv.setNetchange(GeneralTerm.normalCredit(netchange)); 
               cv.setLastbalance(GeneralTerm.normalCredit(netchange + initbalance));
               cv.setStar("<i class=\"fa fa-flag-checkered\" aria-hidden=\"true\"></i>");
            }else{
                cv.setNetchange("");
                cv.setLastbalance("");
                cv.setStar("");
            }
            
            //begin---add cheque no-----------------------------------------------------
            if(rs.getString("novoucher").substring(0, 3).equals("ORN") || rs.getString("novoucher").substring(0, 3).equals("PVN")){
                
                String c = "";
                //c = GListDAO.getChequeNo(log, rs.getString("novoucher").substring(0, 3), rs.getString("novoucher"));
                if(c==null){
                    cv.setChequeno("");
                }else{
                    cv.setChequeno(c);
                }
                
            }else{
                cv.setChequeno("");
            }
            //end-----add cheque no-----------------------------------------------------
            
            lt.add(cv);

        }

        stmt.close();
        rs.close();

        return lt;
    }
    
    
    
    
    
    private static String getNoteRemark(LoginProfile log, String refer) throws SQLException{
        
        String remark = "";
        String table = "";
        if(refer.substring(0, 3).equals("DNE")){
            table = "gl_itemdebitnote";
        }else if(refer.substring(0, 3).equals("CNE")){
            table = "gl_itemcreditnote";
        }
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT descp FROM "+ table +" WHERE noteno = ?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        while (rs.next()) {
            if(rs.isLast()){
                remark += rs.getString(1);
            }else{
                remark += rs.getString(1) + ", ";
            }
            
        }
        
        return remark;
    }

    public static List<GListHeader> getListCode(LoginProfile log, GListParam prm) throws Exception {

        Connection con = log.getCon();
        List<GListHeader> lt = new ArrayList();

        String oblby = "and loccode like concat('" + prm.getLoccode() + "','%')";
        String sub = "";

        if (!prm.getSatype().equals("None")) {
            sub = " and satype='" + prm.getSatype() + "' and sacode='" + prm.getSacode() + "'";
        }

        String debitsyt = "";
        String creditsyt = "";
        if (prm.getDebitopt() != null) {
            debitsyt = "and debit<>0";
        }
        if (prm.getCreditopt() != null) {
            creditsyt = "and credit<>0";
        }
        
        String tableGL = "";
        if(prm.getViewby().equals("realtime")){
            tableGL = "gl_posting_temp";
        }else if(prm.getViewby().equals("final")){
            tableGL = "gl_posting_distribute";
        }

        String query = "(SELECT coacode,coadesc,satype,sacode,sadesc  from gl_openingbalance  where (coacode between '" + prm.getFcode() + "' and '" + prm.getTcode() + "') and concat(year,lpad(period,2,'0'))  between concat('" + prm.getFyear() + "',lpad('" + prm.getFperiod() + "',2,'0')) and concat('" + prm.getTyear() + "',lpad('" + prm.getTperiod() + "',2,'0')) " + oblby + " " + sub + " group by coacode,satype,sacode) union (SELECT coacode,coadesc,satype,sacode,sadesc from "+ tableGL +" where (coacode between '" + prm.getFcode() + "' and '" + prm.getTcode() + "') and (concat(year,lpad(period,2,'0'))  between concat('" + prm.getFyear() + "',lpad('" + prm.getFperiod() + "',2,'0')) and concat('" + prm.getTyear() + "',lpad('" + prm.getTperiod() + "',2,'0'))) " + sub + "  " + oblby + "  " + debitsyt + " " + creditsyt + " group by coacode,satype,sacode) order by coacode,satype,sacode";
        
        Logger.getLogger(GListDAO.class.getName()).log(Level.INFO, "-----------"+query);
        
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement(query);
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            GListHeader cv = new GListHeader();

            cv.setCoacode(rs.getString("coacode"));
            cv.setCoadescp(rs.getString("coadesc"));
            cv.setSacode(rs.getString("sacode"));
            cv.setSadesc(rs.getString("sadesc"));
            cv.setSatype(rs.getString("satype"));
            cv.setBalance(GListDAO.getBalance(log, rs.getString("coacode"), rs.getString("satype"), rs.getString("sacode"), oblby, prm.getFyear(), prm.getFperiod()));
            cv.setListDetail(GListDAO.getListDetail(log, prm, cv.getCoacode(), cv.getSatype(), cv.getSacode(), oblby, cv.getBalance()));
            
            lt.add(cv);

        }

        stmt.close();
        rs.close();

        return lt;
    }

    private static GListHeader getHeader(ResultSet rs) throws SQLException {
        GListHeader cv = new GListHeader();

        //cv.setBalance(rs.getDouble("balance"));
        cv.setCoacode(rs.getString("coacode"));
        cv.setCoadescp(rs.getString("coadesc"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));

        return cv;
    }

    private static double getBalance(LoginProfile log, String coacode, String satype, String sacode, String oblby, String yearf, String periodf) throws SQLException, Exception {

        double bal = 0.00;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum(debit-credit),0) as jum from gl_openingbalance where coacode='" + coacode + "' and satype='" + satype + "' and sacode='" + sacode + "' " + oblby + " and concat(year,lpad(period,2,'0'))  = concat('" + yearf + "',lpad('" + periodf + "',2,'0'))");
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            bal = rs.getDouble("jum");
        }

        stmt.close();
        rs.close();

        return bal;

    }
    
    private static double getTotal(LoginProfile log, String type, GListParam prm) throws Exception{
        
        
        double bal = 0.00;

        ResultSet rs = null;
Logger.getLogger(GListDAO.class.getName()).log(Level.INFO, "select ifnull(sum("+type+"),0) as "+type+",ifnull(sum("+type+"),0) as "+type+" from gl_posting_distribute where (coacode between '"+prm.getFcode()+"' and '"+prm.getTcode()+"') and (concat(year,lpad(period,2,'0'))  between concat('"+prm.getFyear()+"',lpad('"+prm.getFperiod()+"',2,'0')) and concat('"+prm.getTyear()+"',lpad('"+prm.getTperiod()+"',2,'0'))) and  loccode like concat('"+prm.getLoccode()+"','%')");
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum("+type+"),0) as "+type+",ifnull(sum("+type+"),0) as "+type+" from gl_posting_distribute where (coacode between '"+prm.getFcode()+"' and '"+prm.getTcode()+"') and (concat(year,lpad(period,2,'0'))  between concat('"+prm.getFyear()+"',lpad('"+prm.getFperiod()+"',2,'0')) and concat('"+prm.getTyear()+"',lpad('"+prm.getTperiod()+"',2,'0'))) and  loccode like concat('"+prm.getLoccode()+"','%')");
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            bal = rs.getDouble(type);
        }
        
        stmt.close();
        rs.close();
        
        return GeneralTerm.PrecisionDouble(bal);
        
    }
    
    public static GListMaster generateGListing(LoginProfile log, GListParam prm) throws Exception{
        
        GListMaster gl = new GListMaster();
        
        gl.setListCore(GListDAO.getListCode(log, prm));
        gl.setTotalDebit(getTotal(log, "debit", prm));
        gl.setTotalCredit(getTotal(log, "credit", prm));
        gl.setGltitle("GENERAL LEDGER LISTING FOR PERIOD "+prm.getFperiod()+"-"+prm.getFyear()+" TO PERIOD "+prm.getTperiod()+"-"+prm.getTyear()+"");
        gl.setGlcodetocode("FROM ACCOUNT "+prm.getFcode()+" TO "+prm.getTcode());
        
        return gl;
        
    }
    
    

}
