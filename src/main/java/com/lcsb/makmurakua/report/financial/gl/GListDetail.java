/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.gl;

/**
 *
 * @author fadhilfahmi
 */
public class GListDetail {
    
    private String year,period,tarikh,source,remark,novoucher,netchange,debit,credit, lastbalance, star, chequeno;

    public String getChequeno() {
        return chequeno;
    }

    public void setChequeno(String chequeno) {
        this.chequeno = chequeno;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getLastbalance() {
        return lastbalance;
    }

    public void setLastbalance(String lastbalance) {
        this.lastbalance = lastbalance;
    }

    public String getNetchange() {
        return netchange;
    }

    public void setNetchange(String netchange) {
        this.netchange = netchange;
    }


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNovoucher() {
        return novoucher;
    }

    public void setNovoucher(String novoucher) {
        this.novoucher = novoucher;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    
    
    
}
