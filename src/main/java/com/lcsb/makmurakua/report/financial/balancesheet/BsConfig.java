/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.balancesheet;

/**
 *
 * @author fadhilfahmi
 */
public class BsConfig {
    
    private String code;
    private String header;
    private String queryLvl1;
    private String queryLvl2;
    private String queryLvl3;

    public String getQueryLvl1() {
        return queryLvl1;
    }

    public void setQueryLvl1(String queryLvl1) {
        this.queryLvl1 = queryLvl1;
    }

    public String getQueryLvl2() {
        return queryLvl2;
    }

    public void setQueryLvl2(String queryLvl2) {
        this.queryLvl2 = queryLvl2;
    }

    public String getQueryLvl3() {
        return queryLvl3;
    }

    public void setQueryLvl3(String queryLvl3) {
        this.queryLvl3 = queryLvl3;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    
    
}
