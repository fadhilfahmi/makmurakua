/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.PnL;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class PnLCodes {
    
    private String code;
    private String descp;
    private int level;
    private String finallvl;
    private double totalActualMonth;
    private List<PnLCodes> subListCode;
    private String renderRowParent;
    private String renderRowChild;

    public String getRenderRowParent() {
        return renderRowParent;
    }

    public void setRenderRowParent(String renderRowParent) {
        this.renderRowParent = renderRowParent;
    }

    public String getRenderRowChild() {
        return renderRowChild;
    }

    public void setRenderRowChild(String renderRowChild) {
        this.renderRowChild = renderRowChild;
    }


    public double getTotalActualMonth() {
        return totalActualMonth;
    }

    public void setTotalActualMonth(double totalActualMonth) {
        this.totalActualMonth = totalActualMonth;
    }

    public List<PnLCodes> getSubListCode() {
        return subListCode;
    }

    public void setSubListCode(List<PnLCodes> subListCode) {
        this.subListCode = subListCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getFinallvl() {
        return finallvl;
    }

    public void setFinallvl(String finallvl) {
        this.finallvl = finallvl;
    }

    
    
}
