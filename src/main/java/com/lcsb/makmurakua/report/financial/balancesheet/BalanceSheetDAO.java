/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.balancesheet;

import com.lcsb.makmurakua.general.GeneralTerm;
import com.lcsb.makmurakua.model.report.BalancesheetModel;
import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.ext.ParseSafely;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class BalanceSheetDAO {

    private static double total1;
    private static double total2;
    private static double totalpre1;
    private static double totalpre2;

    public static double getTotal1() {
        return total1;
    }

    public static void setTotal1(double total1) {
        BalanceSheetDAO.total1 = total1;
    }

    public static double getTotal2() {
        return total2;
    }

    public static void setTotal2(double total2) {
        BalanceSheetDAO.total2 = total2;
    }

    public static double getTotalpre1() {
        return totalpre1;
    }

    public static void setTotalpre1(double totalpre1) {
        BalanceSheetDAO.totalpre1 = totalpre1;
    }

    public static double getTotalpre2() {
        return totalpre2;
    }

    public static void setTotalpre2(double totalpre2) {
        BalanceSheetDAO.totalpre2 = totalpre2;
    }
    
    

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020112");
        mod.setModuleDesc("Financial Report");
        mod.setMainTable("gl_jv");
        mod.setReferID_Master("JVrefno");
        return mod;

    }

    private static boolean filterCode(LoginProfile log, String code) throws Exception {

        boolean x = false;

        try {
            Statement statement;
            statement = log.getCon().createStatement();
            ResultSet rs = statement.executeQuery("select count(*) as cnt from gl_openingbalance where coacode like '" + code + "%' and sacode <> '00' ");
            //stmt.setString(1, code);
            if (rs.next()) {
                int m = rs.getInt("cnt");

                if (m > 0) {
                    x = true;
                }

            }
        } catch (SQLException e) {

        }

        return x;
    }

    private static int getMaxLength(LoginProfile log, String code) throws Exception {

        int x = 0;

        try {
            Statement statement;
            statement = log.getCon().createStatement();
            ResultSet rs = statement.executeQuery("SELECT MAX(LENGTH(code)) as length FROM chartofacccount where code like '" + code + "%'");
            //stmt.setString(1, code);
            if (rs.next()) {
                x = rs.getInt("length");
            }
            statement.close();
            rs.close();
        } catch (SQLException e) {

        }
        return x;
    }

    private static Double getTotal(LoginProfile log, BSParam b, String code, String time, String sacode) throws Exception {

        Double total = 0.00;

        String year = b.getYear();

        String table = "";
        if (b.getOtype().equalsIgnoreCase("Opening Balance")) {
            if (time.equalsIgnoreCase("now")) {
                table = " from gl_openingbalance ";
            } else if (time.equalsIgnoreCase("then")) {
                table = " from gl_closingbalance ";
                year = String.valueOf(Integer.parseInt(b.getYear()) - 1);
            }

        } else if (b.getOtype().equalsIgnoreCase("This Month")) {
            if (time.equalsIgnoreCase("now")) {
                table = " from gl_openingbalance_detail ";
            } else if (time.equalsIgnoreCase("then")) {
                table = " from gl_openingbalance_detail ";
                year = String.valueOf(Integer.parseInt(b.getYear()) - 1);
            }
        } else if (b.getOtype().equalsIgnoreCase("To Date")) {
            if (time.equalsIgnoreCase("now")) {
                table = " from gl_closingbalance ";
            } else if (time.equalsIgnoreCase("then")) {
                table = " from gl_closingbalance ";
                year = String.valueOf(Integer.parseInt(b.getYear()) - 1);
            }
        }

        String ext = "";
        if (!sacode.equals("00")) {
            ext = " and sacode = '" + sacode + "'";
        }

        //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, time+"******select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as total  " + table + " where concat(year,period)='" + b.getYear().concat(b.getPeriod()) + "' and coacode like '" + code + "%' " + ext + "");
        try {
            Statement statement;
            statement = log.getCon().createStatement();
            ResultSet rs = statement.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as total  " + table + " where concat(year,period)='" + year.concat(b.getPeriod()) + "' and coacode like '" + code + "%' " + ext + "");
            while (rs.next()) {
                total = rs.getDouble("total");
            }

            statement.close();
            rs.close();
        } catch (SQLException e) {

        }

        return total;
    }

    public static String getReportNo(LoginProfile log, String coacode) throws Exception {

        String repno = "";

        Statement statement;
        statement = log.getCon().createStatement();
        ResultSet rs = statement.executeQuery("select report from chartofacccount where code = '" + coacode + "'");
        if (rs.next()) {
            repno = rs.getString("report");
        }

        return repno;
    }

    public static double reob(LoginProfile log, String loccode, String year, String period, String otype) throws Exception {
        //double dblx=0;
        Connection con = log.getCon();
        double pendapatan = 0;
        double perbelanjaan = 0;

        String obltype = " from gl_openingbalance ";
        String obltype2 = " from gl_closingbalance ";

        String loccodex = "";

        if (!loccode.equalsIgnoreCase("")) {
            loccodex = " and loccode like '" + loccode + "%' ";
        }

        if (otype.equalsIgnoreCase("Opening Balance")) {
            obltype = " from gl_openingbalance ";
        } else if (otype.equalsIgnoreCase("This Month")) {
            obltype = " from gl_openingbalance_detail ";
        } else if (otype.equalsIgnoreCase("To Date")) {
            obltype = " from gl_closingbalance ";
        }

        Statement stmdapat = con.createStatement();
        ResultSet setdapat = stmdapat.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype + " where (coacode like '21%' or coacode like '23%') " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setdapat.next()) {
            pendapatan = setdapat.getDouble("beza");
        }

        Statement stmbelanja = con.createStatement();
        ResultSet setbelanja = stmbelanja.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype + " where (coacode like '22%' or coacode like '24%' or coacode like '29%' or coacode like '3104%') " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setbelanja.next()) {
            perbelanjaan = setbelanja.getDouble("beza");
        }

        double re = pendapatan + perbelanjaan;

        double terkumpul = 0;

        //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, "select (ifnull(sum(debit),0) - ifnull(sum(credit),0))) as beza "+obltype+" where coacode = '3103' " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        setbelanja = stmbelanja.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype + " where coacode = '3103' " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setbelanja.next()) {
            re += setbelanja.getDouble("beza");
        }

        return re;
    }

    public static double recb(LoginProfile log, String loccode, String year, String period, String otype) throws Exception {
        //double dblx=0;
        Connection con = log.getCon();
        double pendapatan = 0;
        double perbelanjaan = 0;

        String loccodex = "";
        String obltype2 = "";

        if (!loccode.equalsIgnoreCase("")) {
            loccodex = " and loccode like '" + loccode + "%' ";
        }

        if (otype.equalsIgnoreCase("Opening Balance")) {
            obltype2 = " from gl_closingbalance ";
        } else if (otype.equalsIgnoreCase("This Month")) {
            obltype2 = " from gl_openingbalance_detail ";
        } else if (otype.equalsIgnoreCase("To Date")) {
            obltype2 = " from gl_closingbalance ";
        }

        Statement stmdapat = con.createStatement();
        ResultSet setdapat = stmdapat.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype2 + " where (coacode like '21%' or coacode like '23%') " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setdapat.next()) {
            pendapatan = setdapat.getDouble("beza");
            // if(pendapatan<0)pendapatan*=-1;
        }

        Statement stmbelanja = con.createStatement();
        ResultSet setbelanja = stmbelanja.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype2 + " where ((coacode like '22%'  and coacode not like '2201%')  or coacode like '24%' or coacode like '29%' or coacode like '3104%') " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setbelanja.next()) {
            perbelanjaan = setbelanja.getDouble("beza");
            //if(perbelanjaan<0)perbelanjaan*=-1;
        }

        //test+=String.valueOf(perbelanjaan).concat("<br><br>");
        double re = pendapatan + perbelanjaan;

        double terkumpul = 0;
        setbelanja = stmbelanja.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as beza " + obltype2 + " where coacode = '3103' " + loccodex + " and concat(year,period)=concat('" + year + "','" + period + "')");
        if (setbelanja.next()) {
            re += setbelanja.getDouble("beza");
            //test+="dari opening "+String.valueOf(setbelanja.getDouble("beza"))+"<br><br>";
        }

        return re;
    }

    public static BsMaster generateBalanceSheet(BSParam b, LoginProfile log) throws Exception {

        Connection con = log.getCon();

        BsMaster mt = new BsMaster();

        List<BsItem> lt = new ArrayList();

        ResultSet rs = null;
        int inc = 0;
        double totalLvl2_cur = 0.0;
        double totalLvl2_pre = 0.0;
        PreparedStatement stmt = con.prepareStatement("select * from gl_config_balancesheet WHERE level = '1' order by code");
        //stmt.setString(1, "12");
        rs = stmt.executeQuery();
        while (rs.next()) {

            inc++;

            if (inc == 2) {

                BsItem m1 = new BsItem();
                m1.setChild(true);
                m1.setLevel(2);
                m1.setCoacode("0000");
                m1.setCoadescp(" ");
                m1.setCurrentAmount("");
                m1.setPrevAmount("");
                lt.add(m1);

                BalanceSheetDAO.setTotal1(totalLvl2_cur);
                BalanceSheetDAO.setTotalpre1(totalLvl2_pre);
                totalLvl2_cur = 0.0;

            }

            lt.add(getBsDetail(log, b, rs.getString("code"), rs.getString("descp"), rs.getString("level"), rs.getBoolean("reverse")));

            if (b.getVby() == 2 || b.getVby() == 3) {

                ResultSet rs1 = null;
                PreparedStatement stmt1 = con.prepareStatement("select * from gl_config_balancesheet WHERE level = '2' and id like '" + rs.getString("id") + "%'  order by code");
                //stmt1.setInt(1, 2);
                // stmt1.setString(2, rs.getString("code") + "%");
                rs1 = stmt1.executeQuery();
                while (rs1.next()) {

                    BsItem m2 = getBsDetail(log, b, rs1.getString("code"), rs1.getString("descp"), rs1.getString("level"), rs1.getBoolean("reverse"));
                    lt.add(m2);
                    totalLvl2_cur += ParseSafely.parseDoubleSafely(m2.getCurrentAmount());
                    totalLvl2_pre += ParseSafely.parseDoubleSafely(m2.getPrevAmount());
                    if (b.getVby() == 3) {

                        double totalLvl3_cur = 0.0;
                        double totalLvl3_pre = 0.0;
                        ResultSet rs2 = null;
                        PreparedStatement stmt2 = con.prepareStatement("select * from gl_config_balancesheet WHERE (level = ? or sub = ?) and id like ?  order by code");
                        stmt2.setInt(1, 3);
                        stmt2.setBoolean(2, true);
                        stmt2.setString(3, rs1.getString("id") + "%");
                        rs2 = stmt2.executeQuery();
                        while (rs2.next()) {

                            if (rs2.getBoolean("sub") && rs2.getString("level").equals("2")) {

                                List<BsItem> listAll = (List<BsItem>) getSubCode(log, rs2.getString("code"), b, rs2.getBoolean("reverse"));
                                for (BsItem j : listAll) {

                                    totalLvl3_cur += ParseSafely.parseDoubleSafely(j.getCurrentAmount());
                                    totalLvl3_pre += ParseSafely.parseDoubleSafely(j.getPrevAmount());
                                    lt.add(j);

                                }

                            } else if (rs2.getString("level").equals("3")) {

                                BsItem m1 = getBsDetail(log, b, rs2.getString("code"), rs2.getString("descp"), rs2.getString("level"), rs2.getBoolean("reverse"));
                                lt.add(m1);
                                totalLvl3_cur += ParseSafely.parseDoubleSafely(m1.getCurrentAmount());
                                totalLvl3_pre += ParseSafely.parseDoubleSafely(m1.getPrevAmount());

                            }

                            if (rs2.isLast()) {
                                BsItem m1 = new BsItem();
                                m1.setChild(true);
                                m1.setLevel(2);
                                m1.setCoacode("0000");
                                m1.setCoadescp("");
                                m1.setCurrentAmount("__________________");
                                m1.setPrevAmount("__________________");
                                lt.add(m1);

                                BsItem m5 = new BsItem();
                                m5.setChild(true);
                                m5.setLevel(2);
                                m5.setCoacode("0000");
                                m5.setCoadescp("TOTAL " + rs1.getString("descp"));
                                m5.setCurrentAmount(GeneralTerm.normalCredit(totalLvl3_cur));
                                m5.setPrevAmount(GeneralTerm.normalCredit(totalLvl3_pre));
                                lt.add(m5);
                            }

                        }
                        totalLvl2_cur += totalLvl3_cur;
                        totalLvl2_pre += totalLvl3_pre;
                        Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, "totalLvl2 - " + totalLvl2_cur + "** totalLvl3 - " + totalLvl3_cur);
                        rs2.close();
                        stmt2.close();

                    }

                    if (rs1.isLast()) {
                        BsItem m1 = new BsItem();
                        m1.setChild(true);
                        m1.setLevel(1);
                        m1.setCoacode("0000");
                        m1.setCoadescp("");
                        m1.setCurrentAmount("__________________");
                        m1.setPrevAmount("__________________");
                        lt.add(m1);

                        BsItem m4 = new BsItem();
                        m4.setChild(true);
                        m4.setLevel(1);
                        m4.setCoacode("0000");
                        m4.setCoadescp("TOTAL " + rs.getString("descp"));
                        m4.setCurrentAmount(GeneralTerm.normalCredit(totalLvl2_cur));
                        m4.setPrevAmount(GeneralTerm.normalCredit(totalLvl2_pre));
                        lt.add(m4);
                    }

                    BalanceSheetDAO.setTotal2(totalLvl2_cur);
                    BalanceSheetDAO.setTotalpre2(totalLvl2_pre);

                }

                rs1.close();
                stmt1.close();

            }

        }

        BsItem m1 = new BsItem();
        m1.setChild(true);
        m1.setLevel(1);
        m1.setCoacode("0000");
        m1.setCoadescp("");
        m1.setCurrentAmount("__________________");
        m1.setPrevAmount("__________________");
        lt.add(m1);
        
        BsItem m2 = new BsItem();
        m2.setChild(true);
        m2.setLevel(1);
        m2.setCoacode("0000");
        m2.setCoadescp("NET ASSETS ");
        m2.setCurrentAmount(GeneralTerm.normalCredit(BalanceSheetDAO.getTotal1() - BalanceSheetDAO.getTotal2()));
        m2.setPrevAmount(GeneralTerm.normalCredit(BalanceSheetDAO.getTotalpre1()- BalanceSheetDAO.getTotalpre2()));
        lt.add(m2);
        
        BsItem m3 = new BsItem();
        m3.setChild(true);
        m3.setLevel(1);
        m3.setCoacode("0000");
        m3.setCoadescp("");
        m3.setCurrentAmount("=================");
        m3.setPrevAmount("=================");
        lt.add(m3);

        rs.close();
        stmt.close();

        mt.setListItem(lt);

        return mt;

    }

    private static BsItem getBsDetail(LoginProfile log, BSParam b, String code, String descp, String level, boolean reverse) throws Exception {

        BsItem m = new BsItem();

        //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, "level - "+level+"** vby - "+b.getVby());
        int lvl = Integer.parseInt(level);
        String cAmt = "";
        String pAmt = "";

        if (b.getVby() != lvl) {
            m.setHeader(true);
            m.setLevel(lvl);
            cAmt = "";
            pAmt = "";
        } else {
            m.setHeader(false);
            m.setChild(true);
            m.setLevel(lvl);

            if (reverse) {

                cAmt = GeneralTerm.normalDebit(getTotal(log, b, code, "now", "00"));
                pAmt = GeneralTerm.normalDebit(getTotal(log, b, descp, "then", "00"));

            } else {

                cAmt = GeneralTerm.normalCredit(getTotal(log, b, code, "now", "00"));
                pAmt = GeneralTerm.normalCredit(getTotal(log, b, descp, "then", "00"));

            }

        }

        m.setCoacode(code);
        m.setCoadescp(descp);
        m.setCurrentAmount(cAmt);
        m.setPrevAmount(pAmt);

        return m;

    }

    private static List<BsItem> getSubCode(LoginProfile log, String code, BSParam b, boolean reverse) throws Exception {

        Connection con = log.getCon();

        List<BsItem> lt = new ArrayList();

        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement("select * from gl_openingbalance where coacode = ? group by sacode");

        stmt.setString(1, code);
        rs = stmt.executeQuery();

        while (rs.next()) {
            Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, b.getYear() + "- period=" + b.getPeriod() + " - " + rs.getString("sadesc"));
            double curAmount = getTotal(log, b, code, "now", rs.getString("sacode"));
            double prevAmount = getTotal(log, b, code, "then", rs.getString("sacode"));
            //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, ">>>>" + curAmount + "-" + prevAmount);
            if (curAmount != 0 || prevAmount != 0) {
                //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, ">>>>" + rs.getString("sadesc"));
                BsItem m1 = new BsItem();
                m1.setChild(true);
                m1.setLevel(3);
                m1.setCoacode(rs.getString("sacode"));
                m1.setCoadescp(rs.getString("sadesc"));

                if (reverse) {

                    m1.setCurrentAmount(GeneralTerm.normalDebit(curAmount));
                    m1.setPrevAmount(GeneralTerm.normalDebit(prevAmount));

                } else {

                    m1.setCurrentAmount(GeneralTerm.normalCredit(curAmount));
                    m1.setPrevAmount(GeneralTerm.normalCredit(prevAmount));

                }
                lt.add(m1);
            }

        }

        return lt;

    }

//    public static BsMaster getMaster_old(String year, String period, String otype, String vby, String code, LoginProfile log) throws Exception {
//
//        BsMaster mt = new BsMaster();
//
//        mt.setListItem(getCodeCluster(year, period, otype, vby, code, log));
//
//        double totalNow = 0.00;
//        double totalPrev = 0.00;
//
//        for (int i = 0; i < mt.getListItem().size(); i++) {
//            totalNow += mt.getListItem().get(i).getCurrentAmount();
//            totalPrev += mt.getListItem().get(i).getPrevAmount();
//        }
//
//        if (totalNow < 0) {
//            totalNow = totalNow * -1;
//        }
//
//        if (totalPrev < 0) {
//            totalPrev = totalPrev * -1;
//        }
//
//        mt.setClusterTotalNow(totalNow);
//        mt.setClusterTotalPrev(totalPrev);
//
//        return mt;
//
//    }
    private static boolean subExist(LoginProfile log, String code, String year, String period) throws Exception {

        boolean b = false;

        ResultSet rs = null;
        String ck = "00";

        PreparedStatement stmt = log.getCon().prepareStatement("select sacode from gl_openingbalance where year = ? and period = ? and coacode = ?");

        stmt.setString(1, year);
        stmt.setString(2, period);
        stmt.setString(3, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
            if (!ck.equals("00")) {
                b = true;
            } else {
                b = false;
            }
        }
        //Logger.getLogger(BalanceSheetDAO.class.getName()).log(Level.INFO, code + " - " + ck);
        if (ck.equals("00")) {
            b = false;
        }

        return b;
    }

}
