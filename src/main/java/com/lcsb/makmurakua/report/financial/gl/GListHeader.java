/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.gl;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class GListHeader {
    
    private String coacode;
    private String coadescp;
    private String sacode;
    private String sadesc;
    private String satype;
    private double balance;
    private List<GListDetail> listDetail;

    public List<GListDetail> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<GListDetail> listDetail) {
        this.listDetail = listDetail;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    
    
}
