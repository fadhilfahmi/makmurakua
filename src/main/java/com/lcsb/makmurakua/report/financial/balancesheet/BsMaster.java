/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.balancesheet;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class BsMaster {
    
    
    private List<BsItem> listItem;
    private double clusterTotalNow;
    private double clusterTotalPrev;

    public List<BsItem> getListItem() {
        return listItem;
    }

    public void setListItem(List<BsItem> listItem) {
        this.listItem = listItem;
    }

    public double getClusterTotalNow() {
        return clusterTotalNow;
    }

    public void setClusterTotalNow(double clusterTotalNow) {
        this.clusterTotalNow = clusterTotalNow;
    }

    public double getClusterTotalPrev() {
        return clusterTotalPrev;
    }

    public void setClusterTotalPrev(double clusterTotalPrev) {
        this.clusterTotalPrev = clusterTotalPrev;
    }

    
    
}
