/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.report.financial.PnL;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class PnLMaster {
    
    private List<PnLItem> listItem;
    private double clusterTotalNow;
    private double clusterTotalPrev;

    public List<PnLItem> getListItem() {
        return listItem;
    }

    public void setListItem(List<PnLItem> listItem) {
        this.listItem = listItem;
    }

    public double getClusterTotalNow() {
        return clusterTotalNow;
    }

    public void setClusterTotalNow(double clusterTotalNow) {
        this.clusterTotalNow = clusterTotalNow;
    }

    public double getClusterTotalPrev() {
        return clusterTotalPrev;
    }

    public void setClusterTotalPrev(double clusterTotalPrev) {
        this.clusterTotalPrev = clusterTotalPrev;
    }
    
    
    
}
