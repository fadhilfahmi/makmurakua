/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.financial.gl.post;

/**
 *
 * @author Dell
 */
public class Error {
    
    private Boolean error;
    private String errorID;
    private String errorDesc;
    private String errorItem;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getErrorItem() {
        return errorItem;
    }

    public void setErrorItem(String errorItem) {
        this.errorItem = errorItem;
    }
   
    
    public void setErrorID(String errorID){
        this.errorID = errorID;
    }
    
    public String getErrorID(){
        
        return errorID;
    }
    
    
  
    
}
