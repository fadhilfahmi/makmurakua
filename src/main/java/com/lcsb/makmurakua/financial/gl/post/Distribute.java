/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.financial.gl.post;

import java.util.List;

/**
 *
 * @author Dell
 */
public class Distribute {
    
    private boolean success;
    private Master master;
    private List<ErrorPost> listError;
    private String noteNo;

    public String getNoteNo() {
        return noteNo;
    }

    public void setNoteNo(String noteNo) {
        this.noteNo = noteNo;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<ErrorPost> getListError() {
        return listError;
    }

    public void setListError(List<ErrorPost> listError) {
        this.listError = listError;
    }
    
    
    
}
