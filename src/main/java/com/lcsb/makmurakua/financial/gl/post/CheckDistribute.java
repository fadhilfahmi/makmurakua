/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.financial.gl.post;

import java.util.List;

/**
 *
 * @author Dell
 */
public class CheckDistribute {
    
    private boolean check;
    private List<ErrorPost> error;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public List<ErrorPost> getError() {
        return error;
    }

    public void setError(List<ErrorPost> error) {
        this.error = error;
    }
    
    
    
}
