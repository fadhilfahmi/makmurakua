/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.financial.gl.post;

import java.util.List;

/**
 *
 * @author Dell
 */
public class Master {
    private String novoucher;
    private String tarikh;
    private String estatecode;
    private String estatename;
    private String year;
    private String period;
    private String source;
    private String vtype;
    private String debit;
    private String credit;
    private boolean checkOnly;
    private List<Item> listItem;
    private List<ErrorPost> listError;

    public List<Item> getListItem() {
        return listItem;
    }

    public void setListItem(List<Item> listItem) {
        this.listItem = listItem;
    }

    
    public List<ErrorPost> getListError() {
        return listError;
    }

    public void setListError(List<ErrorPost> listError) {
        this.listError = listError;
    }

    public String getNovoucher() {
        return novoucher;
    }

    public void setNovoucher(String novoucher) {
        this.novoucher = novoucher;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getVtype() {
        return vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }


    public boolean isCheckOnly() {
        return checkOnly;
    }

    public void setCheckOnly(boolean checkOnly) {
        this.checkOnly = checkOnly;
    }

   
    
    
}
