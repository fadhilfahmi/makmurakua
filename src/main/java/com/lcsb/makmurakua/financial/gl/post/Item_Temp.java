/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.financial.gl.post;

/**
 *
 * @author Dell
 */
public class Item_Temp {
     private String jid="";
    private String novoucher="";
    private String loclevel="";
    private String loccode="";
    private String locdesc="";
    private String cttype="";
    private String ctcode="";
    private String ctdesc="";
    private String coacode="";
    private String coadesc="";
    private String satype="";
    private String sacode="";
    private String sadesc="";
    private double debit=0;
    private double credit=0;
    private String remarks="";

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getNovoucher() {
        return novoucher;
    }

    public void setNovoucher(String novoucher) {
        this.novoucher = novoucher;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadesc() {
        return coadesc;
    }

    public void setCoadesc(String coadesc) {
        this.coadesc = coadesc;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    
    
}
