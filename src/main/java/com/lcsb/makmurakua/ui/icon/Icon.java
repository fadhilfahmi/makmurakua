/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.ui.icon;

/**
 *
 * @author fadhilfahmi
 */
public class Icon {
    
    public static String editIcon(){
         String icon = "<i class=\"fa fa-pencil\"></i>";
         return icon;
    }
    
    public static String deleteIcon(){
         String icon = "<i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i>";
         return icon;
    } 
    
    public static String addIcon(){
         String icon = "<i class=\"fa fa-plus-circle\"></i>";
         return icon;
    }
    
    public static String deductIcon(){
         String icon = "<i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>";
         return icon;
    } 
    
    
}
