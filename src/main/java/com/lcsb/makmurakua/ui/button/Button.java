/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.ui.button;

import com.lcsb.makmurakua.ui.icon.Icon;

/**
 *
 * @author Dell
 */
public class Button {
    
    public static String backButton(){
        
        String btn = "<button id=\"backto\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-arrow-circle-o-left\"></i>&nbsp;&nbsp;Back</button>";
        return btn;
        
    }
    
    public static String saveButton(){
        
        String btn = "<button id=\"savebutton\"  class=\"btn btn-success btn-sm\"><i class=\"fa fa-floppy-o\"></i>&nbsp;&nbsp;Save</button>";
        return btn;
        
    }
    
    public static String updateButton(String moduleid, String refer){
        
        String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs editmain\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></button>";
        return btn;
        
    }
    
    public static String noUpdateButton(String moduleid, String refer){
        
        String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs showNoEdit\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></button>";
        return btn;
        
    }
    
    public static String printButton(){
        
        String btn = "<button id=\"printbutton\" data-toggle=\"modal\"  class=\"btn btn-default btn-sm\"><i class=\"fa fa-print\"></i>&nbsp;&nbsp;Print</button>";
        return btn;
        
    }
    
    public static String goviewButton(){
        
        String btn = "<button id=\"goviewbutton\"  class=\"btn btn-primary btn-sm\"><i class=\"fa fa-arrow-circle-right\"></i>&nbsp;&nbsp;View</button>";
        return btn;
        
    }
    
    public static String viewlistButton(){
         String btn = "<button type=\"button\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder-open\"></i></button>";
         
         return btn;
    }
    
    public static String editlistButton(){
         String btn = "<button type=\"button\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-pencil\"></i></button>";
         
         return btn;
    }
    
    public static String deletelistButton(){
         String btn = "<button type=\"button\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash-o\"></i></button>";
         
         return btn;
    }
    
    public static String changePeriod(){
        String btn = "<button id=\"changeperiod\" class=\"btn btn-default btn-sm\"><i class=\"fa fa-calendar\"></i>&nbsp;&nbsp;Change Period</button>";
        
        return btn;
    }
    
    public static String addNewItem(){
        String btn = "<button type=\"button\" class=\"btn btn-default btn-xs\"><i class=\"fa fa-plus-circle\"></i></button>";
        
        return btn;
    }
    
     public static String editlistItem(){
         String btn = "<button type=\"button\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-pencil\"></i>&nbsp;Item</button>";
         
         return btn;
    }
     
    public static String viewGL(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs viewgl\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i> View in GL</button>";
         
         return btn;
    }
    
    public static String replicateButton(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs replicate\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-files-o\" aria-hidden=\"true\"></i> Replicate</button>";
         
         return btn;
    }
    
    public static String viewPrintedButton(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs viewvoucher\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-print\" aria-hidden=\"true\"></i></button>";
         
         return btn;
    }
    
    public static String cancelButton(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\" class=\"btn btn-default btn-xs cancel\" title=\"" + moduleid + "\" name=\"edit_st\"><i class=\"fa fa-ban\" aria-hidden=\"true\"></i></button>";
         
         return btn;
    } 
    
    public static String deleteMainButton(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\"  class=\"btn btn-danger btn-xs deletemain\" title=\"" + moduleid + "\" id=\"<%//= pvi.getRefer() %>\" name=\"delete_st\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>";;
         
         return btn;
    }
    
    public static String noDeleteMainButton(String moduleid, String refer){
         String btn = "<button id=\"" + refer + "\"  class=\"btn btn-danger btn-xs showNoEdit\" title=\"" + moduleid + "\" id=\"<%//= pvi.getRefer() %>\" name=\"delete_st\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>";;
         
         return btn;
    }
    
    public static String approveButton(String moduleid, String refer){
         String btn = "<button  class=\"btn btn-default btn-xs approve\" title=\"" + moduleid + "\" id=\"" + refer + "\"><i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i> Approve</button>";
         
         return btn;
    }
    
    public static String checkButton(String moduleid, String refer){
         String btn = "<button  class=\"btn btn-default btn-xs check\" title=\"" + moduleid + "\" id=\"" + refer + "\" name=\"addrefer\"><i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i> Check</button>";
         
         return btn;
    }
    
    public static String addSubEditListButton(String moduleid, String refer, String attrClass, String attrName, String name, String type){
        
        String icon = "";
        if(type.equals("edit")){
            icon = Icon.editIcon();
        }
        if(type.equals("delete")){
            icon = Icon.deleteIcon();
        }
        if(type.equals("add")){
            icon = Icon.addIcon();
        }
        if(type.equals("deduct")){
            icon = Icon.deductIcon();
        }
        
         String btn = "<button  class=\"btn btn-default btn-xs "+attrClass+"\" title=\""+ moduleid +"\" id=\""+ refer +"\" name=\""+attrName+"\">"+ icon +" "+name+"</button>";
         
         return btn;
    }
    
     
}
