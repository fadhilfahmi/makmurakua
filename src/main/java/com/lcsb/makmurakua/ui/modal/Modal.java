/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.ui.modal;

/**
 *
 * @author Dell
 */
public class Modal {
    
    public static String deleteModal(String moduleid, String referenceno){
         String modal;
        modal = "<div class=\"modal fade\" id=\"deleteModal\" role=\"dialog\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header modal-header-danger\">";
                modal += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button><h4 class=\"modal-title\">Delete Confirmation</h4></div><div class=\"modal-body\"><p>";
                modal += "Are you sure to delete this ";
                modal += "<strong>";
                modal += referenceno;
                modal += "</strong>";
                modal += "</p></div><div class=\"modal-footer\">";
                modal += "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>";
                modal += "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Yes</button>";
                modal += "</div></div></div></div>";
        return modal;
    }
    
}
