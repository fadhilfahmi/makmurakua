/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import com.lcsb.makmurakua.util.dao.ModuleDAO;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author fadhilfahmi
 */
public class SearchDAO {
    
    public static String searchInput(LoginProfile log,String keyword) throws Exception {
        
        String result = "";
        
        String inputType = analysisInput(log, keyword);
        
        if(inputType.equals("referinput")){
            
            Module mod = (Module) ModuleDAO.getModuleFromReferenceNo(log, keyword);
            
            result = pageEditList(mod.getModuleID());

            
        }
       
                
        
        
        return result;

    }

    public static String analysisInput(LoginProfile log,String keyword) throws Exception {
        
        String inputType = "default";
        
        if (keyword.length() == 15) {
            
            if(containABB(log, keyword.substring(0, 3))){
                inputType = "referinput";
            }
            
        }
        
        return inputType;

    }

    private static boolean containABB(LoginProfile log,String input) {
        
        boolean has = false;
        
        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT abb FROM sec_module WHERE abb <> '' AND abb = ?");
            stmt.setString(1, input);
            rs = stmt.executeQuery();
            if (rs.next()) {
                has = true;
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
        }
        
        return has;

    }
    
    private static String pageEditList(String moduleid){
        
        String page = "";
        
        if(moduleid.equals("020203")){
            page = "cb_official_edit_list";
        }
        
        
        return page;
    }

}
