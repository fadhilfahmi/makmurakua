/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.model.ConnectionModel;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class AccountingPeriod {

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public static Date convertStringtoDate(String strDate) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        if (strDate.equals("") || strDate.equals("0000-00-00") || strDate == null || strDate == "null" || strDate.equals("null") || strDate.equals(null)) {
            Date date = formatter.parse("0000-00-00");
            return date;
        } else {
            Date date = formatter.parse(strDate);
            return date;
        }

    }

    public static String fullDateMonth(String strDate) throws ParseException {
        if(strDate.equals("") || strDate == null || strDate == "null"){
            strDate = "0000-00-00";
        }
        
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = format1.parse(strDate);
        return format2.format(date);

    }

    public static String gst03Formatter(Date olddate) throws ParseException {

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");//dd/MM/yyyy
        if (olddate == null || olddate.equals(null)) {
            return "00-00-0000";
        } else {
            String strDate = sdfDate.format(olddate);
            return strDate;
        }

    }

    public static String gst03FormatterNormal(Date olddate) throws ParseException {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        if (olddate == null || olddate.equals(null)) {
            return "00-00-0000";
        } else {
            String strDate = sdfDate.format(olddate);
            return strDate;
        }

    }

    public static String convertDatetoString(Date olddate) throws ParseException {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        if (olddate == null || olddate.equals(null)) {
            return "00-00-0000";
        } else {
            String strDate = sdfDate.format(olddate);
            return strDate;
        }

    }

    public static String getStartofPeriod(LoginProfile log, String year, String period) throws SQLException, Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        String startperiod = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select startperiod from gl_altaccountingperiod where year='" + year + "' and period='" + period + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            startperiod = String.valueOf(rs.getDate("startperiod"));
        }
        
        rs.close();

        return startperiod;

    }

    public static String getEndofPeriod(LoginProfile log, String year, String period) throws SQLException, Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        String endPeriod = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select endperiod from gl_altaccountingperiod where year='" + year + "' and period='" + period + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            endPeriod = String.valueOf(rs.getDate("endperiod"));
        }
        
        rs.close();

        return endPeriod;

    }

    public static String getCurYear(LoginProfile log) throws SQLException, Exception {

        String year = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select year from gl_altaccountingperiod where current='Yes'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            year = rs.getString("year");
        }
        
        rs.close();

        return year;
    }

    public static String getCurPeriod(LoginProfile log) throws SQLException, Exception {

        String period = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select period from gl_altaccountingperiod where current='Yes'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            period = rs.getString("period");
        }
        
        rs.close();

        return period;
    }
    
    public static String getCurPeriod2Digit(LoginProfile log) throws SQLException, Exception {

        String period = getCurPeriod(log);
        
        if(period.length()==1){
            period = "0"+period;
        }

        return period;
    }
    public static String getMonthofPeriod(String period) {

        String month = "";
        if (period.equalsIgnoreCase("1")) {
            month = "January";
        } else if (period.equalsIgnoreCase("2")) {
            month = "February";
        } else if (period.equalsIgnoreCase("3")) {
            month = "March";
        } else if (period.equalsIgnoreCase("4")) {
            month = "April";
        } else if (period.equalsIgnoreCase("5")) {
            month = "May";
        } else if (period.equalsIgnoreCase("6")) {
            month = "June";
        } else if (period.equalsIgnoreCase("7")) {
            month = "July";
        } else if (period.equalsIgnoreCase("8")) {
            month = "August";
        } else if (period.equalsIgnoreCase("9")) {
            month = "September";
        } else if (period.equalsIgnoreCase("10")) {
            month = "October";
        } else if (period.equalsIgnoreCase("11")) {
            month = "November";
        } else if (period.equalsIgnoreCase("12")) {
            month = "December";
        }

        return month;
    }

    public static String getCurPeriodByCurrentDate() throws SQLException, Exception {

        String date = getCurrentTimeStamp();

        String period = date.substring(5, 7);

        if (period.substring(0, 1).equals("0")) {
            period = period.substring(1, 2);
        }

        return period;
    }

    public static String getCurYearByCurrentDate() throws SQLException, Exception {

        String date = getCurrentTimeStamp();

        String year = date.substring(0, 4);

        return year;
    }
    
    public static String getPeriodByDate(String date) throws SQLException, Exception {


        String period = date.substring(5, 7);

        if (period.substring(0, 1).equals("0")) {
            period = period.substring(1, 2);
        }

        return period;
    }
    
    public static int getDayPerMonth(int period, int year){
        int day = 0;
        
        if(period == 1 || period == 3 || period == 5 || period == 7 || period == 8 || period == 10 || period == 12){
            day = 31;
        }else if(period == 4 || period == 6 || period == 9 || period == 11){
            day = 30;
        }else if(period == 2){
            
            if((year/4) == 0){
                day = 29;
            }else{
                day = 28;
            }
            
        }
        
        return day;
    }

}
