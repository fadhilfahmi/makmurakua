/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.general;

/**
 *
 * @author Dell
 */
public class ErrorIO {
    private static String code;
    private static String type;
    private static String error;
    private static String path;
    private static String errorCode;
    private static String moduleid;
    private static String process;
    private static String loggedbyid;
    private static String loggedbyname;
    private static String comcode;

    public static String getComcode() {
        return comcode;
    }

    public static void setComcode(String comcode) {
        ErrorIO.comcode = comcode;
    }

    public static String getErrorCode() {
        return errorCode;
    }

    public static void setErrorCode(String errorCode) {
        ErrorIO.errorCode = errorCode;
    }

    public static String getModuleid() {
        return moduleid;
    }

    public static void setModuleid(String moduleid) {
        ErrorIO.moduleid = moduleid;
    }

    public static String getProcess() {
        return process;
    }

    public static void setProcess(String process) {
        ErrorIO.process = process;
    }

    public static String getLoggedbyid() {
        return loggedbyid;
    }

    public static void setLoggedbyid(String loggedbyid) {
        ErrorIO.loggedbyid = loggedbyid;
    }

    public static String getLoggedbyname() {
        return loggedbyname;
    }

    public static void setLoggedbyname(String loggedbyname) {
        ErrorIO.loggedbyname = loggedbyname;
    }

    public static String getCode() {
        return code;
    }

    public static void setCode(String code) {
        ErrorIO.code = code;
    }

    public static String getType() {
        return type;
    }

    public static void setType(String type) {
        ErrorIO.type = type;
    }

    public static String getError() {
        return error;
    }

    public static void setError(String error) {
        ErrorIO.error = error;
    }

    public static String getPath() {
        return path;
    }

    public static void setPath(String path) {
        ErrorIO.path = path;
    }

    
    
    
}
