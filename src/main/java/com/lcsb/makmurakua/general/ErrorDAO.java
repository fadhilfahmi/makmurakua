/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ErrorDAO {
    
    public static void logError(LoginProfile log) throws Exception{
        
        
        try {
            
            String q = ("insert into fms_general.sys_error(errorcode, errordesc, loggedbyid, loggedbyname, moduleid, process,comcode,date,time) values (?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, ErrorIO.getCode());
            ps.setString(2, ErrorIO.getError());
            ps.setString(3, ErrorIO.getLoggedbyid());
            ps.setString(4, ErrorIO.getLoggedbyname());
            ps.setString(5, ErrorIO.getModuleid());
            ps.setString(6, ErrorIO.getProcess());
            ps.setString(7, ErrorIO.getComcode());
            ps.setString(8, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(9, AccountingPeriod.getCurrentTime());
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
}
