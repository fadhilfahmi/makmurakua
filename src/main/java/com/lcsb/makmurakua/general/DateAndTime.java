/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author fadhilfahmi
 */
public class DateAndTime {

    public static String addTime(String time) throws ParseException {//add 8 hours to palarel with server time

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date d = df.parse(time);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        //cal.add(Calendar.MINUTE, 475);
        return df.format(cal.getTime());
        
    }

}
