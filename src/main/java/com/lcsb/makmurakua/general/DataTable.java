/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import com.lcsb.makmurakua.util.model.ListTable;
import com.lcsb.makmurakua.util.model.Module;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class DataTable {
    
    private Module module;
    private List<ListTable> listTable;
    private String renderData;
    private String viewBy;
    private String year;
    private String period;
    private String moduleTypeID;

    public String getModuleTypeID() {
        return moduleTypeID;
    }

    public void setModuleTypeID(String moduleTypeID) {
        this.moduleTypeID = moduleTypeID;
    }

    public String getViewBy() {
        return viewBy;
    }

    public void setViewBy(String viewBy) {
        this.viewBy = viewBy;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRenderData() {
        return renderData;
    }

    public void setRenderData(String renderData) {
        this.renderData = renderData;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public List<ListTable> getListTable() {
        return listTable;
    }

    public void setListTable(List<ListTable> listTable) {
        this.listTable = listTable;
    }
    
    
    
    
    
}
