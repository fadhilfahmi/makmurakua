/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.general;

import com.lcsb.makmurakua.model.development.SecModule;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Dell
 */
public class GeneralTerm {

    public String getAccountName(LoginProfile log, String code) throws SQLException, Exception {
        String descp = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from chartofacccount where code=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            descp = rs.getString("descp");
        }

        return descp;

    }

    public static String normalCredit(double val) {
        DecimalFormat df = new DecimalFormat("###,###,###,##0.00");
        String tipu = df.format(val);
        if (tipu.equalsIgnoreCase("0.00")) {
            return tipu;
        } else if (val < 0) {
            return "(".concat(df.format(-1 * val)).concat(")");
        } else {
            return df.format(val);
        }
    }

    public static String normalDebit(double val) {
        DecimalFormat df = new DecimalFormat("###,###,###,##0.00");
        String tipu = df.format(val);
        if (tipu.equalsIgnoreCase("0.00")) {
            return tipu;
        } else if (val < 0) {
            return df.format(val * -1);
        } else {
            return "(".concat(df.format(val)).concat(")");
        }

    }

    public static Double amountFormattoSave(String val) throws ParseException {

        //Double d = Double.parseDouble(val.replaceAll(",", ""));
        boolean hasParens = false;
        String s = val;
        s = s.replaceAll(",", "");

        if (s.contains("(")) {
            s = s.replaceAll("[()]", "");
            hasParens = true;
        }

        double number = Double.parseDouble(s);

        if (hasParens) {
            number = -number;
        }

        return number;

    }

    public static String amountFormattoSaveString(String val) throws ParseException {

        String d = val.replaceAll(",", "");

        return d;

    }

    public static String getEstateName(Connection con, String estatecode) throws Exception {

        String descp = "";
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM estateinfo WHERE estatecode = ?");
        stmt.setString(1, estatecode);
        rs = stmt.executeQuery();
        if (rs.next()) {
            descp = rs.getString("estatedescp");
        }

        rs.close();
        stmt.close();

        return descp;

    }

    public static Date getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date datex = new Date();
        return datex;
    }

    public static String currencyFormat(Double value) {
        DecimalFormat df = new DecimalFormat("###,###,###,##0.00");

        String output = df.format(value);

        return output;

    }

    public static double PrecisionDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double number = Math.ceil(value * 100);

        double output = Double.parseDouble(df.format(number / 100));

        return output;

    }

    public static double twoDecimalDouble(Double value) {
        DecimalFormat df = new DecimalFormat("###########0.00");

        double output = Double.parseDouble(df.format(value));

        return output;

    }

    public static String getSelected(String a, String b) {
        String selected = "";
        if (a.equals(b)) {
            selected = "selected";
        }
        return selected;

    }

    public static String getStatusLabel(String status) throws Exception {

        String label = "<span class=\"label label-primary\">Preparing</span>";

        if (status.equals("Checked")) {
            label = "<span class=\"label label-warning\">Checked</span>";
        }

        if (status.equals("Approved")) {
            label = "<span class=\"label label-success\">Approve</span>";
        }

        if (status.equals("JV Created")) {
            label = "<span class=\"label label-success\">JV Created</span>";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (status.equals("Canceled")) {
            label = "<span class=\"label label-default\">Canceled</span>";
        }

        return label;

    }

    public static JSONArray getModuleBreadcrumb(LoginProfile log, String moduleid) throws Exception {

        Connection con = log.getCon();
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM sec_module where moduleid = SUBSTRING(?,1,2)");
        stmt.setString(1, moduleid);
        ResultSet rs1 = stmt.executeQuery();

        if (rs1.next()) {
            JSONObject jo = new JSONObject();

            jo.put("moduleid", rs1.getString("moduleid"));
            jo.put("moduledesc", rs1.getString("moduledesc"));

            array.add(jo);
            jo = null;
        }

        PreparedStatement stmt2 = con.prepareStatement("SELECT * FROM sec_module where moduleid = SUBSTRING(?,1,4)");
        stmt2.setString(1, moduleid);
        ResultSet rs2 = stmt2.executeQuery();

        if (rs2.next()) {
            JSONObject jo = new JSONObject();

            jo.put("moduleid", rs2.getString("moduleid"));
            jo.put("moduledesc", rs2.getString("moduledesc"));

            array.add(jo);
            jo = null;
        }

        PreparedStatement stmt3 = con.prepareStatement("SELECT * FROM sec_module where moduleid = ?");
        stmt3.setString(1, moduleid);
        ResultSet rs3 = stmt3.executeQuery();

        if (rs3.next()) {
            JSONObject jo = new JSONObject();

            jo.put("moduleid", rs3.getString("moduleid"));
            jo.put("moduledesc", rs3.getString("moduledesc"));

            array.add(jo);
            jo = null;
        }

        return array;

    }

    public static SecModule getType(LoginProfile log, String refer) throws Exception {

        SecModule sc = new SecModule();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM sec_module WHERE abb like '%" + refer.substring(0, 3) + "%'");
            //Logger.getLogger(GeneralTerm.class.getName()).log(Level.INFO, "-----------" + "SELECT * FROM sec_module WHERE abb='%"+refer.substring(0, 3)+"%'");
            //stmt.setString(1, "%"+refer.substring(0, 3)+"%");
            rs = stmt.executeQuery();
            if (rs.next()) {
                sc = getSecModule(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sc;
    }

    private static SecModule getSecModule(ResultSet rs) throws SQLException {
        SecModule sc = new SecModule();

        sc.setAbb(rs.getString("abb"));
        sc.setMaintable(rs.getString("maintable"));
        sc.setReferidMaster(rs.getString("referid_master"));
        sc.setModuleid(rs.getString("moduleid"));
        sc.setModuledesc(rs.getString("moduledesc"));
        sc.setApprove(rs.getString("approve"));
        sc.setCheck(rs.getString("check"));
        sc.setPost(rs.getString("post"));

        return sc;
    }

    public static String capitalizeFirstLetter(String value) {

        String newstring = "";

        if (value.equals("") || value == null || value == "null") {

        } else {
            
            String origin = value.toLowerCase();
            // Convert String to char array.
            char[] array = origin.toCharArray();
            // Modify first element in array.
            array[0] = Character.toUpperCase(array[0]);

            for (int i = 1; i < array.length; i++) {
                if (Character.isWhitespace(array[i - 1])) {
                    array[i] = Character.toUpperCase(array[i]);
                }
            }
            
            newstring = new String(array);
        }

        // Return string.
        return newstring;

    }
    
    public static String replaceString(String origin, String toReplace){
        
        String replaced = "";
        
        
        return replaced;
        
    }

}
