/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.dashboard;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "dashboard_donut_usage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DashboardDonutUsage.findAll", query = "SELECT d FROM DashboardDonutUsage d")
    , @NamedQuery(name = "DashboardDonutUsage.findById", query = "SELECT d FROM DashboardDonutUsage d WHERE d.id = :id")
    , @NamedQuery(name = "DashboardDonutUsage.findByUserid", query = "SELECT d FROM DashboardDonutUsage d WHERE d.userid = :userid")
    , @NamedQuery(name = "DashboardDonutUsage.findByRefer", query = "SELECT d FROM DashboardDonutUsage d WHERE d.refer = :refer")
    , @NamedQuery(name = "DashboardDonutUsage.findByDate", query = "SELECT d FROM DashboardDonutUsage d WHERE d.date = :date")
    , @NamedQuery(name = "DashboardDonutUsage.findByTime", query = "SELECT d FROM DashboardDonutUsage d WHERE d.time = :time")
    , @NamedQuery(name = "DashboardDonutUsage.findByModuleid", query = "SELECT d FROM DashboardDonutUsage d WHERE d.moduleid = :moduleid")
    , @NamedQuery(name = "DashboardDonutUsage.findByType", query = "SELECT d FROM DashboardDonutUsage d WHERE d.type = :type")})
public class DashboardDonutUsage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "userid")
    private String userid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "time")
    private String time;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "moduleid")
    private String moduleid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "type")
    private String type;

    public DashboardDonutUsage() {
    }

    public DashboardDonutUsage(Integer id) {
        this.id = id;
    }

    public DashboardDonutUsage(Integer id, String userid, String refer, String date, String time, String moduleid, String type) {
        this.id = id;
        this.userid = userid;
        this.refer = refer;
        this.date = date;
        this.time = time;
        this.moduleid = moduleid;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DashboardDonutUsage)) {
            return false;
        }
        DashboardDonutUsage other = (DashboardDonutUsage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.dashboard.DashboardDonutUsage[ id=" + id + " ]";
    }
    
}
