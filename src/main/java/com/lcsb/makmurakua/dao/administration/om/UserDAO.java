/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.dao.administration.om;

import com.lcsb.makmurakua.general.AccountingPeriod;
import com.lcsb.makmurakua.model.administration.om.UserAccount;
import com.lcsb.makmurakua.model.administration.om.UserDashboardModuleSummary;
import com.lcsb.makmurakua.model.administration.om.UserFunction;
import com.lcsb.makmurakua.util.dao.ModuleDAO;
import com.lcsb.makmurakua.util.model.ListTable;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
public class UserDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("040101");
        mod.setModuleDesc("User Account Information");
        mod.setMainTable("om_user_account");
        mod.setReferID_Master("id");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"id", "user", "staff_id", "staff_name", "level", "level_id"};
        String title_name[] = {"ID", "User Name", "Staff ID", "Staff Name", "Level", "level_id"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void saveData(LoginProfile log, UserAccount item) throws Exception {

        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {

            String q = ("insert into om_user_account(user, password, staff_id, staff_name, level,level_id) values (?,password(?),?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getUser());
            ps.setString(2, item.getPassword());
            ps.setString(3, item.getStaffId());
            ps.setString(4, item.getStaffName());
            ps.setString(5, item.getLevel());
            ps.setInt(6, item.getLevelId());

            ps.executeUpdate();
            ps.close();

            UserDAO.generateAccess(log, item.getStaffId());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_1 = "delete from om_user_account where staff_id = ?";
            try (PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1)) {
                ps_1.setString(1, no);
                ps_1.executeUpdate();
            }

            String deleteQuery_2 = "delete from om_user_access where user_id = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }

    public static void updateItem(LoginProfile log, UserAccount item) throws Exception {

        try {
            String q = ("UPDATE om_user_account SET user=?, password=password(?), staff_id=?, staff_name=?, level=?  WHERE id = '" + item.getId() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getUser());
            ps.setString(2, item.getPassword());
            ps.setString(3, item.getStaffId());
            ps.setString(4, item.getStaffName());
            ps.setString(5, item.getLevel());

            Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static UserAccount getInfo(LoginProfile log, String no) throws SQLException, Exception {
        UserAccount c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_account where id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    public static UserAccount getInfoByStaffID(LoginProfile log, String no) throws SQLException, Exception {
        UserAccount c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_account where staff_id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    private static UserAccount getInfo(ResultSet rs) throws SQLException {
        UserAccount g = new UserAccount();
        g.setId(rs.getInt("id"));
        g.setLevel(rs.getString("level"));
        g.setLevelId(rs.getInt("level_id"));
        g.setPassword(rs.getString("password"));
        g.setStaffId(rs.getString("staff_id"));
        g.setStaffName(rs.getString("staff_name"));
        g.setUser(rs.getString("user"));

        return g;
    }

    public static List<UserFunction> getFunction(LoginProfile log) throws SQLException, Exception {
        List<UserFunction> c;
        c = new ArrayList();
        Statement stmt = null;

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM om_user_function");

            while (rs.next()) {
                c.add(getFunction(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static UserFunction getFunction(ResultSet rs) throws SQLException {
        UserFunction g = new UserFunction();
        g.setId(rs.getInt("id"));
        g.setFunction(rs.getString("function"));
        g.setFunctionId(rs.getString("function_id"));

        return g;
    }

    public static String getAccess(LoginProfile log, String userID, String functionID) throws SQLException, Exception {

        ResultSet rs = null;
        String checked = "";

        try {
            boolean access = false;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_access where user_id= ? and function_id = ?");
            stmt.setString(1, userID);
            stmt.setString(2, functionID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                access = rs.getBoolean("access");
            }

            if (access) {
                checked = "checked";
            } else {
                checked = "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checked;
    }

    public static boolean getDashboardAccess(LoginProfile log, String userID, String moduleID) throws SQLException, Exception {

        ResultSet rs = null;
        String checked = "";
        boolean access = false;

        try {

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and module_id = ?");
            stmt.setString(1, userID);
            stmt.setString(2, moduleID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                access = rs.getBoolean("dashboard");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return access;
    }

    public static void generateAccess(LoginProfile log, String userID) throws Exception {

        try {

            List<UserFunction> listAll = (List<UserFunction>) UserDAO.getFunction(log);
            for (UserFunction j : listAll) {

                String q = ("insert into om_user_access(user_id, function_id, access) values (?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, userID);
                ps.setString(2, j.getFunctionId());
                ps.setBoolean(3, false);
                ps.executeUpdate();
                ps.close();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void clearAccess(LoginProfile log, String userID) throws Exception {

        {
            try {
                String q = ("UPDATE om_user_access SET access=?  WHERE user_id = '" + userID + "' ");

                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setBoolean(1, false);
                Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();
                ps.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    public static void updateAccess(LoginProfile log, String userID, String[] functionID) throws Exception {

        Logger.getLogger(UserDAO.class.getName()).log(Level.INFO, "-------------------------asdsad----------" + String.valueOf(functionID));
        UserDAO.clearAccess(log, userID);

        if (functionID != null) {
            for (int i = 0; i < functionID.length; i++) {
                try {
                    String q = ("UPDATE om_user_access SET access=?  WHERE user_id = '" + userID + "' and function_id = '" + functionID[i] + "'");

                    PreparedStatement ps = log.getCon().prepareStatement(q);
                    ps.setBoolean(1, true);
                    Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();
                    ps.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public static List<Module> getModuleAccess(LoginProfile log, int lvl, String module, String userID) throws Exception {

        Statement stmt = null;
        List<Module> Modu;
        Modu = new ArrayList();

        String q = "";
        if (lvl == 2) {
            q = "";
        } else if (lvl > 2) {
            q = "and moduleid like '" + module + "%'";
        }
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select *,COALESCE((SELECT access FROM om_user_module WHERE module_id = moduleid and user_id = '" + userID + "'),0) as acc from sec_module where LENGTH(RTRIM(moduleid)) = " + lvl + " " + q + "  order by sortid,moduleid");

            while (rs.next()) {
                Modu.add(getModule(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Modu;
    }

    private static Module getModule(ResultSet rs) throws SQLException {
        Module mod = new Module();
        mod.setModuleID(rs.getString("moduleid"));
        mod.setModuleDesc(rs.getString("moduledesc"));
        mod.setHyperlink(rs.getString("hyperlink"));
        mod.setFinalLevel(rs.getString("finallevel"));
        mod.setDisable(rs.getString("disable"));
        mod.setMainTable(rs.getString("maintable"));
        mod.setSubTable(rs.getString("subtable"));
        mod.setAbb(rs.getString("abb"));
        mod.setReferID_Master(rs.getString("referid_master"));
        mod.setReferID_Sub(rs.getString("referid_sub"));
        mod.setAccess(rs.getBoolean("acc"));
        return mod;
    }

    public static void checkModule(LoginProfile log, String userID, String moduleID) throws Exception {

        try {

            if (moduleID.length() == 6) {
                String qy = "";
                int x = 0;
                int y = 0;
                ResultSet rs = null;
                PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and module_id = ?");
                stmt.setString(1, userID);
                stmt.setString(2, moduleID.substring(0, 2));
                rs = stmt.executeQuery();
                if (rs.next()) {
                    x++;

                }

                ResultSet rs1 = null;
                PreparedStatement stmt1 = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and module_id = ?");
                stmt1.setString(1, userID);
                stmt1.setString(2, moduleID.substring(0, 4));
                rs1 = stmt1.executeQuery();
                if (rs1.next()) {
                    y++;

                }

                if (x == 0) {
                    qy = ("insert into om_user_module(user_id, module_id, module_desc, access) values (?,?,?,?)");
                    PreparedStatement ps = log.getCon().prepareStatement(qy);
                    ps.setString(1, userID);
                    ps.setString(2, moduleID.substring(0, 2));
                    ps.setString(3, "");
                    ps.setBoolean(4, true);
                    ps.executeUpdate();
                    ps.close();
                }

                if (y == 0) {
                    qy = ("insert into om_user_module(user_id, module_id, module_desc, access) values (?,?,?,?)");
                    PreparedStatement ps = log.getCon().prepareStatement(qy);
                    ps.setString(1, userID);
                    ps.setString(2, moduleID.substring(0, 4));
                    ps.setString(3, "");
                    ps.setBoolean(4, true);
                    ps.executeUpdate();
                    ps.close();
                }
            }
            String q = ("insert into om_user_module(user_id, module_id, module_desc, access) values (?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, userID);
            ps.setString(2, moduleID);
            ps.setString(3, "");
            ps.setBoolean(4, true);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void uncheckModule(LoginProfile log, String userID, String moduleID) throws Exception {

        try {

            int x = 0;
            int y = 0;
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and module_id like '" + moduleID.substring(0, 4) + "%' and module_id <> '" + moduleID.substring(0, 4) + "'");
            stmt.setString(1, userID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                x++;

            }

            rs.close();

            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and module_id like '" + moduleID.substring(0, 2) + "%' and module_id <> '" + moduleID.substring(0, 2) + "'");
            stmt1.setString(1, userID);
            rs1 = stmt.executeQuery();
            while (rs1.next()) {
                y++;

            }

            rs.close();

            if (x < 2) {
                String deleteQuery_1 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1)) {
                    ps_1.setString(1, userID);
                    ps_1.setString(2, moduleID.substring(0, 4));
                    ps_1.executeUpdate();
                    ps_1.close();
                }

                String deleteQuery_2 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                    ps_2.setString(1, userID);
                    ps_2.setString(2, moduleID);
                    ps_2.executeUpdate();
                    ps_2.close();
                }
            } else {

                String deleteQuery_1 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1)) {
                    ps_1.setString(1, userID);
                    ps_1.setString(2, moduleID);
                    ps_1.executeUpdate();
                    ps_1.close();
                }

            }

            if (y < 2 && x < 2) {
                String deleteQuery_1 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1)) {
                    ps_1.setString(1, userID);
                    ps_1.setString(2, moduleID.substring(0, 2));
                    ps_1.executeUpdate();
                    ps_1.close();
                }
                String deleteQuery_2 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                    ps_2.setString(1, userID);
                    ps_2.setString(2, moduleID);
                    ps_2.executeUpdate();
                    ps_2.close();
                }
            } else {
                String deleteQuery_1 = "delete from om_user_module where user_id = ? and module_id = ?";
                try (PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1)) {
                    ps_1.setString(1, userID);
                    ps_1.setString(2, moduleID);
                    ps_1.executeUpdate();
                    ps_1.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static JSONArray getMonthActivityx() {

        JSONArray array = new JSONArray();

        JSONObject jo = new JSONObject();

        jo.put("year", "2017");
        jo.put("monthx", "8");

        array.add(jo);
        //out.println(jo);
        jo = null;

        return array;

    }

    public static JSONArray getMonthActivity(LoginProfile log) throws Exception {

        JSONArray array = new JSONArray();
        

        String year = AccountingPeriod.getCurrentTimeStamp().substring(0,4);
        String period = AccountingPeriod.getCurrentTimeStamp().substring(5,7);

        for (int i = 1; i <= AccountingPeriod.getDayPerMonth(Integer.parseInt(period), Integer.parseInt(year)); i++) {
            JSONObject jo = new JSONObject();
            String j = String.valueOf(i);

            if (j.length() == 1) {
                j = "0" + j;
            }
            String dt = year + "-" + period + "-" + j;
            jo.put("year", year);
            jo.put("month", period);
            jo.put("total", getUserLoginPerday(log, dt));
            jo.put("totaltrans", getTransactionPerday(log, dt));
            array.add(jo);
            jo = null;
        }

        return array;

    }

    public static int getUserLoginPerday(LoginProfile log, String date) throws SQLException {

        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM sys_log_login where date= ?");

        stmt.setString(1, date);
        //Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        rs.close();
        stmt.close();
        return cnt;
    }

    public static int getUserLoginPerMonth(LoginProfile log, String date) throws SQLException {

        String concatDate = date.substring(0, 7);
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM sys_log_login where date like '" + concatDate + "%'");

        //stmt.setString(1, date);
        Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, concatDate);
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        rs.close();
        stmt.close();
        return cnt;
    }

    private static int getTransactionPerday(LoginProfile log, String date) throws SQLException {

        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM sys_log where date= ?");

        stmt.setString(1, date);
        //Logger.getLogger(UserAccount.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        rs.close();
        stmt.close();
        return cnt;
    }

    public static JSONArray getDonutChart(LoginProfile log) throws Exception {

        JSONArray array = new JSONArray();

        ResultSet rs = null;
        String moduleid = "";
        boolean access = false;

        try {

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM om_user_module where user_id= ? and dashboard = ?");
            stmt.setString(1, log.getUserID());
            stmt.setBoolean(2, true);
            rs = stmt.executeQuery();
            while (rs.next()) {
                moduleid = rs.getString("module_id");

                JSONObject jo = new JSONObject();

                jo.put("moduleid", moduleid);
                jo.put("moduledesc", ModuleDAO.getModule(log, moduleid).getModuleDesc());
                jo.put("counter_pre", UserDAO.getStatusCounter(log, moduleid, "prepare"));
                jo.put("counter_che", UserDAO.getStatusCounter(log, moduleid, "check"));
                jo.put("counter_app", UserDAO.getStatusCounter(log, moduleid, "approve"));
                array.add(jo);
                jo = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return array;

    }

    private static String getStatusQuery(LoginProfile log, String status, String moduleid, Module mod) throws Exception {

        String q = "";

        switch (status) {
            case "check":
                q = mod.getCheck() + " <> '' and " + mod.getApprove() + " = ''";
                break;
            case "approve":
                q = mod.getCheck() + " <> '' and " + mod.getApprove() + " <> ''";
                break;
            case "prepare":
                q = mod.getCheck() + " = '' and " + mod.getPost() + " <> 'cancel'";
                break;
            default:
                break;
        }

        return q;

    }

    public static int getStatusCounter(LoginProfile log, String moduleid, String status) throws Exception {

        int cnt = 0;
        ResultSet rs = null;
        Module mod = (Module) ModuleDAO.getModule(log, moduleid);

        String q = getStatusQuery(log, status, moduleid, mod);

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from " + mod.getMainTable() + "  where year = ? and period = ? and  " + q);

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static List<UserDashboardModuleSummary> getSummaryModule(LoginProfile log, String moduleid, String status) throws SQLException, Exception {
        List<UserDashboardModuleSummary> c;
        c = new ArrayList();
        
        Module mod = (Module) ModuleDAO.getModule(log, moduleid);

        try {
            ResultSet rs = null;
            
            String stat = "";
            String actionButton = "";
            String attrButton = "";
            
            switch (status) {
            case "Prepared":
                stat = "prepare";
                actionButton = "Check";
                break;
            case "Checked":
                stat = "check";
                actionButton = "Approve";
                break;
            case "Approved":
                stat = "approve";
                actionButton = "Approved";
                attrButton = "disabled";
                break;
            default:
                break;
        }

            String q = getStatusQuery(log, stat, moduleid, mod);

            PreparedStatement stmt = log.getCon().prepareStatement("select "+mod.getReferID_Master()+" from " + mod.getMainTable() + "  where year = ? and period >= ? and  " + q);

            stmt.setString(1, AccountingPeriod.getCurYear(log));
            stmt.setString(2, AccountingPeriod.getCurPeriod(log));
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                UserDashboardModuleSummary u = new UserDashboardModuleSummary();
                u.setRefer(rs.getString(1));
                u.setStatus(status);
                u.setActButton(actionButton);
                u.setAttrButton(attrButton);
                
                c.add(u);
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

}
