/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_cashrequest_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcCashrequestDetail.findAll", query = "SELECT a FROM AcCashrequestDetail a")
    , @NamedQuery(name = "AcCashrequestDetail.findByReferno", query = "SELECT a FROM AcCashrequestDetail a WHERE a.referno = :referno")
    , @NamedQuery(name = "AcCashrequestDetail.findByRefer", query = "SELECT a FROM AcCashrequestDetail a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcCashrequestDetail.findByEstatecode", query = "SELECT a FROM AcCashrequestDetail a WHERE a.estatecode = :estatecode")
    , @NamedQuery(name = "AcCashrequestDetail.findByEstatedescp", query = "SELECT a FROM AcCashrequestDetail a WHERE a.estatedescp = :estatedescp")
    , @NamedQuery(name = "AcCashrequestDetail.findBySuspencecode", query = "SELECT a FROM AcCashrequestDetail a WHERE a.suspencecode = :suspencecode")
    , @NamedQuery(name = "AcCashrequestDetail.findBySuspencedesc", query = "SELECT a FROM AcCashrequestDetail a WHERE a.suspencedesc = :suspencedesc")
    , @NamedQuery(name = "AcCashrequestDetail.findByAmount", query = "SELECT a FROM AcCashrequestDetail a WHERE a.amount = :amount")})
public class AcCashrequestDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "referno")
    private String referno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Size(max = 4)
    @Column(name = "estatecode")
    private String estatecode;
    @Size(max = 200)
    @Column(name = "estatedescp")
    private String estatedescp;
    @Size(max = 20)
    @Column(name = "suspencecode")
    private String suspencecode;
    @Size(max = 200)
    @Column(name = "suspencedesc")
    private String suspencedesc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;

    public AcCashrequestDetail() {
    }

    public AcCashrequestDetail(String referno) {
        this.referno = referno;
    }

    public AcCashrequestDetail(String referno, String refer) {
        this.referno = referno;
        this.refer = refer;
    }

    public String getReferno() {
        return referno;
    }

    public void setReferno(String referno) {
        this.referno = referno;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatedescp() {
        return estatedescp;
    }

    public void setEstatedescp(String estatedescp) {
        this.estatedescp = estatedescp;
    }

    public String getSuspencecode() {
        return suspencecode;
    }

    public void setSuspencecode(String suspencecode) {
        this.suspencecode = suspencecode;
    }

    public String getSuspencedesc() {
        return suspencedesc;
    }

    public void setSuspencedesc(String suspencedesc) {
        this.suspencedesc = suspencedesc;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referno != null ? referno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcCashrequestDetail)) {
            return false;
        }
        AcCashrequestDetail other = (AcCashrequestDetail) object;
        if ((this.referno == null && other.referno != null) || (this.referno != null && !this.referno.equals(other.referno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcCashrequestDetail[ referno=" + referno + " ]";
    }
    
}
