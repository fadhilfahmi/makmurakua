/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.contract;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_big_agree_work")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgreementJob.findAll", query = "SELECT a FROM AgreementJob a"),
    @NamedQuery(name = "AgreementJob.findByRemark", query = "SELECT a FROM AgreementJob a WHERE a.remark = :remark"),
    @NamedQuery(name = "AgreementJob.findByVotype", query = "SELECT a FROM AgreementJob a WHERE a.votype = :votype"),
    @NamedQuery(name = "AgreementJob.findByNo", query = "SELECT a FROM AgreementJob a WHERE a.no = :no"),
    @NamedQuery(name = "AgreementJob.findByUnit", query = "SELECT a FROM AgreementJob a WHERE a.unit = :unit"),
    @NamedQuery(name = "AgreementJob.findByPriceunit", query = "SELECT a FROM AgreementJob a WHERE a.priceunit = :priceunit"),
    @NamedQuery(name = "AgreementJob.findByPricework", query = "SELECT a FROM AgreementJob a WHERE a.pricework = :pricework"),
    @NamedQuery(name = "AgreementJob.findByRefer", query = "SELECT a FROM AgreementJob a WHERE a.refer = :refer"),
    @NamedQuery(name = "AgreementJob.findByJobcode", query = "SELECT a FROM AgreementJob a WHERE a.jobcode = :jobcode"),
    @NamedQuery(name = "AgreementJob.findByJobdescp", query = "SELECT a FROM AgreementJob a WHERE a.jobdescp = :jobdescp"),
    @NamedQuery(name = "AgreementJob.findByLoccode", query = "SELECT a FROM AgreementJob a WHERE a.loccode = :loccode"),
    @NamedQuery(name = "AgreementJob.findByLocdesc", query = "SELECT a FROM AgreementJob a WHERE a.locdesc = :locdesc"),
    @NamedQuery(name = "AgreementJob.findByUnitmeasure", query = "SELECT a FROM AgreementJob a WHERE a.unitmeasure = :unitmeasure"),
    @NamedQuery(name = "AgreementJob.findByPvpercent", query = "SELECT a FROM AgreementJob a WHERE a.pvpercent = :pvpercent"),
    @NamedQuery(name = "AgreementJob.findByPvqty", query = "SELECT a FROM AgreementJob a WHERE a.pvqty = :pvqty"),
    @NamedQuery(name = "AgreementJob.findByPvstartdate", query = "SELECT a FROM AgreementJob a WHERE a.pvstartdate = :pvstartdate"),
    @NamedQuery(name = "AgreementJob.findByPvenddate", query = "SELECT a FROM AgreementJob a WHERE a.pvenddate = :pvenddate"),
    @NamedQuery(name = "AgreementJob.findByPvpenalty", query = "SELECT a FROM AgreementJob a WHERE a.pvpenalty = :pvpenalty"),
    @NamedQuery(name = "AgreementJob.findByFlag", query = "SELECT a FROM AgreementJob a WHERE a.flag = :flag"),
    @NamedQuery(name = "AgreementJob.findByOutputcode", query = "SELECT a FROM AgreementJob a WHERE a.outputcode = :outputcode"),
    @NamedQuery(name = "AgreementJob.findByOutputdesc", query = "SELECT a FROM AgreementJob a WHERE a.outputdesc = :outputdesc"),
    @NamedQuery(name = "AgreementJob.findByType", query = "SELECT a FROM AgreementJob a WHERE a.type = :type"),
    @NamedQuery(name = "AgreementJob.findByEntercode", query = "SELECT a FROM AgreementJob a WHERE a.entercode = :entercode"),
    @NamedQuery(name = "AgreementJob.findByEnterdesc", query = "SELECT a FROM AgreementJob a WHERE a.enterdesc = :enterdesc"),
    @NamedQuery(name = "AgreementJob.findByAcdesc", query = "SELECT a FROM AgreementJob a WHERE a.acdesc = :acdesc"),
    @NamedQuery(name = "AgreementJob.findByAccode", query = "SELECT a FROM AgreementJob a WHERE a.accode = :accode"),
    @NamedQuery(name = "AgreementJob.findByStartdate", query = "SELECT a FROM AgreementJob a WHERE a.startdate = :startdate"),
    @NamedQuery(name = "AgreementJob.findByLoclevel", query = "SELECT a FROM AgreementJob a WHERE a.loclevel = :loclevel"),
    @NamedQuery(name = "AgreementJob.findByTaxcode", query = "SELECT a FROM AgreementJob a WHERE a.taxcode = :taxcode"),
    @NamedQuery(name = "AgreementJob.findByTaxrate", query = "SELECT a FROM AgreementJob a WHERE a.taxrate = :taxrate"),
    @NamedQuery(name = "AgreementJob.findByTaxamt", query = "SELECT a FROM AgreementJob a WHERE a.taxamt = :taxamt"),
    @NamedQuery(name = "AgreementJob.findByTaxcoacode", query = "SELECT a FROM AgreementJob a WHERE a.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "AgreementJob.findByTaxdescp", query = "SELECT a FROM AgreementJob a WHERE a.taxdescp = :taxdescp"),
    @NamedQuery(name = "AgreementJob.findByTaxcoadescp", query = "SELECT a FROM AgreementJob a WHERE a.taxcoadescp = :taxcoadescp")})
public class AgreementJob implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "votype")
    private String votype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unit")
    private String unit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "priceunit")
    private String priceunit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pricework")
    private double pricework;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "jobcode")
    private String jobcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "jobdescp")
    private String jobdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitmeasure")
    private String unitmeasure;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvpercent")
    private String pvpercent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvqty")
    private String pvqty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvstartdate")
    private String pvstartdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvenddate")
    private String pvenddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvpenalty")
    private String pvpenalty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "outputcode")
    private String outputcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "outputdesc")
    private String outputdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "entercode")
    private String entercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "enterdesc")
    private String enterdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acdesc")
    private String acdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accode")
    private String accode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "startdate")
    private String startdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;

    public AgreementJob() {
    }

    public AgreementJob(String refer) {
        this.refer = refer;
    }

    public AgreementJob(String refer, String remark, String votype, String no, String unit, String priceunit, double pricework, String jobcode, String jobdescp, String loccode, String locdesc, String unitmeasure, String pvpercent, String pvqty, String pvstartdate, String pvenddate, String pvpenalty, String flag, String outputcode, String outputdesc, String type, String entercode, String enterdesc, String acdesc, String accode, String startdate, String loclevel, String taxcode, double taxrate, double taxamt, String taxcoacode, String taxdescp, String taxcoadescp) {
        this.refer = refer;
        this.remark = remark;
        this.votype = votype;
        this.no = no;
        this.unit = unit;
        this.priceunit = priceunit;
        this.pricework = pricework;
        this.jobcode = jobcode;
        this.jobdescp = jobdescp;
        this.loccode = loccode;
        this.locdesc = locdesc;
        this.unitmeasure = unitmeasure;
        this.pvpercent = pvpercent;
        this.pvqty = pvqty;
        this.pvstartdate = pvstartdate;
        this.pvenddate = pvenddate;
        this.pvpenalty = pvpenalty;
        this.flag = flag;
        this.outputcode = outputcode;
        this.outputdesc = outputdesc;
        this.type = type;
        this.entercode = entercode;
        this.enterdesc = enterdesc;
        this.acdesc = acdesc;
        this.accode = accode;
        this.startdate = startdate;
        this.loclevel = loclevel;
        this.taxcode = taxcode;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.taxcoacode = taxcoacode;
        this.taxdescp = taxdescp;
        this.taxcoadescp = taxcoadescp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVotype() {
        return votype;
    }

    public void setVotype(String votype) {
        this.votype = votype;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPriceunit() {
        return priceunit;
    }

    public void setPriceunit(String priceunit) {
        this.priceunit = priceunit;
    }

    public double getPricework() {
        return pricework;
    }

    public void setPricework(double pricework) {
        this.pricework = pricework;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getJobcode() {
        return jobcode;
    }

    public void setJobcode(String jobcode) {
        this.jobcode = jobcode;
    }

    public String getJobdescp() {
        return jobdescp;
    }

    public void setJobdescp(String jobdescp) {
        this.jobdescp = jobdescp;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getUnitmeasure() {
        return unitmeasure;
    }

    public void setUnitmeasure(String unitmeasure) {
        this.unitmeasure = unitmeasure;
    }

    public String getPvpercent() {
        return pvpercent;
    }

    public void setPvpercent(String pvpercent) {
        this.pvpercent = pvpercent;
    }

    public String getPvqty() {
        return pvqty;
    }

    public void setPvqty(String pvqty) {
        this.pvqty = pvqty;
    }

    public String getPvstartdate() {
        return pvstartdate;
    }

    public void setPvstartdate(String pvstartdate) {
        this.pvstartdate = pvstartdate;
    }

    public String getPvenddate() {
        return pvenddate;
    }

    public void setPvenddate(String pvenddate) {
        this.pvenddate = pvenddate;
    }

    public String getPvpenalty() {
        return pvpenalty;
    }

    public void setPvpenalty(String pvpenalty) {
        this.pvpenalty = pvpenalty;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getOutputcode() {
        return outputcode;
    }

    public void setOutputcode(String outputcode) {
        this.outputcode = outputcode;
    }

    public String getOutputdesc() {
        return outputdesc;
    }

    public void setOutputdesc(String outputdesc) {
        this.outputdesc = outputdesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEntercode() {
        return entercode;
    }

    public void setEntercode(String entercode) {
        this.entercode = entercode;
    }

    public String getEnterdesc() {
        return enterdesc;
    }

    public void setEnterdesc(String enterdesc) {
        this.enterdesc = enterdesc;
    }

    public String getAcdesc() {
        return acdesc;
    }

    public void setAcdesc(String acdesc) {
        this.acdesc = acdesc;
    }

    public String getAccode() {
        return accode;
    }

    public void setAccode(String accode) {
        this.accode = accode;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgreementJob)) {
            return false;
        }
        AgreementJob other = (AgreementJob) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.contract.AgreementJob[ refer=" + refer + " ]";
    }
    
}
