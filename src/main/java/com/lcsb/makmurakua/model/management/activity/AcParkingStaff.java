/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_parking_staff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcParkingStaff.findAll", query = "SELECT a FROM AcParkingStaff a")
    , @NamedQuery(name = "AcParkingStaff.findByReferid", query = "SELECT a FROM AcParkingStaff a WHERE a.referid = :referid")
    , @NamedQuery(name = "AcParkingStaff.findByRefer", query = "SELECT a FROM AcParkingStaff a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcParkingStaff.findByStaffID", query = "SELECT a FROM AcParkingStaff a WHERE a.staffID = :staffID")
    , @NamedQuery(name = "AcParkingStaff.findByStaffname", query = "SELECT a FROM AcParkingStaff a WHERE a.staffname = :staffname")
    , @NamedQuery(name = "AcParkingStaff.findByAmount", query = "SELECT a FROM AcParkingStaff a WHERE a.amount = :amount")
    , @NamedQuery(name = "AcParkingStaff.findByDepartmentID", query = "SELECT a FROM AcParkingStaff a WHERE a.departmentID = :departmentID")
    , @NamedQuery(name = "AcParkingStaff.findByDepartmentname", query = "SELECT a FROM AcParkingStaff a WHERE a.departmentname = :departmentname")})
public class AcParkingStaff implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "referid")
    private String referid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Size(max = 10)
    @Column(name = "staffID")
    private String staffID;
    @Size(max = 150)
    @Column(name = "staffname")
    private String staffname;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Size(max = 10)
    @Column(name = "departmentID")
    private String departmentID;
    @Size(max = 30)
    @Column(name = "departmentname")
    private String departmentname;

    public AcParkingStaff() {
    }

    public AcParkingStaff(String referid) {
        this.referid = referid;
    }

    public AcParkingStaff(String referid, String refer) {
        this.referid = referid;
        this.refer = refer;
    }

    public String getReferid() {
        return referid;
    }

    public void setReferid(String referid) {
        this.referid = referid;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(String departmentID) {
        this.departmentID = departmentID;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referid != null ? referid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcParkingStaff)) {
            return false;
        }
        AcParkingStaff other = (AcParkingStaff) object;
        if ((this.referid == null && other.referid != null) || (this.referid != null && !this.referid.equals(other.referid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcParkingStaff[ referid=" + referid + " ]";
    }
    
}
