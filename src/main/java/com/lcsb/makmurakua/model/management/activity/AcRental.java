/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_rental")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcRental.findAll", query = "SELECT a FROM AcRental a")
    , @NamedQuery(name = "AcRental.findByRefer", query = "SELECT a FROM AcRental a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcRental.findByTenantcode", query = "SELECT a FROM AcRental a WHERE a.tenantcode = :tenantcode")
    , @NamedQuery(name = "AcRental.findByTenantdesc", query = "SELECT a FROM AcRental a WHERE a.tenantdesc = :tenantdesc")
    , @NamedQuery(name = "AcRental.findByDate", query = "SELECT a FROM AcRental a WHERE a.date = :date")
    , @NamedQuery(name = "AcRental.findByTenantaddress", query = "SELECT a FROM AcRental a WHERE a.tenantaddress = :tenantaddress")
    , @NamedQuery(name = "AcRental.findByTenantpostcode", query = "SELECT a FROM AcRental a WHERE a.tenantpostcode = :tenantpostcode")
    , @NamedQuery(name = "AcRental.findByTenantcity", query = "SELECT a FROM AcRental a WHERE a.tenantcity = :tenantcity")
    , @NamedQuery(name = "AcRental.findByTenantstate", query = "SELECT a FROM AcRental a WHERE a.tenantstate = :tenantstate")
    , @NamedQuery(name = "AcRental.findByPrepareid", query = "SELECT a FROM AcRental a WHERE a.prepareid = :prepareid")
    , @NamedQuery(name = "AcRental.findByPreparename", query = "SELECT a FROM AcRental a WHERE a.preparename = :preparename")
    , @NamedQuery(name = "AcRental.findByPreparedate", query = "SELECT a FROM AcRental a WHERE a.preparedate = :preparedate")
    , @NamedQuery(name = "AcRental.findByYear", query = "SELECT a FROM AcRental a WHERE a.year = :year")
    , @NamedQuery(name = "AcRental.findByPeriod", query = "SELECT a FROM AcRental a WHERE a.period = :period")
    , @NamedQuery(name = "AcRental.findByCheckid", query = "SELECT a FROM AcRental a WHERE a.checkid = :checkid")
    , @NamedQuery(name = "AcRental.findByCheckname", query = "SELECT a FROM AcRental a WHERE a.checkname = :checkname")
    , @NamedQuery(name = "AcRental.findByCheckdate", query = "SELECT a FROM AcRental a WHERE a.checkdate = :checkdate")
    , @NamedQuery(name = "AcRental.findByAppid", query = "SELECT a FROM AcRental a WHERE a.appid = :appid")
    , @NamedQuery(name = "AcRental.findByAppname", query = "SELECT a FROM AcRental a WHERE a.appname = :appname")
    , @NamedQuery(name = "AcRental.findByAppdate", query = "SELECT a FROM AcRental a WHERE a.appdate = :appdate")
    , @NamedQuery(name = "AcRental.findByRemark", query = "SELECT a FROM AcRental a WHERE a.remark = :remark")
    , @NamedQuery(name = "AcRental.findByCompanycode", query = "SELECT a FROM AcRental a WHERE a.companycode = :companycode")
    , @NamedQuery(name = "AcRental.findByCompanydescp", query = "SELECT a FROM AcRental a WHERE a.companydescp = :companydescp")})
public class AcRental implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "tenantcode")
    private String tenantcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "tenantdesc")
    private String tenantdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Size(max = 300)
    @Column(name = "tenantaddress")
    private String tenantaddress;
    @Size(max = 10)
    @Column(name = "tenantpostcode")
    private String tenantpostcode;
    @Size(max = 30)
    @Column(name = "tenantcity")
    private String tenantcity;
    @Size(max = 30)
    @Column(name = "tenantstate")
    private String tenantstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Size(max = 10)
    @Column(name = "checkid")
    private String checkid;
    @Size(max = 300)
    @Column(name = "checkname")
    private String checkname;
    @Size(max = 10)
    @Column(name = "checkdate")
    private String checkdate;
    @Size(max = 10)
    @Column(name = "appid")
    private String appid;
    @Size(max = 300)
    @Column(name = "appname")
    private String appname;
    @Size(max = 10)
    @Column(name = "appdate")
    private String appdate;
    @Size(max = 300)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "companycode")
    private String companycode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companydescp")
    private String companydescp;

    public AcRental() {
    }

    public AcRental(String refer) {
        this.refer = refer;
    }

    public AcRental(String refer, String tenantcode, String tenantdesc, String date, String prepareid, String preparename, String preparedate, String year, String period, String companycode, String companydescp) {
        this.refer = refer;
        this.tenantcode = tenantcode;
        this.tenantdesc = tenantdesc;
        this.date = date;
        this.prepareid = prepareid;
        this.preparename = preparename;
        this.preparedate = preparedate;
        this.year = year;
        this.period = period;
        this.companycode = companycode;
        this.companydescp = companydescp;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getTenantcode() {
        return tenantcode;
    }

    public void setTenantcode(String tenantcode) {
        this.tenantcode = tenantcode;
    }

    public String getTenantdesc() {
        return tenantdesc;
    }

    public void setTenantdesc(String tenantdesc) {
        this.tenantdesc = tenantdesc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTenantaddress() {
        return tenantaddress;
    }

    public void setTenantaddress(String tenantaddress) {
        this.tenantaddress = tenantaddress;
    }

    public String getTenantpostcode() {
        return tenantpostcode;
    }

    public void setTenantpostcode(String tenantpostcode) {
        this.tenantpostcode = tenantpostcode;
    }

    public String getTenantcity() {
        return tenantcity;
    }

    public void setTenantcity(String tenantcity) {
        this.tenantcity = tenantcity;
    }

    public String getTenantstate() {
        return tenantstate;
    }

    public void setTenantstate(String tenantstate) {
        this.tenantstate = tenantstate;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCompanydescp() {
        return companydescp;
    }

    public void setCompanydescp(String companydescp) {
        this.companydescp = companydescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcRental)) {
            return false;
        }
        AcRental other = (AcRental) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcRental[ refer=" + refer + " ]";
    }
    
}
