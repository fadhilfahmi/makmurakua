/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_cashrequest")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcCashrequest.findAll", query = "SELECT a FROM AcCashrequest a")
    , @NamedQuery(name = "AcCashrequest.findByRefer", query = "SELECT a FROM AcCashrequest a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcCashrequest.findByYear", query = "SELECT a FROM AcCashrequest a WHERE a.year = :year")
    , @NamedQuery(name = "AcCashrequest.findByPeriod", query = "SELECT a FROM AcCashrequest a WHERE a.period = :period")
    , @NamedQuery(name = "AcCashrequest.findByEstatedesc", query = "SELECT a FROM AcCashrequest a WHERE a.estatedesc = :estatedesc")
    , @NamedQuery(name = "AcCashrequest.findByEstatecode", query = "SELECT a FROM AcCashrequest a WHERE a.estatecode = :estatecode")
    , @NamedQuery(name = "AcCashrequest.findByMgtdescp", query = "SELECT a FROM AcCashrequest a WHERE a.mgtdescp = :mgtdescp")
    , @NamedQuery(name = "AcCashrequest.findByMgtcode", query = "SELECT a FROM AcCashrequest a WHERE a.mgtcode = :mgtcode")
    , @NamedQuery(name = "AcCashrequest.findByStartdate", query = "SELECT a FROM AcCashrequest a WHERE a.startdate = :startdate")
    , @NamedQuery(name = "AcCashrequest.findByEnddate", query = "SELECT a FROM AcCashrequest a WHERE a.enddate = :enddate")
    , @NamedQuery(name = "AcCashrequest.findByAppid", query = "SELECT a FROM AcCashrequest a WHERE a.appid = :appid")
    , @NamedQuery(name = "AcCashrequest.findByAppname", query = "SELECT a FROM AcCashrequest a WHERE a.appname = :appname")
    , @NamedQuery(name = "AcCashrequest.findByAppdesign", query = "SELECT a FROM AcCashrequest a WHERE a.appdesign = :appdesign")
    , @NamedQuery(name = "AcCashrequest.findByAppdate", query = "SELECT a FROM AcCashrequest a WHERE a.appdate = :appdate")
    , @NamedQuery(name = "AcCashrequest.findByPreid", query = "SELECT a FROM AcCashrequest a WHERE a.preid = :preid")
    , @NamedQuery(name = "AcCashrequest.findByPrename", query = "SELECT a FROM AcCashrequest a WHERE a.prename = :prename")
    , @NamedQuery(name = "AcCashrequest.findByPredate", query = "SELECT a FROM AcCashrequest a WHERE a.predate = :predate")
    , @NamedQuery(name = "AcCashrequest.findByCheckid", query = "SELECT a FROM AcCashrequest a WHERE a.checkid = :checkid")
    , @NamedQuery(name = "AcCashrequest.findByCheckname", query = "SELECT a FROM AcCashrequest a WHERE a.checkname = :checkname")
    , @NamedQuery(name = "AcCashrequest.findByCheckdate", query = "SELECT a FROM AcCashrequest a WHERE a.checkdate = :checkdate")
    , @NamedQuery(name = "AcCashrequest.findByRemarks", query = "SELECT a FROM AcCashrequest a WHERE a.remarks = :remarks")
    , @NamedQuery(name = "AcCashrequest.findByStatus", query = "SELECT a FROM AcCashrequest a WHERE a.status = :status")})
public class AcCashrequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatedesc")
    private String estatedesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtdescp")
    private String mgtdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode")
    private String mgtcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "startdate")
    private String startdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "enddate")
    private String enddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predate")
    private String predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    private double amount;
    private String pvno;

    public AcCashrequest() {
    }

    public AcCashrequest(String refer) {
        this.refer = refer;
    }

    public AcCashrequest(String refer, String year, String period, String estatedesc, String estatecode, String mgtdescp, String mgtcode, String startdate, String enddate, String appid, String appname, String appdesign, String appdate, String preid, String prename, String predate, String checkid, String checkname, String checkdate, String remarks, double amount, String pvno) {
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.estatedesc = estatedesc;
        this.estatecode = estatecode;
        this.mgtdescp = mgtdescp;
        this.mgtcode = mgtcode;
        this.startdate = startdate;
        this.enddate = enddate;
        this.appid = appid;
        this.appname = appname;
        this.appdesign = appdesign;
        this.appdate = appdate;
        this.preid = preid;
        this.prename = prename;
        this.predate = predate;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkdate = checkdate;
        this.remarks = remarks;
        this.amount = amount;
        this.pvno = pvno;
    }

    public String getPvno() {
        return pvno;
    }

    public void setPvno(String pvno) {
        this.pvno = pvno;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    
    
    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getEstatedesc() {
        return estatedesc;
    }

    public void setEstatedesc(String estatedesc) {
        this.estatedesc = estatedesc;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getMgtdescp() {
        return mgtdescp;
    }

    public void setMgtdescp(String mgtdescp) {
        this.mgtdescp = mgtdescp;
    }

    public String getMgtcode() {
        return mgtcode;
    }

    public void setMgtcode(String mgtcode) {
        this.mgtcode = mgtcode;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPredate() {
        return predate;
    }

    public void setPredate(String predate) {
        this.predate = predate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcCashrequest)) {
            return false;
        }
        AcCashrequest other = (AcCashrequest) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcCashrequest[ refer=" + refer + " ]";
    }
    
}
