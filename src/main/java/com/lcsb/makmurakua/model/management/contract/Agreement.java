/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.contract;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_big_agree")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agreement.findAll", query = "SELECT a FROM Agreement a"),
    @NamedQuery(name = "Agreement.findByTkipercent", query = "SELECT a FROM Agreement a WHERE a.tkipercent = :tkipercent"),
    @NamedQuery(name = "Agreement.findByConfirmagree", query = "SELECT a FROM Agreement a WHERE a.confirmagree = :confirmagree"),
    @NamedQuery(name = "Agreement.findByTypeagree", query = "SELECT a FROM Agreement a WHERE a.typeagree = :typeagree"),
    @NamedQuery(name = "Agreement.findByHnoagree", query = "SELECT a FROM Agreement a WHERE a.hnoagree = :hnoagree"),
    @NamedQuery(name = "Agreement.findByRefer", query = "SELECT a FROM Agreement a WHERE a.refer = :refer"),
    @NamedQuery(name = "Agreement.findByYear", query = "SELECT a FROM Agreement a WHERE a.year = :year"),
    @NamedQuery(name = "Agreement.findByPeriod", query = "SELECT a FROM Agreement a WHERE a.period = :period"),
    @NamedQuery(name = "Agreement.findByFlag", query = "SELECT a FROM Agreement a WHERE a.flag = :flag"),
    @NamedQuery(name = "Agreement.findByEstatecode", query = "SELECT a FROM Agreement a WHERE a.estatecode = :estatecode"),
    @NamedQuery(name = "Agreement.findByEstatename", query = "SELECT a FROM Agreement a WHERE a.estatename = :estatename"),
    @NamedQuery(name = "Agreement.findByName", query = "SELECT a FROM Agreement a WHERE a.name = :name"),
    @NamedQuery(name = "Agreement.findByDate", query = "SELECT a FROM Agreement a WHERE a.date = :date"),
    @NamedQuery(name = "Agreement.findByCode", query = "SELECT a FROM Agreement a WHERE a.code = :code"),
    @NamedQuery(name = "Agreement.findByDateend", query = "SELECT a FROM Agreement a WHERE a.dateend = :dateend"),
    @NamedQuery(name = "Agreement.findByPtype", query = "SELECT a FROM Agreement a WHERE a.ptype = :ptype"),
    @NamedQuery(name = "Agreement.findByPamount", query = "SELECT a FROM Agreement a WHERE a.pamount = :pamount"),
    @NamedQuery(name = "Agreement.findByIc", query = "SELECT a FROM Agreement a WHERE a.ic = :ic"),
    @NamedQuery(name = "Agreement.findByAddress", query = "SELECT a FROM Agreement a WHERE a.address = :address"),
    @NamedQuery(name = "Agreement.findByPengurus", query = "SELECT a FROM Agreement a WHERE a.pengurus = :pengurus"),
    @NamedQuery(name = "Agreement.findBySaksi", query = "SELECT a FROM Agreement a WHERE a.saksi = :saksi"),
    @NamedQuery(name = "Agreement.findByOwner", query = "SELECT a FROM Agreement a WHERE a.owner = :owner"),
    @NamedQuery(name = "Agreement.findByPengurusid", query = "SELECT a FROM Agreement a WHERE a.pengurusid = :pengurusid"),
    @NamedQuery(name = "Agreement.findByIcpengurus", query = "SELECT a FROM Agreement a WHERE a.icpengurus = :icpengurus"),
    @NamedQuery(name = "Agreement.findByPdesignation", query = "SELECT a FROM Agreement a WHERE a.pdesignation = :pdesignation"),
    @NamedQuery(name = "Agreement.findBySaksiid", query = "SELECT a FROM Agreement a WHERE a.saksiid = :saksiid"),
    @NamedQuery(name = "Agreement.findBySaksiic", query = "SELECT a FROM Agreement a WHERE a.saksiic = :saksiic"),
    @NamedQuery(name = "Agreement.findByPredate", query = "SELECT a FROM Agreement a WHERE a.predate = :predate"),
    @NamedQuery(name = "Agreement.findByPredesign", query = "SELECT a FROM Agreement a WHERE a.predesign = :predesign"),
    @NamedQuery(name = "Agreement.findByPrename", query = "SELECT a FROM Agreement a WHERE a.prename = :prename"),
    @NamedQuery(name = "Agreement.findByPreid", query = "SELECT a FROM Agreement a WHERE a.preid = :preid"),
    @NamedQuery(name = "Agreement.findByAppdate", query = "SELECT a FROM Agreement a WHERE a.appdate = :appdate"),
    @NamedQuery(name = "Agreement.findByDatecheck", query = "SELECT a FROM Agreement a WHERE a.datecheck = :datecheck"),
    @NamedQuery(name = "Agreement.findBySdesignation", query = "SELECT a FROM Agreement a WHERE a.sdesignation = :sdesignation"),
    @NamedQuery(name = "Agreement.findByGstid", query = "SELECT a FROM Agreement a WHERE a.gstid = :gstid")})
public class Agreement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tkipercent")
    private String tkipercent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "confirmagree")
    private String confirmagree;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "typeagree")
    private String typeagree;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hnoagree")
    private String hnoagree;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dateend")
    private String dateend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ptype")
    private String ptype;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pamount")
    private double pamount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ic")
    private String ic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pengurus")
    private String pengurus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "saksi")
    private String saksi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "owner")
    private String owner;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pengurusid")
    private String pengurusid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "icpengurus")
    private String icpengurus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pdesignation")
    private String pdesignation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "saksiid")
    private String saksiid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "saksiic")
    private String saksiic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predate")
    private String predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predesign")
    private String predesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "datecheck")
    private String datecheck;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sdesignation")
    private String sdesignation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;

    public Agreement() {
    }

    public Agreement(String refer) {
        this.refer = refer;
    }

    public Agreement(String refer, String tkipercent, String confirmagree, String typeagree, String hnoagree, String year, String period, String flag, String estatecode, String estatename, String name, String date, String code, String dateend, String ptype, double pamount, String ic, String address, String pengurus, String saksi, String owner, String pengurusid, String icpengurus, String pdesignation, String saksiid, String saksiic, String predate, String predesign, String prename, String preid, String appdate, String datecheck, String sdesignation, String gstid) {
        this.refer = refer;
        this.tkipercent = tkipercent;
        this.confirmagree = confirmagree;
        this.typeagree = typeagree;
        this.hnoagree = hnoagree;
        this.year = year;
        this.period = period;
        this.flag = flag;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.name = name;
        this.date = date;
        this.code = code;
        this.dateend = dateend;
        this.ptype = ptype;
        this.pamount = pamount;
        this.ic = ic;
        this.address = address;
        this.pengurus = pengurus;
        this.saksi = saksi;
        this.owner = owner;
        this.pengurusid = pengurusid;
        this.icpengurus = icpengurus;
        this.pdesignation = pdesignation;
        this.saksiid = saksiid;
        this.saksiic = saksiic;
        this.predate = predate;
        this.predesign = predesign;
        this.prename = prename;
        this.preid = preid;
        this.appdate = appdate;
        this.datecheck = datecheck;
        this.sdesignation = sdesignation;
        this.gstid = gstid;
    }

    public String getTkipercent() {
        return tkipercent;
    }

    public void setTkipercent(String tkipercent) {
        this.tkipercent = tkipercent;
    }

    public String getConfirmagree() {
        return confirmagree;
    }

    public void setConfirmagree(String confirmagree) {
        this.confirmagree = confirmagree;
    }

    public String getTypeagree() {
        return typeagree;
    }

    public void setTypeagree(String typeagree) {
        this.typeagree = typeagree;
    }

    public String getHnoagree() {
        return hnoagree;
    }

    public void setHnoagree(String hnoagree) {
        this.hnoagree = hnoagree;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateend() {
        return dateend;
    }

    public void setDateend(String dateend) {
        this.dateend = dateend;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public double getPamount() {
        return pamount;
    }

    public void setPamount(double pamount) {
        this.pamount = pamount;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPengurus() {
        return pengurus;
    }

    public void setPengurus(String pengurus) {
        this.pengurus = pengurus;
    }

    public String getSaksi() {
        return saksi;
    }

    public void setSaksi(String saksi) {
        this.saksi = saksi;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPengurusid() {
        return pengurusid;
    }

    public void setPengurusid(String pengurusid) {
        this.pengurusid = pengurusid;
    }

    public String getIcpengurus() {
        return icpengurus;
    }

    public void setIcpengurus(String icpengurus) {
        this.icpengurus = icpengurus;
    }

    public String getPdesignation() {
        return pdesignation;
    }

    public void setPdesignation(String pdesignation) {
        this.pdesignation = pdesignation;
    }

    public String getSaksiid() {
        return saksiid;
    }

    public void setSaksiid(String saksiid) {
        this.saksiid = saksiid;
    }

    public String getSaksiic() {
        return saksiic;
    }

    public void setSaksiic(String saksiic) {
        this.saksiic = saksiic;
    }

    public String getPredate() {
        return predate;
    }

    public void setPredate(String predate) {
        this.predate = predate;
    }

    public String getPredesign() {
        return predesign;
    }

    public void setPredesign(String predesign) {
        this.predesign = predesign;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getDatecheck() {
        return datecheck;
    }

    public void setDatecheck(String datecheck) {
        this.datecheck = datecheck;
    }

    public String getSdesignation() {
        return sdesignation;
    }

    public void setSdesignation(String sdesignation) {
        this.sdesignation = sdesignation;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agreement)) {
            return false;
        }
        Agreement other = (Agreement) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.contract.Agreement[ refer=" + refer + " ]";
    }
    
}
