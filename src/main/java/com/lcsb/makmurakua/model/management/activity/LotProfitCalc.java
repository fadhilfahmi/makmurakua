/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_lot_profit_calc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotProfitCalc.findAll", query = "SELECT l FROM LotProfitCalc l"),
    @NamedQuery(name = "LotProfitCalc.findById", query = "SELECT l FROM LotProfitCalc l WHERE l.id = :id"),
    @NamedQuery(name = "LotProfitCalc.findByCurrevenue", query = "SELECT l FROM LotProfitCalc l WHERE l.currevenue = :currevenue"),
    @NamedQuery(name = "LotProfitCalc.findByPrerevenue", query = "SELECT l FROM LotProfitCalc l WHERE l.prerevenue = :prerevenue"),
    @NamedQuery(name = "LotProfitCalc.findByTotalrevenue", query = "SELECT l FROM LotProfitCalc l WHERE l.totalrevenue = :totalrevenue"),
    @NamedQuery(name = "LotProfitCalc.findByDistributed", query = "SELECT l FROM LotProfitCalc l WHERE l.distributed = :distributed"),
    @NamedQuery(name = "LotProfitCalc.findByLkpprevenue", query = "SELECT l FROM LotProfitCalc l WHERE l.lkpprevenue = :lkpprevenue"),
    @NamedQuery(name = "LotProfitCalc.findByTotaldistributed", query = "SELECT l FROM LotProfitCalc l WHERE l.totaldistributed = :totaldistributed"),
    @NamedQuery(name = "LotProfitCalc.findByBalafterdist", query = "SELECT l FROM LotProfitCalc l WHERE l.balafterdist = :balafterdist"),
    @NamedQuery(name = "LotProfitCalc.findByRollingcapital", query = "SELECT l FROM LotProfitCalc l WHERE l.rollingcapital = :rollingcapital"),
    @NamedQuery(name = "LotProfitCalc.findByPreyearreplanting", query = "SELECT l FROM LotProfitCalc l WHERE l.preyearreplanting = :preyearreplanting"),
    @NamedQuery(name = "LotProfitCalc.findByCuryearreplanting", query = "SELECT l FROM LotProfitCalc l WHERE l.curyearreplanting = :curyearreplanting"),
    @NamedQuery(name = "LotProfitCalc.findByDistributableprofit", query = "SELECT l FROM LotProfitCalc l WHERE l.distributableprofit = :distributableprofit"),
    @NamedQuery(name = "LotProfitCalc.findByProfitperhect", query = "SELECT l FROM LotProfitCalc l WHERE l.profitperhect = :profitperhect"),
    @NamedQuery(name = "LotProfitCalc.findByDistsuggest", query = "SELECT l FROM LotProfitCalc l WHERE l.distsuggest = :distsuggest"),
    @NamedQuery(name = "LotProfitCalc.findBySessionid", query = "SELECT l FROM LotProfitCalc l WHERE l.sessionid = :sessionid"),
    @NamedQuery(name = "LotProfitCalc.findByLotcode", query = "SELECT l FROM LotProfitCalc l WHERE l.lotcode = :lotcode"),
    @NamedQuery(name = "LotProfitCalc.findByLotdesc", query = "SELECT l FROM LotProfitCalc l WHERE l.lotdesc = :lotdesc"),
    @NamedQuery(name = "LotProfitCalc.findByYear", query = "SELECT l FROM LotProfitCalc l WHERE l.year = :year"),
    @NamedQuery(name = "LotProfitCalc.findByPeriod", query = "SELECT l FROM LotProfitCalc l WHERE l.period = :period"),
    @NamedQuery(name = "LotProfitCalc.findByQuarter", query = "SELECT l FROM LotProfitCalc l WHERE l.quarter = :quarter")})
public class LotProfitCalc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "currevenue")
    private double currevenue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prerevenue")
    private double prerevenue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalrevenue")
    private double totalrevenue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "distributed")
    private double distributed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lkpprevenue")
    private double lkpprevenue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totaldistributed")
    private double totaldistributed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "balafterdist")
    private double balafterdist;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rollingcapital")
    private double rollingcapital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preyearreplanting")
    private double preyearreplanting;
    @Basic(optional = false)
    @NotNull
    @Column(name = "curyearreplanting")
    private double curyearreplanting;
    @Basic(optional = false)
    @NotNull
    @Column(name = "distributableprofit")
    private double distributableprofit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "profitperhect")
    private double profitperhect;
    @Basic(optional = false)
    @NotNull
    @Column(name = "distsuggest")
    private double distsuggest;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sessionid")
    private String sessionid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "lotcode")
    private String lotcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "lotdesc")
    private String lotdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "quarter")
    private String quarter;

    public LotProfitCalc() {
    }

    public LotProfitCalc(Integer id) {
        this.id = id;
    }

    public LotProfitCalc(Integer id, double currevenue, double prerevenue, double totalrevenue, double distributed, double lkpprevenue, double totaldistributed, double balafterdist, double rollingcapital, double preyearreplanting, double curyearreplanting, double distributableprofit, double profitperhect, double distsuggest, String sessionid, String lotcode, String lotdesc, String year, String period, String quarter) {
        this.id = id;
        this.currevenue = currevenue;
        this.prerevenue = prerevenue;
        this.totalrevenue = totalrevenue;
        this.distributed = distributed;
        this.lkpprevenue = lkpprevenue;
        this.totaldistributed = totaldistributed;
        this.balafterdist = balafterdist;
        this.rollingcapital = rollingcapital;
        this.preyearreplanting = preyearreplanting;
        this.curyearreplanting = curyearreplanting;
        this.distributableprofit = distributableprofit;
        this.profitperhect = profitperhect;
        this.distsuggest = distsuggest;
        this.sessionid = sessionid;
        this.lotcode = lotcode;
        this.lotdesc = lotdesc;
        this.year = year;
        this.period = period;
        this.quarter = quarter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getCurrevenue() {
        return currevenue;
    }

    public void setCurrevenue(double currevenue) {
        this.currevenue = currevenue;
    }

    public double getPrerevenue() {
        return prerevenue;
    }

    public void setPrerevenue(double prerevenue) {
        this.prerevenue = prerevenue;
    }

    public double getTotalrevenue() {
        return totalrevenue;
    }

    public void setTotalrevenue(double totalrevenue) {
        this.totalrevenue = totalrevenue;
    }

    public double getDistributed() {
        return distributed;
    }

    public void setDistributed(double distributed) {
        this.distributed = distributed;
    }

    public double getLkpprevenue() {
        return lkpprevenue;
    }

    public void setLkpprevenue(double lkpprevenue) {
        this.lkpprevenue = lkpprevenue;
    }

    public double getTotaldistributed() {
        return totaldistributed;
    }

    public void setTotaldistributed(double totaldistributed) {
        this.totaldistributed = totaldistributed;
    }

    public double getBalafterdist() {
        return balafterdist;
    }

    public void setBalafterdist(double balafterdist) {
        this.balafterdist = balafterdist;
    }

    public double getRollingcapital() {
        return rollingcapital;
    }

    public void setRollingcapital(double rollingcapital) {
        this.rollingcapital = rollingcapital;
    }

    public double getPreyearreplanting() {
        return preyearreplanting;
    }

    public void setPreyearreplanting(double preyearreplanting) {
        this.preyearreplanting = preyearreplanting;
    }

    public double getCuryearreplanting() {
        return curyearreplanting;
    }

    public void setCuryearreplanting(double curyearreplanting) {
        this.curyearreplanting = curyearreplanting;
    }

    public double getDistributableprofit() {
        return distributableprofit;
    }

    public void setDistributableprofit(double distributableprofit) {
        this.distributableprofit = distributableprofit;
    }

    public double getProfitperhect() {
        return profitperhect;
    }

    public void setProfitperhect(double profitperhect) {
        this.profitperhect = profitperhect;
    }

    public double getDistsuggest() {
        return distsuggest;
    }

    public void setDistsuggest(double distsuggest) {
        this.distsuggest = distsuggest;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getLotcode() {
        return lotcode;
    }

    public void setLotcode(String lotcode) {
        this.lotcode = lotcode;
    }

    public String getLotdesc() {
        return lotdesc;
    }

    public void setLotdesc(String lotdesc) {
        this.lotdesc = lotdesc;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotProfitCalc)) {
            return false;
        }
        LotProfitCalc other = (LotProfitCalc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.LotProfitCalc[ id=" + id + " ]";
    }
    
}
