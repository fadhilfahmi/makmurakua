/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_lot_member")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotMemberProfit.findAll", query = "SELECT l FROM LotMemberProfit l")
    , @NamedQuery(name = "LotMemberProfit.findById", query = "SELECT l FROM LotMemberProfit l WHERE l.id = :id")
    , @NamedQuery(name = "LotMemberProfit.findByCode", query = "SELECT l FROM LotMemberProfit l WHERE l.code = :code")
    , @NamedQuery(name = "LotMemberProfit.findByName", query = "SELECT l FROM LotMemberProfit l WHERE l.name = :name")
    , @NamedQuery(name = "LotMemberProfit.findByRefer", query = "SELECT l FROM LotMemberProfit l WHERE l.refer = :refer")
    , @NamedQuery(name = "LotMemberProfit.findByLotcode", query = "SELECT l FROM LotMemberProfit l WHERE l.lotcode = :lotcode")
    , @NamedQuery(name = "LotMemberProfit.findByYear", query = "SELECT l FROM LotMemberProfit l WHERE l.year = :year")
    , @NamedQuery(name = "LotMemberProfit.findByPeriod", query = "SELECT l FROM LotMemberProfit l WHERE l.period = :period")
    , @NamedQuery(name = "LotMemberProfit.findByQuarter", query = "SELECT l FROM LotMemberProfit l WHERE l.quarter = :quarter")
    , @NamedQuery(name = "LotMemberProfit.findByPrepareid", query = "SELECT l FROM LotMemberProfit l WHERE l.prepareid = :prepareid")
    , @NamedQuery(name = "LotMemberProfit.findByProfit", query = "SELECT l FROM LotMemberProfit l WHERE l.profit = :profit")
    , @NamedQuery(name = "LotMemberProfit.findByHectarage", query = "SELECT l FROM LotMemberProfit l WHERE l.hectarage = :hectarage")
    , @NamedQuery(name = "LotMemberProfit.findByAcre", query = "SELECT l FROM LotMemberProfit l WHERE l.acre = :acre")})
public class LotMemberProfit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "code")
    private String code;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "lotcode")
    private String lotcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Size(max = 50)
    @Column(name = "quarter")
    private String quarter;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "profit")
    private double profit;
    @Size(max = 20)
    @Column(name = "hectarage")
    private String hectarage;
    @Size(max = 20)
    @Column(name = "acre")
    private String acre;
    
    private String accno;

    public LotMemberProfit() {
    }

    public LotMemberProfit(Integer id) {
        this.id = id;
    }

    public LotMemberProfit(Integer id, String code, String refer, String lotcode, String year, String period, String prepareid, double profit, String accno) {
        this.id = id;
        this.code = code;
        this.refer = refer;
        this.lotcode = lotcode;
        this.year = year;
        this.period = period;
        this.prepareid = prepareid;
        this.profit = profit;
        this.accno = accno;
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getLotcode() {
        return lotcode;
    }

    public void setLotcode(String lotcode) {
        this.lotcode = lotcode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public String getHectarage() {
        return hectarage;
    }

    public void setHectarage(String hectarage) {
        this.hectarage = hectarage;
    }

    public String getAcre() {
        return acre;
    }

    public void setAcre(String acre) {
        this.acre = acre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotMemberProfit)) {
            return false;
        }
        LotMemberProfit other = (LotMemberProfit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.LotMemberProfit[ id=" + id + " ]";
    }
    
}
