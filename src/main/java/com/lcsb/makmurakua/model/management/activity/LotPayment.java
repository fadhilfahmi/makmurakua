/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class LotPayment {
    
    private String typePayment;
    private String bank;
    private String totalMember;
    private double totalAmount;
    private List<LotMemberProfit> lotMemberProfit;

    public String getTypePayment() {
        return typePayment;
    }

    public void setTypePayment(String typePayment) {
        this.typePayment = typePayment;
    }

    public List<LotMemberProfit> getLotMemberProfit() {
        return lotMemberProfit;
    }

    public void setLotMemberProfit(List<LotMemberProfit> lotMemberProfit) {
        this.lotMemberProfit = lotMemberProfit;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(String totalMember) {
        this.totalMember = totalMember;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    
    
}
