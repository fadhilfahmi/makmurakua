/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_rental_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcRentalDetail.findAll", query = "SELECT a FROM AcRentalDetail a")
    , @NamedQuery(name = "AcRentalDetail.findByRefer", query = "SELECT a FROM AcRentalDetail a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcRentalDetail.findByReferno", query = "SELECT a FROM AcRentalDetail a WHERE a.referno = :referno")
    , @NamedQuery(name = "AcRentalDetail.findByArea", query = "SELECT a FROM AcRentalDetail a WHERE a.area = :area")
    , @NamedQuery(name = "AcRentalDetail.findByAmount", query = "SELECT a FROM AcRentalDetail a WHERE a.amount = :amount")
    , @NamedQuery(name = "AcRentalDetail.findByTaxcoacode", query = "SELECT a FROM AcRentalDetail a WHERE a.taxcoacode = :taxcoacode")
    , @NamedQuery(name = "AcRentalDetail.findByTaxcoadescp", query = "SELECT a FROM AcRentalDetail a WHERE a.taxcoadescp = :taxcoadescp")
    , @NamedQuery(name = "AcRentalDetail.findByTaxrate", query = "SELECT a FROM AcRentalDetail a WHERE a.taxrate = :taxrate")
    , @NamedQuery(name = "AcRentalDetail.findByTaxamt", query = "SELECT a FROM AcRentalDetail a WHERE a.taxamt = :taxamt")
    , @NamedQuery(name = "AcRentalDetail.findByTaxcode", query = "SELECT a FROM AcRentalDetail a WHERE a.taxcode = :taxcode")
    , @NamedQuery(name = "AcRentalDetail.findByTaxdescp", query = "SELECT a FROM AcRentalDetail a WHERE a.taxdescp = :taxdescp")})
public class AcRentalDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 19)
    @Column(name = "referno")
    private String referno;
    @Lob
    @Size(max = 65535)
    @Column(name = "remark")
    private String remark;
    @Size(max = 50)
    @Column(name = "area")
    private String area;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Size(max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Size(max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Size(max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Size(max = 100)
    @Column(name = "taxdescp")
    private String taxdescp;

    public AcRentalDetail() {
    }

    public AcRentalDetail(String referno) {
        this.referno = referno;
    }

    public AcRentalDetail(String referno, String refer, double amount, double taxrate, double taxamt) {
        this.referno = referno;
        this.refer = refer;
        this.amount = amount;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getReferno() {
        return referno;
    }

    public void setReferno(String referno) {
        this.referno = referno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referno != null ? referno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcRentalDetail)) {
            return false;
        }
        AcRentalDetail other = (AcRentalDetail) object;
        if ((this.referno == null && other.referno != null) || (this.referno != null && !this.referno.equals(other.referno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcRentalDetail[ referno=" + referno + " ]";
    }
    
}
