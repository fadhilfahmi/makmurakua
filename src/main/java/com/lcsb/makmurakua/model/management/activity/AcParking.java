/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_parking")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcParking.findAll", query = "SELECT a FROM AcParking a")
    , @NamedQuery(name = "AcParking.findByRefer", query = "SELECT a FROM AcParking a WHERE a.refer = :refer")
    , @NamedQuery(name = "AcParking.findByYear", query = "SELECT a FROM AcParking a WHERE a.year = :year")
    , @NamedQuery(name = "AcParking.findByPeriod", query = "SELECT a FROM AcParking a WHERE a.period = :period")
    , @NamedQuery(name = "AcParking.findByDate", query = "SELECT a FROM AcParking a WHERE a.date = :date")
    , @NamedQuery(name = "AcParking.findByBearerID", query = "SELECT a FROM AcParking a WHERE a.bearerID = :bearerID")
    , @NamedQuery(name = "AcParking.findByBearername", query = "SELECT a FROM AcParking a WHERE a.bearername = :bearername")
    , @NamedQuery(name = "AcParking.findByPrepareID", query = "SELECT a FROM AcParking a WHERE a.prepareID = :prepareID")
    , @NamedQuery(name = "AcParking.findByPreparename", query = "SELECT a FROM AcParking a WHERE a.preparename = :preparename")
    , @NamedQuery(name = "AcParking.findByPreparedate", query = "SELECT a FROM AcParking a WHERE a.preparedate = :preparedate")
    , @NamedQuery(name = "AcParking.findByPreparepost", query = "SELECT a FROM AcParking a WHERE a.preparepost = :preparepost")
    , @NamedQuery(name = "AcParking.findByStatus", query = "SELECT a FROM AcParking a WHERE a.status = :status")
    , @NamedQuery(name = "AcParking.findByRemark", query = "SELECT a FROM AcParking a WHERE a.remark = :remark")
    , @NamedQuery(name = "AcParking.findByBankcode", query = "SELECT a FROM AcParking a WHERE a.bankcode = :bankcode")
    , @NamedQuery(name = "AcParking.findByBankname", query = "SELECT a FROM AcParking a WHERE a.bankname = :bankname")
    , @NamedQuery(name = "AcParking.findByCompanycode", query = "SELECT a FROM AcParking a WHERE a.companycode = :companycode")
    , @NamedQuery(name = "AcParking.findByCompanyname", query = "SELECT a FROM AcParking a WHERE a.companyname = :companyname")})
public class AcParking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "bearerID")
    private String bearerID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "bearername")
    private String bearername;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "prepareID")
    private String prepareID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "preparepost")
    private String preparepost;
    @Size(max = 10)
    @Column(name = "status")
    private String status;
    @Size(max = 300)
    @Column(name = "remark")
    private String remark;
    @Size(max = 10)
    @Column(name = "bankcode")
    private String bankcode;
    @Size(max = 100)
    @Column(name = "bankname")
    private String bankname;
    @Size(max = 10)
    @Column(name = "companycode")
    private String companycode;
    @Size(max = 150)
    @Column(name = "companyname")
    private String companyname;
    private double amount;
    private String bearertype;
    private String pvno;

    public AcParking() {
    }

    public AcParking(String refer) {
        this.refer = refer;
    }

    public AcParking(String refer, String year, String period, String date, String bearerID, String bearername, String prepareID, String preparename, String preparedate, String preparepost, double amount, String bearertype, String pvno) {
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.date = date;
        this.bearerID = bearerID;
        this.bearername = bearername;
        this.prepareID = prepareID;
        this.preparename = preparename;
        this.preparedate = preparedate;
        this.preparepost = preparepost;
        this.amount = amount;
        this.bearertype = bearertype;
        this.pvno = pvno;
    }

    public String getPvno() {
        return pvno;
    }

    public void setPvno(String pvno) {
        this.pvno = pvno;
    }

    public String getBearertype() {
        return bearertype;
    }

    public void setBearertype(String bearertype) {
        this.bearertype = bearertype;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBearerID() {
        return bearerID;
    }

    public void setBearerID(String bearerID) {
        this.bearerID = bearerID;
    }

    public String getBearername() {
        return bearername;
    }

    public void setBearername(String bearername) {
        this.bearername = bearername;
    }

    public String getPrepareID() {
        return prepareID;
    }

    public void setPrepareID(String prepareID) {
        this.prepareID = prepareID;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getPreparepost() {
        return preparepost;
    }

    public void setPreparepost(String preparepost) {
        this.preparepost = preparepost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcParking)) {
            return false;
        }
        AcParking other = (AcParking) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.AcParking[ refer=" + refer + " ]";
    }

}
