/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_lot_temp_member")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotTempMember.findAll", query = "SELECT l FROM LotTempMember l"),
    @NamedQuery(name = "LotTempMember.findById", query = "SELECT l FROM LotTempMember l WHERE l.id = :id"),
    @NamedQuery(name = "LotTempMember.findByCode", query = "SELECT l FROM LotTempMember l WHERE l.code = :code"),
    @NamedQuery(name = "LotTempMember.findByName", query = "SELECT l FROM LotTempMember l WHERE l.name = :name"),
    @NamedQuery(name = "LotTempMember.findBySessionid", query = "SELECT l FROM LotTempMember l WHERE l.sessionid = :sessionid"),
    @NamedQuery(name = "LotTempMember.findByLotcode", query = "SELECT l FROM LotTempMember l WHERE l.lotcode = :lotcode"),
    @NamedQuery(name = "LotTempMember.findByYear", query = "SELECT l FROM LotTempMember l WHERE l.year = :year"),
    @NamedQuery(name = "LotTempMember.findByPeriod", query = "SELECT l FROM LotTempMember l WHERE l.period = :period"),
    @NamedQuery(name = "LotTempMember.findByQuarter", query = "SELECT l FROM LotTempMember l WHERE l.quarter = :quarter")})
public class LotTempMember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sessionid")
    private String sessionid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "lotcode")
    private String lotcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Size(max = 50)
    @Column(name = "quarter")
    private String quarter;
    private String prepareid;

    public LotTempMember() {
    }

    public LotTempMember(Integer id) {
        this.id = id;
    }

    public LotTempMember(Integer id, String code, String name, String sessionid, String lotcode, String year, String period, String prepareid) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.sessionid = sessionid;
        this.lotcode = lotcode;
        this.year = year;
        this.period = period;
        this.prepareid = prepareid;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getLotcode() {
        return lotcode;
    }

    public void setLotcode(String lotcode) {
        this.lotcode = lotcode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotTempMember)) {
            return false;
        }
        LotTempMember other = (LotTempMember) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.LotTempMember[ id=" + id + " ]";
    }
    
}
