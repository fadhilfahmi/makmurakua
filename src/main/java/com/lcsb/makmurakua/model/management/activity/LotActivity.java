/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.activity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ac_lot_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotActivity.findAll", query = "SELECT l FROM LotActivity l"),
    @NamedQuery(name = "LotActivity.findByDate", query = "SELECT l FROM LotActivity l WHERE l.date = :date"),
    @NamedQuery(name = "LotActivity.findByAppid", query = "SELECT l FROM LotActivity l WHERE l.appid = :appid"),
    @NamedQuery(name = "LotActivity.findByAppname", query = "SELECT l FROM LotActivity l WHERE l.appname = :appname"),
    @NamedQuery(name = "LotActivity.findByAppdesign", query = "SELECT l FROM LotActivity l WHERE l.appdesign = :appdesign"),
    @NamedQuery(name = "LotActivity.findByPreid", query = "SELECT l FROM LotActivity l WHERE l.preid = :preid"),
    @NamedQuery(name = "LotActivity.findByPrename", query = "SELECT l FROM LotActivity l WHERE l.prename = :prename"),
    @NamedQuery(name = "LotActivity.findByCheckid", query = "SELECT l FROM LotActivity l WHERE l.checkid = :checkid"),
    @NamedQuery(name = "LotActivity.findByCheckname", query = "SELECT l FROM LotActivity l WHERE l.checkname = :checkname"),
    @NamedQuery(name = "LotActivity.findByRefer", query = "SELECT l FROM LotActivity l WHERE l.refer = :refer"),
    @NamedQuery(name = "LotActivity.findByYear", query = "SELECT l FROM LotActivity l WHERE l.year = :year"),
    @NamedQuery(name = "LotActivity.findByPeriod", query = "SELECT l FROM LotActivity l WHERE l.period = :period"),
    @NamedQuery(name = "LotActivity.findByEstatecode", query = "SELECT l FROM LotActivity l WHERE l.estatecode = :estatecode"),
    @NamedQuery(name = "LotActivity.findByEstatename", query = "SELECT l FROM LotActivity l WHERE l.estatename = :estatename"),
    @NamedQuery(name = "LotActivity.findByAmount", query = "SELECT l FROM LotActivity l WHERE l.amount = :amount"),
    @NamedQuery(name = "LotActivity.findByCheckdate", query = "SELECT l FROM LotActivity l WHERE l.checkdate = :checkdate"),
    @NamedQuery(name = "LotActivity.findByPredate", query = "SELECT l FROM LotActivity l WHERE l.predate = :predate"),
    @NamedQuery(name = "LotActivity.findByAppdate", query = "SELECT l FROM LotActivity l WHERE l.appdate = :appdate"),
    @NamedQuery(name = "LotActivity.findByLotcode", query = "SELECT l FROM LotActivity l WHERE l.lotcode = :lotcode"),
    @NamedQuery(name = "LotActivity.findByLotdesc", query = "SELECT l FROM LotActivity l WHERE l.lotdesc = :lotdesc"),
    @NamedQuery(name = "LotActivity.findByQuarter", query = "SELECT l FROM LotActivity l WHERE l.quarter = :quarter")})
public class LotActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Column(name = "checkdate")
    @Temporal(TemporalType.DATE)
    private Date checkdate;
    @Column(name = "predate")
    @Temporal(TemporalType.DATE)
    private Date predate;
    @Column(name = "appdate")
    @Temporal(TemporalType.DATE)
    private Date appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "lotcode")
    private String lotcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lotdesc")
    private String lotdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "quarter")
    private String quarter;

    public LotActivity() {
    }

    public LotActivity(String refer) {
        this.refer = refer;
    }

    public LotActivity(String refer, String remark, String appid, String appname, String appdesign, String preid, String prename, String checkid, String checkname, String year, String period, String estatecode, String estatename, double amount, String lotcode, String lotdesc, String quarter) {
        this.refer = refer;
        this.remark = remark;
        this.appid = appid;
        this.appname = appname;
        this.appdesign = appdesign;
        this.preid = preid;
        this.prename = prename;
        this.checkid = checkid;
        this.checkname = checkname;
        this.year = year;
        this.period = period;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.amount = amount;
        this.lotcode = lotcode;
        this.lotdesc = lotdesc;
        this.quarter = quarter;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(Date checkdate) {
        this.checkdate = checkdate;
    }

    public Date getPredate() {
        return predate;
    }

    public void setPredate(Date predate) {
        this.predate = predate;
    }

    public Date getAppdate() {
        return appdate;
    }

    public void setAppdate(Date appdate) {
        this.appdate = appdate;
    }

    public String getLotcode() {
        return lotcode;
    }

    public void setLotcode(String lotcode) {
        this.lotcode = lotcode;
    }

    public String getLotdesc() {
        return lotdesc;
    }

    public void setLotdesc(String lotdesc) {
        this.lotdesc = lotdesc;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotActivity)) {
            return false;
        }
        LotActivity other = (LotActivity) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.activity.LotActivity[ refer=" + refer + " ]";
    }
    
}
