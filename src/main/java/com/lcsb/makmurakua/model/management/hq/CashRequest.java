/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.management.hq;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "hq_cashreq_master")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CashRequest.findAll", query = "SELECT c FROM CashRequest c")
    , @NamedQuery(name = "CashRequest.findByRefer", query = "SELECT c FROM CashRequest c WHERE c.refer = :refer")
    , @NamedQuery(name = "CashRequest.findByYear", query = "SELECT c FROM CashRequest c WHERE c.year = :year")
    , @NamedQuery(name = "CashRequest.findByPeriod", query = "SELECT c FROM CashRequest c WHERE c.period = :period")
    , @NamedQuery(name = "CashRequest.findByEstatedesc", query = "SELECT c FROM CashRequest c WHERE c.estatedesc = :estatedesc")
    , @NamedQuery(name = "CashRequest.findByEstatecode", query = "SELECT c FROM CashRequest c WHERE c.estatecode = :estatecode")
    , @NamedQuery(name = "CashRequest.findByMgtdescp", query = "SELECT c FROM CashRequest c WHERE c.mgtdescp = :mgtdescp")
    , @NamedQuery(name = "CashRequest.findByMgtcode", query = "SELECT c FROM CashRequest c WHERE c.mgtcode = :mgtcode")
    , @NamedQuery(name = "CashRequest.findByStartdate", query = "SELECT c FROM CashRequest c WHERE c.startdate = :startdate")
    , @NamedQuery(name = "CashRequest.findByEnddate", query = "SELECT c FROM CashRequest c WHERE c.enddate = :enddate")
    , @NamedQuery(name = "CashRequest.findByAppid", query = "SELECT c FROM CashRequest c WHERE c.appid = :appid")
    , @NamedQuery(name = "CashRequest.findByAppname", query = "SELECT c FROM CashRequest c WHERE c.appname = :appname")
    , @NamedQuery(name = "CashRequest.findByAppdesign", query = "SELECT c FROM CashRequest c WHERE c.appdesign = :appdesign")
    , @NamedQuery(name = "CashRequest.findByAppdate", query = "SELECT c FROM CashRequest c WHERE c.appdate = :appdate")
    , @NamedQuery(name = "CashRequest.findByAddappdate", query = "SELECT c FROM CashRequest c WHERE c.addappdate = :addappdate")
    , @NamedQuery(name = "CashRequest.findByAddappdesign", query = "SELECT c FROM CashRequest c WHERE c.addappdesign = :addappdesign")
    , @NamedQuery(name = "CashRequest.findByAddappid", query = "SELECT c FROM CashRequest c WHERE c.addappid = :addappid")
    , @NamedQuery(name = "CashRequest.findByAddappname", query = "SELECT c FROM CashRequest c WHERE c.addappname = :addappname")
    , @NamedQuery(name = "CashRequest.findByAddcid", query = "SELECT c FROM CashRequest c WHERE c.addcid = :addcid")
    , @NamedQuery(name = "CashRequest.findByAddcdate", query = "SELECT c FROM CashRequest c WHERE c.addcdate = :addcdate")
    , @NamedQuery(name = "CashRequest.findByAddcname", query = "SELECT c FROM CashRequest c WHERE c.addcname = :addcname")
    , @NamedQuery(name = "CashRequest.findByAddpname", query = "SELECT c FROM CashRequest c WHERE c.addpname = :addpname")
    , @NamedQuery(name = "CashRequest.findByAddpdate", query = "SELECT c FROM CashRequest c WHERE c.addpdate = :addpdate")
    , @NamedQuery(name = "CashRequest.findByAddpid", query = "SELECT c FROM CashRequest c WHERE c.addpid = :addpid")
    , @NamedQuery(name = "CashRequest.findByPreid", query = "SELECT c FROM CashRequest c WHERE c.preid = :preid")
    , @NamedQuery(name = "CashRequest.findByPrename", query = "SELECT c FROM CashRequest c WHERE c.prename = :prename")
    , @NamedQuery(name = "CashRequest.findByPredate", query = "SELECT c FROM CashRequest c WHERE c.predate = :predate")
    , @NamedQuery(name = "CashRequest.findByCheckid", query = "SELECT c FROM CashRequest c WHERE c.checkid = :checkid")
    , @NamedQuery(name = "CashRequest.findByCheckname", query = "SELECT c FROM CashRequest c WHERE c.checkname = :checkname")
    , @NamedQuery(name = "CashRequest.findByCheckdate", query = "SELECT c FROM CashRequest c WHERE c.checkdate = :checkdate")
    , @NamedQuery(name = "CashRequest.findByRemarks", query = "SELECT c FROM CashRequest c WHERE c.remarks = :remarks")})
public class CashRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatedesc")
    private String estatedesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtdescp")
    private String mgtdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode")
    private String mgtcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "startdate")
    private String startdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "enddate")
    private String enddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addappdate")
    private String addappdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addappdesign")
    private String addappdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addappid")
    private String addappid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addappname")
    private String addappname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addcid")
    private String addcid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addcdate")
    private String addcdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addcname")
    private String addcname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addpname")
    private String addpname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addpdate")
    private String addpdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "addpid")
    private String addpid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predate")
    private String predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;

    public CashRequest() {
    }

    public CashRequest(String refer) {
        this.refer = refer;
    }

    public CashRequest(String refer, String year, String period, String estatedesc, String estatecode, String mgtdescp, String mgtcode, String startdate, String enddate, String appid, String appname, String appdesign, String appdate, String addappdate, String addappdesign, String addappid, String addappname, String addcid, String addcdate, String addcname, String addpname, String addpdate, String addpid, String preid, String prename, String predate, String checkid, String checkname, String checkdate, String remarks) {
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.estatedesc = estatedesc;
        this.estatecode = estatecode;
        this.mgtdescp = mgtdescp;
        this.mgtcode = mgtcode;
        this.startdate = startdate;
        this.enddate = enddate;
        this.appid = appid;
        this.appname = appname;
        this.appdesign = appdesign;
        this.appdate = appdate;
        this.addappdate = addappdate;
        this.addappdesign = addappdesign;
        this.addappid = addappid;
        this.addappname = addappname;
        this.addcid = addcid;
        this.addcdate = addcdate;
        this.addcname = addcname;
        this.addpname = addpname;
        this.addpdate = addpdate;
        this.addpid = addpid;
        this.preid = preid;
        this.prename = prename;
        this.predate = predate;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkdate = checkdate;
        this.remarks = remarks;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getEstatedesc() {
        return estatedesc;
    }

    public void setEstatedesc(String estatedesc) {
        this.estatedesc = estatedesc;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getMgtdescp() {
        return mgtdescp;
    }

    public void setMgtdescp(String mgtdescp) {
        this.mgtdescp = mgtdescp;
    }

    public String getMgtcode() {
        return mgtcode;
    }

    public void setMgtcode(String mgtcode) {
        this.mgtcode = mgtcode;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getAddappdate() {
        return addappdate;
    }

    public void setAddappdate(String addappdate) {
        this.addappdate = addappdate;
    }

    public String getAddappdesign() {
        return addappdesign;
    }

    public void setAddappdesign(String addappdesign) {
        this.addappdesign = addappdesign;
    }

    public String getAddappid() {
        return addappid;
    }

    public void setAddappid(String addappid) {
        this.addappid = addappid;
    }

    public String getAddappname() {
        return addappname;
    }

    public void setAddappname(String addappname) {
        this.addappname = addappname;
    }

    public String getAddcid() {
        return addcid;
    }

    public void setAddcid(String addcid) {
        this.addcid = addcid;
    }

    public String getAddcdate() {
        return addcdate;
    }

    public void setAddcdate(String addcdate) {
        this.addcdate = addcdate;
    }

    public String getAddcname() {
        return addcname;
    }

    public void setAddcname(String addcname) {
        this.addcname = addcname;
    }

    public String getAddpname() {
        return addpname;
    }

    public void setAddpname(String addpname) {
        this.addpname = addpname;
    }

    public String getAddpdate() {
        return addpdate;
    }

    public void setAddpdate(String addpdate) {
        this.addpdate = addpdate;
    }

    public String getAddpid() {
        return addpid;
    }

    public void setAddpid(String addpid) {
        this.addpid = addpid;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPredate() {
        return predate;
    }

    public void setPredate(String predate) {
        this.predate = predate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashRequest)) {
            return false;
        }
        CashRequest other = (CashRequest) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.management.hq.CashRequest[ refer=" + refer + " ]";
    }
    
}
