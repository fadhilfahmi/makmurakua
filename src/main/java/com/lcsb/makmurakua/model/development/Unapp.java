/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.development;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "unapp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unapp.findAll", query = "SELECT u FROM Unapp u"),
    @NamedQuery(name = "Unapp.findByCode", query = "SELECT u FROM Unapp u WHERE u.code = :code"),
    @NamedQuery(name = "Unapp.findBySubject", query = "SELECT u FROM Unapp u WHERE u.subject = :subject"),
    @NamedQuery(name = "Unapp.findByTable", query = "SELECT u FROM Unapp u WHERE u.table = :table"),
    @NamedQuery(name = "Unapp.findByReferal", query = "SELECT u FROM Unapp u WHERE u.referal = :referal"),
    @NamedQuery(name = "Unapp.findByAbb", query = "SELECT u FROM Unapp u WHERE u.abb = :abb"),
    @NamedQuery(name = "Unapp.findByApp", query = "SELECT u FROM Unapp u WHERE u.app = :app"),
    @NamedQuery(name = "Unapp.findByCheck", query = "SELECT u FROM Unapp u WHERE u.check = :check"),
    @NamedQuery(name = "Unapp.findByPost", query = "SELECT u FROM Unapp u WHERE u.post = :post"),
    @NamedQuery(name = "Unapp.findByPostvalue", query = "SELECT u FROM Unapp u WHERE u.postvalue = :postvalue")})
public class Unapp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code")
    private String code;
    @Size(max = 100)
    @Column(name = "subject")
    private String subject;
    @Size(max = 100)
    @Column(name = "table")
    private String table;
    @Size(max = 100)
    @Column(name = "referal")
    private String referal;
    @Size(max = 100)
    @Column(name = "abb")
    private String abb;
    @Size(max = 100)
    @Column(name = "app")
    private String app;
    @Size(max = 100)
    @Column(name = "check")
    private String check;
    @Size(max = 100)
    @Column(name = "post")
    private String post;
    @Size(max = 100)
    @Column(name = "postvalue")
    private String postvalue;

    public Unapp() {
    }

    public Unapp(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getReferal() {
        return referal;
    }

    public void setReferal(String referal) {
        this.referal = referal;
    }

    public String getAbb() {
        return abb;
    }

    public void setAbb(String abb) {
        this.abb = abb;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPostvalue() {
        return postvalue;
    }

    public void setPostvalue(String postvalue) {
        this.postvalue = postvalue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unapp)) {
            return false;
        }
        Unapp other = (Unapp) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.development.Unapp[ code=" + code + " ]";
    }
    
}
