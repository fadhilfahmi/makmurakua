/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.development;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sec_module")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SecModule.findAll", query = "SELECT s FROM SecModule s"),
    @NamedQuery(name = "SecModule.findByModuleid", query = "SELECT s FROM SecModule s WHERE s.moduleid = :moduleid"),
    @NamedQuery(name = "SecModule.findByModuledesc", query = "SELECT s FROM SecModule s WHERE s.moduledesc = :moduledesc"),
    @NamedQuery(name = "SecModule.findByHyperlink", query = "SELECT s FROM SecModule s WHERE s.hyperlink = :hyperlink"),
    @NamedQuery(name = "SecModule.findByView", query = "SELECT s FROM SecModule s WHERE s.view = :view"),
    @NamedQuery(name = "SecModule.findByDisable", query = "SELECT s FROM SecModule s WHERE s.disable = :disable"),
    @NamedQuery(name = "SecModule.findByFinallevel", query = "SELECT s FROM SecModule s WHERE s.finallevel = :finallevel"),
    @NamedQuery(name = "SecModule.findByPathcontrol", query = "SELECT s FROM SecModule s WHERE s.pathcontrol = :pathcontrol"),
    @NamedQuery(name = "SecModule.findBySortid", query = "SELECT s FROM SecModule s WHERE s.sortid = :sortid"),
    @NamedQuery(name = "SecModule.findByIcon", query = "SELECT s FROM SecModule s WHERE s.icon = :icon"),
    @NamedQuery(name = "SecModule.findByMaintable", query = "SELECT s FROM SecModule s WHERE s.maintable = :maintable"),
    @NamedQuery(name = "SecModule.findBySubtable", query = "SELECT s FROM SecModule s WHERE s.subtable = :subtable"),
    @NamedQuery(name = "SecModule.findByAbb", query = "SELECT s FROM SecModule s WHERE s.abb = :abb"),
    @NamedQuery(name = "SecModule.findByReferidMaster", query = "SELECT s FROM SecModule s WHERE s.referidMaster = :referidMaster"),
    @NamedQuery(name = "SecModule.findByReferidSub", query = "SELECT s FROM SecModule s WHERE s.referidSub = :referidSub"),
    @NamedQuery(name = "SecModule.findByApprove", query = "SELECT s FROM SecModule s WHERE s.approve = :approve"),
    @NamedQuery(name = "SecModule.findByCheck", query = "SELECT s FROM SecModule s WHERE s.check = :check"),
    @NamedQuery(name = "SecModule.findByPost", query = "SELECT s FROM SecModule s WHERE s.post = :post")})
public class SecModule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "moduleid")
    private String moduleid;
    @Size(max = 100)
    @Column(name = "moduledesc")
    private String moduledesc;
    @Size(max = 200)
    @Column(name = "hyperlink")
    private String hyperlink;
    @Column(name = "view")
    private Boolean view;
    @Column(name = "disable")
    private Boolean disable;
    @Column(name = "finallevel")
    private Boolean finallevel;
    @Size(max = 100)
    @Column(name = "pathcontrol")
    private String pathcontrol;
    @Column(name = "sortid")
    private Integer sortid;
    @Size(max = 100)
    @Column(name = "icon")
    private String icon;
    @Size(max = 100)
    @Column(name = "maintable")
    private String maintable;
    @Size(max = 100)
    @Column(name = "subtable")
    private String subtable;
    @Size(max = 50)
    @Column(name = "abb")
    private String abb;
    @Size(max = 100)
    @Column(name = "referid_master")
    private String referidMaster;
    @Size(max = 100)
    @Column(name = "referid_sub")
    private String referidSub;
    @Size(max = 100)
    @Column(name = "approve")
    private String approve;
    @Size(max = 100)
    @Column(name = "check")
    private String check;
    @Size(max = 100)
    @Column(name = "post")
    private String post;

    public SecModule() {
    }

    public SecModule(String moduleid) {
        this.moduleid = moduleid;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    public String getModuledesc() {
        return moduledesc;
    }

    public void setModuledesc(String moduledesc) {
        this.moduledesc = moduledesc;
    }

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    public Boolean getView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public Boolean getFinallevel() {
        return finallevel;
    }

    public void setFinallevel(Boolean finallevel) {
        this.finallevel = finallevel;
    }

    public String getPathcontrol() {
        return pathcontrol;
    }

    public void setPathcontrol(String pathcontrol) {
        this.pathcontrol = pathcontrol;
    }

    public Integer getSortid() {
        return sortid;
    }

    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMaintable() {
        return maintable;
    }

    public void setMaintable(String maintable) {
        this.maintable = maintable;
    }

    public String getSubtable() {
        return subtable;
    }

    public void setSubtable(String subtable) {
        this.subtable = subtable;
    }

    public String getAbb() {
        return abb;
    }

    public void setAbb(String abb) {
        this.abb = abb;
    }

    public String getReferidMaster() {
        return referidMaster;
    }

    public void setReferidMaster(String referidMaster) {
        this.referidMaster = referidMaster;
    }

    public String getReferidSub() {
        return referidSub;
    }

    public void setReferidSub(String referidSub) {
        this.referidSub = referidSub;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (moduleid != null ? moduleid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecModule)) {
            return false;
        }
        SecModule other = (SecModule) object;
        if ((this.moduleid == null && other.moduleid != null) || (this.moduleid != null && !this.moduleid.equals(other.moduleid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.development.SecModule[ moduleid=" + moduleid + " ]";
    }
    
}
