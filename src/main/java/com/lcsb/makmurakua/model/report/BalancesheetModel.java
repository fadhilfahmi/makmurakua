/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.model.report;

/**
 *
 * @author Dell
 */
public class BalancesheetModel {
    private String accountCode;
    private String accountDescp;
    private Double totalCurrentAsset_now;
    private Double totalCurrentLiability_now;
    private Double totalFixedAsset_now;
    private Double totalCurrentAsset_then;
    private Double totalCurrentLiability_then;
    private Double totalFixedAsset_then;
    private Double totalCurrentAsset_thenend;
    private Double totalCurrentLiability_thenend;
    private Double totalFixedAsset_thenend;
    
    private Double totalNetCurrentAsset_now;
    private Double totalNetAsset_now;
    private Double totalNetCurrentAsset_then;
    private Double totalNetAsset_then;
    private Double totalNetCurrentAsset_thenend;
    private Double totalNetAsset_thenend;
    
    private Double totalNetFixedAsset_now;
    private Double totalNetFixedAsset_then;
    private Double totalNetFixedAsset_thenend;
    
    private Double accumulatedDepreciation_now;
    private Double accumulatedDepreciation_then;
    private Double accumulatedDepreciation_thenend;

    public Double getTotalNetFixedAsset_now() {
        return totalFixedAsset_now - accumulatedDepreciation_now;
    }

    public void setTotalNetFixedAsset_now(Double totalNetFixedAsset_now) {
        this.totalNetFixedAsset_now = totalNetFixedAsset_now;
    }

    public Double getTotalNetFixedAsset_then() {
        return totalFixedAsset_then - accumulatedDepreciation_then;
    }

    public void setTotalNetFixedAsset_then(Double totalNetFixedAsset_then) {
        this.totalNetFixedAsset_then = totalNetFixedAsset_then;
    }

    public Double getTotalNetFixedAsset_thenend() {
        return totalFixedAsset_thenend - accumulatedDepreciation_thenend;
    }

    public void setTotalNetFixedAsset_thenend(Double totalNetFixedAsset_thenend) {
        this.totalNetFixedAsset_thenend = totalNetFixedAsset_thenend; 
    }

    
    public Double getAccumulatedDepreciation_now() {
        return accumulatedDepreciation_now;
    }

    public void setAccumulatedDepreciation_now(Double accumulatedDepreciation_now) {
        this.accumulatedDepreciation_now = accumulatedDepreciation_now;
    }

    public Double getAccumulatedDepreciation_then() {
        return accumulatedDepreciation_then;
    }

    public void setAccumulatedDepreciation_then(Double accumulatedDepreciation_then) {
        this.accumulatedDepreciation_then = accumulatedDepreciation_then;
    }

    public Double getAccumulatedDepreciation_thenend() {
        return accumulatedDepreciation_thenend;
    }

    public void setAccumulatedDepreciation_thenend(Double accumulatedDepreciation_thenend) {
        this.accumulatedDepreciation_thenend = accumulatedDepreciation_thenend;
    }
    
    
    public Double getTotalNetCurrentAsset_now() {
        return totalCurrentAsset_now - totalCurrentLiability_now; 
    }

    public void setTotalNetCurrentAsset_now(Double totalNetCurrentAsset_now) {
        this.totalNetCurrentAsset_now = totalNetCurrentAsset_now;
    }

    public Double getTotalNetAsset_now() {
        return getTotalNetCurrentAsset_now() + getTotalNetFixedAsset_now();
    }

    public void setTotalNetAsset_now(Double totalNetAsset_now) {
        this.totalNetAsset_now = totalNetAsset_now;
    }

    public Double getTotalNetCurrentAsset_then() {
        return totalCurrentAsset_then-totalCurrentLiability_then;
    }

    public void setTotalNetCurrentAsset_then(Double totalNetCurrentAsset_then) {
        this.totalNetCurrentAsset_then =  totalNetCurrentAsset_then; 
    }

    public Double getTotalNetAsset_then() {
        return getTotalNetCurrentAsset_then() + getTotalNetFixedAsset_then();
    }

    public void setTotalNetAsset_then(Double totalNetAsset_then) {
        this.totalNetAsset_then = totalNetAsset_then;
    }

    public Double getTotalNetCurrentAsset_thenend() {
        return totalCurrentAsset_thenend-totalCurrentLiability_thenend; 
    }

    public void setTotalNetCurrentAsset_thenend(Double totalNetCurrentAsset_thenend) {
        this.totalNetCurrentAsset_thenend = totalNetCurrentAsset_thenend; 
    }

    public Double getTotalNetAsset_thenend() {
        return getTotalNetCurrentAsset_thenend() + getTotalNetFixedAsset_thenend();
    }

    public void setTotalNetAsset_thenend(Double totalNetAsset_thenend) {
        this.totalNetAsset_thenend = totalNetAsset_thenend;
    }

    
    public Double getTotalCurrentAsset_now() {
        return totalCurrentAsset_now;
    }

    public void setTotalCurrentAsset_now(Double totalCurrentAsset_now) {
        this.totalCurrentAsset_now = totalCurrentAsset_now;
    }

    public Double getTotalCurrentLiability_now() {
        return totalCurrentLiability_now;
    }

    public void setTotalCurrentLiability_now(Double totalCurrentLiability_now) {
        this.totalCurrentLiability_now = totalCurrentLiability_now;
    }

    public Double getTotalFixedAsset_now() {
        return totalFixedAsset_now;
    }

    public void setTotalFixedAsset_now(Double totalFixedAsset_now) {
        this.totalFixedAsset_now = totalFixedAsset_now;
    }

    public Double getTotalCurrentAsset_then() {
        return totalCurrentAsset_then;
    }

    public void setTotalCurrentAsset_then(Double totalCurrentAsset_then) {
        this.totalCurrentAsset_then = totalCurrentAsset_then;
    }

    public Double getTotalCurrentLiability_then() {
        return totalCurrentLiability_then;
    }

    public void setTotalCurrentLiability_then(Double totalCurrentLiability_then) {
        this.totalCurrentLiability_then = totalCurrentLiability_then;
    }

    public Double getTotalFixedAsset_then() {
        return totalFixedAsset_then;
    }

    public void setTotalFixedAsset_then(Double totalFixedAsset_then) {
        this.totalFixedAsset_then = totalFixedAsset_then;
    }

    public Double getTotalCurrentAsset_thenend() {
        return totalCurrentAsset_thenend;
    }

    public void setTotalCurrentAsset_thenend(Double totalCurrentAsset_thenend) {
        this.totalCurrentAsset_thenend = totalCurrentAsset_thenend;
    }

    public Double getTotalCurrentLiability_thenend() {
        return totalCurrentLiability_thenend;
    }

    public void setTotalCurrentLiability_thenend(Double totalCurrentLiability_thenend) {
        this.totalCurrentLiability_thenend = totalCurrentLiability_thenend;
    }

    public Double getTotalFixedAsset_thenend() {
        return totalFixedAsset_thenend;
    }

    public void setTotalFixedAsset_thenend(Double totalFixedAsset_thenend) {
        this.totalFixedAsset_thenend = totalFixedAsset_thenend;
    }

 
    public void setAccountCode(String accountCode){
        this.accountCode = accountCode;
    }
    
    public String getAccountCode(){
        return accountCode;
    }

    public String getAccountDescp() {
        return accountDescp;
    }

    public void setAccountDescp(String accountDescp) {
        this.accountDescp = accountDescp;
    }
    
}
