/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.model.report;

import java.util.ArrayList;

/**
 *
 * @author Dell
 */
public class ProfitnLossModel {
    private Double sumTotal = 0.00;
    private Double sumTotalActToDate = 0.00;
    private Double sumTotalBudThisMonth = 0.00;
    private Double sumTotalBudToDate = 0.00;
    private Double sumTotalBudYear = 0.00;
    private Double sumTotalPrevToDate = 0.00;
    private Double sumTotalPrevAllYear = 0.00;
    
    private Double sumAll = 0.00;
    private Double sumAllActToDate = 0.00;
    private Double sumAllBudThisMonth = 0.00;
    private Double sumAllBudToDate = 0.00;
    private Double sumAllBudYear = 0.00;
    private Double sumAllPrevToDate = 0.00;
    private Double sumAllPrevAllYear = 0.00;
    
    private final Double val = 0.00;
    private final ArrayList<Double> r =  new ArrayList<Double>();
    private final ArrayList<Double> s =  new ArrayList<Double>();
    private final ArrayList<Double> u =  new ArrayList<Double>();
    private final ArrayList<Double> v =  new ArrayList<Double>();
    private final ArrayList<Double> w =  new ArrayList<Double>();
    private final ArrayList<Double> x =  new ArrayList<Double>();
    private final ArrayList<Double> y =  new ArrayList<Double>();
    
    private Double netProfit;
    private Double netProfitActToDate;
    private Double netProfitBudThisMonth;
    private Double netProfitBudToDate;
    private Double netProfitBudYear;
    private Double netProfitPrevToDate;
    private Double netProfitPrevAllYear;
    
    //actual this month
    public void setTotal(Double sumTotal) {
       this.sumTotal += sumTotal;
    }
    
    public Double getTotal() {
        return sumTotal;
    }
    public void setSumTotal(Double sumAll){
        this.sumAll += sumAll;
    }
    
    public Double getAllTotal(){
        return sumAll;
    }
    
    public void setDeduction(Double val){
        r.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeduction(){
        return r;
    }
    public void setNetProfit(Double revenue, Double expense){
        this.netProfit = revenue - expense;
    }
    
    public Double getNetProfit(){
        
        return netProfit;
    }
    
    //actual to date
    public void setTotalActToDate(Double sumTotalActToDate) {
       this.sumTotalActToDate += sumTotalActToDate;
    }
    
    public Double getTotalActToDate() {
        return sumTotalActToDate;
    }
    public void setSumTotalActToDate(Double sumAllActToDate){
        this.sumAllActToDate += sumAllActToDate;
    }
    
    public Double getAllTotalActToDate(){
        return sumAllActToDate;
    }
    public void setDeductionActToDate(Double val){
        s.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionActToDate(){
        return s;
    }
    
    public void setNetProfitActToDate(Double revenue, Double expense){
        this.netProfitActToDate = revenue - expense;
    }
    
    public Double getNetProfitActToDate(){
        
        return netProfitActToDate;
    }
    
    
    //budget this month
    public void setTotalBudThisMonth(Double sumTotalBudThisMonth) {
       this.sumTotalBudThisMonth += sumTotalBudThisMonth;
    }
    
    public Double getTotalBudThisMonth() {
        return sumTotalBudThisMonth;
    }
    public void setSumTotalBudThisMonth(Double sumAllBudThisMonth){
        this.sumAllBudThisMonth += sumAllBudThisMonth;
    }
    
    public Double getAllTotalBudThisMonth(){
        return sumAllBudThisMonth;
    }
    public void setDeductionBudThisMonth(Double val){
        u.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionBudThisMonth(){
        return u;
    }
    
    public void setNetProfitBudThisMonth(Double revenue, Double expense){
        this.netProfitBudThisMonth = revenue - expense;
    }
    
    public Double getNetProfitBudThisMonth(){
        
        return netProfitBudThisMonth;
    }
    
    //budget to date
    public void setTotalBudToDate(Double sumTotalBudToDate) {
       this.sumTotalBudToDate += sumTotalBudToDate;
    }
    
    public Double getTotalBudToDate() {
        return sumTotalBudToDate;
    }
    public void setSumTotalBudToDate(Double sumAllBudToDate){
        this.sumAllBudToDate += sumAllBudToDate;
    }
    
    public Double getAllTotalBudToDate(){
        return sumAllBudToDate;
    }
    public void setDeductionBudToDate(Double val){
        v.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionBudToDate(){
        return v;
    }
    
    public void setNetProfitBudToDate(Double revenue, Double expense){
        this.netProfitBudToDate = revenue - expense;
    }
    
    public Double getNetProfitBudToDate(){
        
        return netProfitBudToDate;
    }
    
    //budget for year
    public void setTotalBudYear(Double sumTotalBudYear) {
       this.sumTotalBudYear += sumTotalBudYear;
    }
    
    public Double getTotalBudYear() {
        return sumTotalBudYear;
    }
    public void setSumTotalBudYear(Double sumAllBudYear){
        this.sumAllBudYear += sumAllBudYear;
    }
    
    public Double getAllTotalBudYear(){
        return sumAllBudYear;
    }
    public void setDeductionBudYear(Double val){
        w.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionBudYear(){
        return w;
    }
    
    public void setNetProfitBudYear(Double revenue, Double expense){
        this.netProfitBudYear = revenue - expense;
    }
    
    public Double getNetProfitBudYear(){
        
        return netProfitBudYear;
    }
    
    //previous to date
    public void setTotalPrevToDate(Double sumTotalPrevToDate) {
       this.sumTotalPrevToDate += sumTotalPrevToDate;
    }
    
    public Double getTotalPrevToDate() {
        return sumTotalPrevToDate;
    }
    public void setSumTotalPrevToDate(Double sumAllPrevToDate){
        this.sumAllPrevToDate += sumAllPrevToDate;
    }
    
    public Double getAllTotalPrevToDate(){
        return sumAllPrevToDate;
    }
    public void setDeductionPrevToDate(Double val){
        x.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionPrevToDate(){
        return x;
    }
    
    public void setNetProfitPrevToDate(Double revenue, Double expense){
        this.netProfitPrevToDate = revenue - expense;
    }
    
    public Double getNetProfitPrevToDate(){
        
        return netProfitPrevToDate;
    }
    
    //previous year all
    public void setTotalPrevAllYear(Double sumTotalPrevAllYear) {
       this.sumTotalPrevAllYear += sumTotalPrevAllYear;
    }
    
    public Double getTotalPrevAllYear() {
        return sumTotalPrevAllYear;
    }
    public void setSumTotalPrevAllYear(Double sumAllPrevAllYear){
        this.sumAllPrevAllYear += sumAllPrevAllYear;
    }
    
    public Double getAllTotalPrevAllYear(){
        return sumAllPrevAllYear;
    }
    public void setDeductionPrevAllYear(Double val){
        y.add(val);
        //this.val = val;
    }
    
    public ArrayList<Double> getDeductionPrevAllYear(){
        return y;
    }
    
    public void setNetProfitPrevAllYear(Double revenue, Double expense){
        this.netProfitPrevAllYear = revenue - expense;
    }
    
    public Double getNetProfitPrevAllYear(){
        
        return netProfitPrevAllYear;
    }
    
    
    
    
   
    
    
}
