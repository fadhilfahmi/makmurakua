/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.administration.om;

/**
 *
 * @author fadhilfahmi
 */
public class UserDashboardModuleSummary {
    
    private String refer;
    private String status;
    private String moduleid;
    private String actButton;
    private String attrButton;

    public String getAttrButton() {
        return attrButton;
    }

    public void setAttrButton(String attrButton) {
        this.attrButton = attrButton;
    }

    public String getActButton() {
        return actButton;
    }

    public void setActButton(String actButton) {
        this.actButton = actButton;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }
    
}
