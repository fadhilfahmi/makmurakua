/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.administration.om;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class UserAuth {
    
    private List<UserFunction> userFunction;
    private List<UserAccess> userAccess;
    private String userID;

    public List<UserFunction> getUserFunction() {
        return userFunction;
    }

    public void setUserFunction(List<UserFunction> userFunction) {
        this.userFunction = userFunction;
    }

    public List<UserAccess> getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(List<UserAccess> userAccess) {
        this.userAccess = userAccess;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
    
    
    
}
