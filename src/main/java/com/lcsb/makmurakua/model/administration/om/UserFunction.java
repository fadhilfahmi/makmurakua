/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.model.administration.om;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "om_user_function")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserFunction.findAll", query = "SELECT u FROM UserFunction u"),
    @NamedQuery(name = "UserFunction.findById", query = "SELECT u FROM UserFunction u WHERE u.id = :id"),
    @NamedQuery(name = "UserFunction.findByFunction", query = "SELECT u FROM UserFunction u WHERE u.function = :function"),
    @NamedQuery(name = "UserFunction.findByFunctionId", query = "SELECT u FROM UserFunction u WHERE u.functionId = :functionId")})
public class UserFunction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "function")
    private String function;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "function_id")
    private String functionId;

    public UserFunction() {
    }

    public UserFunction(Integer id) {
        this.id = id;
    }

    public UserFunction(Integer id, String function, String functionId) {
        this.id = id;
        this.function = function;
        this.functionId = functionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserFunction)) {
            return false;
        }
        UserFunction other = (UserFunction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.model.administration.om.UserFunction[ id=" + id + " ]";
    }
    
}
