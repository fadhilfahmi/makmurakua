/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.ext;

import com.lcsb.makmurakua.general.GeneralTerm;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ParseSafely {
    
    public static double parseDoubleSafely(String str) throws ParseException {
        double result = 0;
        try {
            result = GeneralTerm.amountFormattoSave(str);
        } catch (NullPointerException | NumberFormatException npe) {
            Logger.getLogger(ParseSafely.class.getName()).log(Level.INFO, str +"+++++++** "+npe);
        }
        return result;
    }
    
    public static int parseIntegerSafely(String str) {
        int result = 0;
        try {
            result = Integer.parseInt(str);
        } catch (NullPointerException | NumberFormatException npe) {
        }
        return result;
    }
    
   
}
