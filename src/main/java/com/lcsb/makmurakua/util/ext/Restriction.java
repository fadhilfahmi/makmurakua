/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.ext;

import com.lcsb.makmurakua.general.GeneralTerm;
import java.text.ParseException;

/**
 *
 * @author fadhilfahmi
 */
public class Restriction {
    
    public static String getEdit(String chk, String app) throws ParseException {
        
        String disable = "disabled";
        
        if(chk.equals("")){
            disable = "";
        }else if(!chk.equals("") && app.equals("")){
            disable = "disabled";
        }else if(!chk.equals("") && !app.equals("")){
            disable = "disabled";
        }
        
        return disable;
    }
    
    public static String getCheck(String str) throws ParseException {
        
        String disable = "disabled";
        
        if(str.equals("")){
            disable = "";
        }
        
        return disable;
    }
    
    public static String getApprove(String chk, String app) throws ParseException {
        
        String disable = "disabled";
        
        if(chk.equals("")){
            disable = "disabled";
        }else if(!chk.equals("") && app.equals("")){
            disable = "";
        }else if(!chk.equals("") && !app.equals("")){
            disable = "disabled";
        }
        
        return disable;
    }
    
}
