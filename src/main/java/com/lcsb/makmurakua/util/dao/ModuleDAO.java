/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.dao;


import com.lcsb.makmurakua.util.model.ConnectionModel;
import com.lcsb.makmurakua.util.model.ListTable;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Module;
import com.lcsb.makmurakua.util.model.ModuleItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ModuleDAO {
    
    public static List<Module> getAllModule(LoginProfile log, int lvl, String module) throws Exception {

        Statement stmt = null;
        List<Module> Modu;
        Modu = new ArrayList();
        
        String q = "";
        if(lvl==2){
            q = "";
        }else if(lvl>2){
            q = "and moduleid like '"+module+"%'";
        }
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sec_module where LENGTH(RTRIM(moduleid)) = "+lvl+" "+q+" and disable = 0  and disable = 0  order by moduleid");

            while (rs.next()) {
                Modu.add(getModule(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Modu;
    }
    
    public static Module getModule(LoginProfile log, String code) throws SQLException, Exception {
        Module c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sec_module where moduleid=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getModule(rs);
        }
        
        rs.close();
        stmt.close();

        
        return c;
    }
    
    public static Module getModuleFromReferenceNo(LoginProfile log, String refer) throws SQLException, Exception {
        
        
        Module c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sec_module where abb like ?");
        stmt.setString(1, "%"+refer.substring(0, 3)+"%");
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getModule(rs);
        }
        
        rs.close();
        stmt.close();

        
        return c;
    }
    
    private static Module getModule(ResultSet rs) throws SQLException {
        Module mod = new Module();
        mod.setModuleID(rs.getString("moduleid"));
        mod.setModuleDesc(rs.getString("moduledesc"));
        mod.setHyperlink(rs.getString("hyperlink"));
        mod.setFinalLevel(rs.getString("finallevel"));
        mod.setDisable(rs.getString("disable"));
        mod.setMainTable(rs.getString("maintable"));
        mod.setSubTable(rs.getString("subtable"));
        mod.setAbb(rs.getString("abb"));
        mod.setReferID_Master(rs.getString("referid_master"));
        mod.setReferID_Sub(rs.getString("referid_sub"));
        mod.setPathcontrol(rs.getString("pathcontrol"));
        mod.setApprove(rs.getString("approve"));
        mod.setPosttype(rs.getString("posttype"));
        mod.setPost(rs.getString("post"));
        mod.setCheck(rs.getString("check"));
        mod.setGl(rs.getBoolean("gl"));
        return mod;
    }
    
    public static List<ModuleItem> getAllModuleItem(LoginProfile log, String module, String table) throws Exception {
        Logger.getLogger(ModuleDAO.class.getName()).log(Level.INFO, module);
        Statement stmt = null;
        List<ModuleItem> Item;
        Item = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from "+table+" where module_id = '"+module+"'  order by position asc");

            while (rs.next()) {
                Item.add(getModuleItem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return Item;
    }
    
    public static ModuleItem getModuleItem(LoginProfile log, String code,String table) throws SQLException, Exception {
        ModuleItem c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from "+table+" where module_id=?  order by position asc");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getModuleItem(rs);
        }

        
        return c;
    }
    
    public static ModuleItem getModuleItemByAbb(LoginProfile log, String abb,String table) throws SQLException, Exception {
        ModuleItem c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from "+table+" where abb=?  order by position asc");
        stmt.setString(1, "%" + abb + "%");
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getModuleItem(rs);
        }

        
        return c;
    }
    
    private static ModuleItem getModuleItem(ResultSet rs) throws SQLException {
        ModuleItem mod = new ModuleItem();
        //[ "module_id","module_name","position","title","refer_id","column_name","type_field","attribute_1","attribute_2","default_value","param_name","field_size","listview","id_u","field_check","regex_check","date", "listform","autorefer","autovoucher","get_info","basecolumn","get_info_1stcolumn","get_info_2ndcolumn","get_info_table","get_info_table_column1", "get_info_table_column2" ];
        mod.setModule_ID(rs.getString("module_id"));
        mod.setModule_Name(rs.getString("module_name"));
        mod.setPosition(rs.getInt("position"));
        mod.setTitle(rs.getString("title"));
        mod.setRefer_ID(rs.getString("refer_id"));
        mod.setColumn_Name(rs.getString("column_name"));
        mod.setType_Field(rs.getString("type_field"));
        mod.setAttribute_1(rs.getString("attribute_1"));
        mod.setAttribute_2(rs.getString("attribute_2"));
        mod.setDefault_Value(rs.getString("default_value"));
        mod.setParam_Name(rs.getString("param_name"));
        mod.setField_Size(rs.getString("field_size"));
        mod.setList_View(rs.getString("listview"));
        mod.setId_u(rs.getInt("id_u"));
        mod.setField_Check(rs.getString("field_check"));
        mod.setRegex_Check(rs.getString("regex_check"));
        mod.setDate(rs.getString("date"));
        mod.setList_Form(rs.getString("listform"));
        mod.setAuto_Refer(rs.getString("autorefer"));
        mod.setAuto_Voucher(rs.getString("autovoucher"));
        mod.setAuto_4digitno(rs.getString("auto4digitno"));
        mod.setGet_info(rs.getString("get_info"));
        mod.setBase_Column(rs.getString("basecolumn"));
        mod.setGet_info_1stColumn(rs.getString("get_info_1stcolumn"));
        mod.setGet_info_2ncColumn(rs.getString("get_info_2ndcolumn"));
        mod.setGet_info_3rdColumn(rs.getString("get_info_3rdcolumn"));
        mod.setGet_info_Table(rs.getString("get_info_table"));
        mod.setGet_info_Table_Column1(rs.getString("get_info_table_column1"));
        mod.setGet_info_Table_Column2(rs.getString("get_info_table_column2"));
        mod.setGet_info_Table_Column3(rs.getString("get_info_table_column3"));
        mod.setConvertRM(rs.getString("convertrm"));
        mod.setConvertRM_base(rs.getString("convertrm_base"));
        mod.setConvertRM_to(rs.getString("convertrm_to"));
        mod.setAuto_referno(rs.getString("auto_referno"));
        mod.setCarry_referno(rs.getString("carry_mainrefer"));
        mod.setColumn_from(rs.getString("column_from"));
        mod.setSavedata(rs.getString("savedata"));
        mod.setJsfunction(rs.getString("jsfunction"));
        return mod;
    }
    
    public static String moduleABB(String type){
        
        String module_id = "";
        
        if(type.equals("JV")){
            module_id = "020104";
        }else if(type.equals("SNV")){
            module_id = "020903";
        }else if(type.equals("PV")){
            module_id = "020204";
        }else if(type.equals("OR")){
            module_id = "020203";
        }else if(type.equals("PNV")){
            module_id = "021008";
        }else if(type.equals("DN")){
            module_id = "020108";
        }else if(type.equals("CN")){
            module_id = "020109";
        }else if(type.equals("DNR")){
            module_id = "020908";
        }else if(type.equals("CNR")){
            module_id = "020909";
        }else if(type.equals("CV")){
            module_id = "020202";
        }else if(type.equals("CNP")){
            module_id = "021007";
        }
        
        return module_id;
    }

   
    

    
    public static List<Module> getModuleforGL(LoginProfile log) throws SQLException, Exception {
        
        List<Module> Mod;
        Mod = new ArrayList();
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sec_module where gl=?");
        stmt.setBoolean(1, true);
        rs = stmt.executeQuery();
        while (rs.next()) {
            Mod.add(getModule(rs));
        }
        
        rs.close();
        stmt.close();

        
        return Mod;
    }
    
}
