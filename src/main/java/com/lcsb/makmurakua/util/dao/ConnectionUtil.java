/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.ConnectionModel;
import com.lcsb.makmurakua.util.model.ServerList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ConnectionUtil {

    public static Connection getConnection(Connection con) throws Exception {

        Connection newcon = null;
        ResultSet rs = null;
        String dbname = "";

      

        try {

            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://localhost:3306/fms_dssb?zeroDateTimeBehavior=convertToNull";
            //String dbURL = "jdbc:mysql://mysql/"+dbname + "?zeroDateTimeBehavior=convertToNull";
            String dbuser = "root";
            String dbpass = "adminlcsb";
            newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);

        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        //createHQConnection();

        //con.close();

        return newcon;
    }

    public static Connection getTemporaryConnection() throws Exception {
        Connection newcon = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://localhost:3306/fms_general?zeroDateTimeBehavior=convertToNull";
            //String dbURL = "jdbc:mysql://mysql/fms_general?zeroDateTimeBehavior=convertToNull";
            String dbuser = "root";
            String dbpass = "adminlcsb";
            newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);
            ConnectionModel.setTempConnection(newcon);

        } catch (SQLException ex) {
            ex.printStackTrace();

        }

        return newcon;

    }

    public static Connection createHQConnection() throws Exception {
        Connection newcon = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://192.168.254.203:3306/ipamis_dbhq?zeroDateTimeBehavior=convertToNull";
            //String dbURL = "jdbc:mysql://localhost:3306/fms_general";
            //String dbURL = "jdbc:mysql://mysql/fms_dssb";
            String dbuser = "root";
            String dbpass = "adminlcsb";
            newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);
            ConnectionModel.setHQConnection(newcon);

        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newcon;

    }

    public static void closeConnection() throws SQLException {
        Connection con = ConnectionModel.getMainConnection();
        con.close();
    }

    public static void closeSessionConnection(Connection con) throws SQLException {
        con.close();
    }

    public static void closeMainConnection() throws Exception {

        ConnectionModel.getMainConnection().close();

    }

    public static void closeTemporaryConnection(Connection con) throws Exception {

        //con.close();

    }

    public static Connection getHQConnection() throws Exception {
        Connection con = ConnectionModel.getHQConnection();

        return con;

    }

    public static void closeHQConnection() throws SQLException {
        Connection con = ConnectionModel.getHQConnection();
        con.close();
    }

    public static Connection createMillConnection(String loccode) throws Exception {
        Connection newcon = null;

        ServerList svr = (ServerList) getServerInfo(loccode);

        try {

            Class.forName("com.mysql.jdbc.Driver");
            //String dbURL = "jdbc:mysql://localhost:3306/ipamis_nenasi?zeroDateTimeBehavior=convertToNull";
            //String dbuser = "root";
            //String dbpass = "adminlcsb";
            //newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);
            String dbURL = "jdbc:mysql://" + svr.getSvrip() + ":3306/" + svr.getFoldername() + "?zeroDateTimeBehavior=convertToNull";
            newcon = DriverManager.getConnection(dbURL, svr.getUsername(), svr.getPassword());
            ConnectionModel.setMillConnection(newcon);

        } catch (Exception ex) {
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newcon;

    }

    public static Connection getMillConnection(String code) throws Exception {
        Connection con = null;
        try {

            if (ConnectionModel.getMillConnection() == null) {
                con = createMillConnection(code);

            } else {
                con = ConnectionModel.getMillConnection();

            }
        } catch (Exception ex) {
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public static boolean checkMillConnection() {

        boolean i = false;

        if (ConnectionModel.getMillConnection() == null) {

        } else {
            i = true;
        }

        return i;

    }

    public static void closeMillConnection() throws SQLException {
        Connection con = ConnectionModel.getMillConnection();
        con.close();
    }

    public static ServerList getServerInfo(String code) throws SQLException, Exception {
        ServerList c = null;
        ResultSet rs = null;
        Logger.getLogger(ConnectionUtil.class.getName()).log(Level.INFO, "=========code====" + code);
        Connection conet = ConnectionUtil.getHQConnection();
        try {
            PreparedStatement stmt = conet.prepareStatement("select * from server_list where estatecode=LEFT(?, 2)");
            //PreparedStatement stmt = conet.prepareStatement("select * from server_list where estatecode=?");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getServerInfox(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static ServerList getServerInfox(ResultSet rs) throws SQLException {
        ServerList si = new ServerList();

        si.setFoldername(rs.getString("foldername"));
        si.setFolderldg(rs.getString("folderldg"));
        si.setSvrip(rs.getString("svrip"));
        si.setSvrname(rs.getString("svrname"));
        si.setUsername(rs.getString("username"));
        si.setPassword(rs.getString("password"));
        si.setEstatecode(rs.getString("estatecode"));

        return si;
    }

    public static ServerList getServerInfoN(String code) throws SQLException, Exception {
        ServerList c = null;
        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        try {
            //PreparedStatement stmt = conet.prepareStatement("select * from server_list where estatecode=LEFT(?, 2)");
            PreparedStatement stmt = conet.prepareStatement("select * from server_list where estatecode=?");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getServerInfox(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static List<ServerList> getAllServerlist() throws Exception {

        Connection con = ConnectionUtil.getHQConnection();
        Statement stmt = null;
        List<ServerList> CV;
        CV = new ArrayList();

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from server_list");

            while (rs.next()) {
                CV.add(getServerInfox(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static Connection createExternalConnection(ServerList svr) throws Exception {
        Connection newcon = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            //String dbURL = "jdbc:mysql://localhost:3306/ipamis_nenasi?zeroDateTimeBehavior=convertToNull";
            //String dbuser = "root";
            //String dbpass = "adminlcsb";
            //newcon = DriverManager.getConnection(dbURL, dbuser, dbpass);
            String dbURL = "jdbc:mysql://" + svr.getSvrip() + ":3306/" + svr.getFoldername() + "?zeroDateTimeBehavior=convertToNull";
            newcon = DriverManager.getConnection(dbURL, svr.getUsername(), svr.getPassword());
            ConnectionModel.setMillConnection(newcon);

        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newcon;

    }

    /*public static Connection getConnection() throws Exception{
        Connection oldcon = ConnectionModel.getMainConnection();
        Connection newcon = null;
        
        if(oldcon==null){
            try {
            

                Class.forName("com.mysql.jdbc.Driver");
                String dbURL = "jdbc:mysql://localhost:3306/fms_latest";
                String dbuser = "root";
                String dbpass = "admin";
                newcon=DriverManager.getConnection(dbURL,dbuser,dbpass);
                ConnectionModel.setMainConnection(newcon);

            
            }catch (SQLException ex) {
                    Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }else{
            newcon = oldcon;
        }
       
        
        return newcon;
        
    }*/
}
