/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.general.GeneralTerm;
import com.lcsb.makmurakua.model.development.SecModule;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author fadhilfahmi
 */
public class GetStatusDAO {

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));

        String q = "";

        if (!sc.getApprove().equals("no")) {

            q = "select " + sc.getCheck() + "," + sc.getApprove() + "," + sc.getPost() + " from " + sc.getMaintable() + " where " + sc.getReferidMaster() + " = ?";

            PreparedStatement stmt = log.getCon().prepareStatement(q);

            stmt.setString(1, refer);
            rs = stmt.executeQuery();
            if (rs.next()) {
                check = rs.getString(sc.getCheck());
                app = rs.getString(sc.getApprove());
                post = rs.getString(sc.getPost());
            }

            stmt.close();
            rs.close();

            if (!check.equals("")) {
                status = "Checked";
            }

            if (!app.equals("")) {
                status = "Approved";
            }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
            if (post.equals("cancel")) {
                status = "Canceled";
            }

        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));

        if (!sc.getCheck().equals("no")) {

            PreparedStatement stmt = log.getCon().prepareStatement("select " + sc.getCheck() + " from " + sc.getMaintable() + " where " + sc.getReferidMaster() + " = ?");

            stmt.setString(1, refer);
            rs = stmt.executeQuery();
            if (rs.next()) {
                ck = rs.getString(1);
            }

            if (!ck.equals("")) {
                cek = true;
            }

            stmt.close();
            rs.close();
        }

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));

        PreparedStatement stmt = log.getCon().prepareStatement("select " + sc.getApprove() + " from " + sc.getMaintable() + " where " + sc.getReferidMaster() + " = ?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

}
