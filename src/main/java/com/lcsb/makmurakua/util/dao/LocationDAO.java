/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.Location;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Search;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */

public class LocationDAO {
    
   public static List<Location> getLocation(LoginProfile log, String by, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Location> Com;
        Com = new ArrayList();
        
        String q = "";
        String table = "";
        
        if(by.equals("Management")){
            table = "ce_manage";
        }else if(by.equals("Company")){
            table = "ce_estate";
        }else if(by.equals("Department")){
            table = "department";
        }
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or name like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,name from "+table+" "+q+" order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Location getResult(ResultSet rs) throws SQLException {
        Location comp= new Location();
        comp.setLocationCode(rs.getString("code"));
        comp.setLocationDesc(rs.getString("name"));
        return comp;
    }
    
}
