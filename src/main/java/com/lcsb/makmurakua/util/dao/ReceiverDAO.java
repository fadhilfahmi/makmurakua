/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Receiver;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ReceiverDAO {
    
    public static List<Receiver> getReceiver(LoginProfile log, String by, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Receiver> Com;
        Com = new ArrayList();
        
        String q = "";
        String table = "";
        
        if(by.equals("Executive") || by.equals("Non - Executive")){
            table = "executive";
        }
        
        if(keyword!=null){
            q = "where id like '%"+keyword+"%' or name like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select id,name from "+table+" "+q+" order by id limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Receiver getResult(ResultSet rs) throws SQLException {
        Receiver comp= new Receiver();
        comp.setReceiverCode(rs.getString("id"));
        comp.setReceiverDesc(rs.getString("name"));
        return comp;
    }
    
}
