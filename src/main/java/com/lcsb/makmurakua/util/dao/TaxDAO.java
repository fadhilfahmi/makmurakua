/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.general.AccountingPeriod;
import com.lcsb.makmurakua.util.model.Location;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Tax;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class TaxDAO {
    
    public static List<Tax> getAllTax(LoginProfile log, String keyword) throws Exception {
        
        Statement stmt = null;
        List<Tax> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where taxcode like '%"+keyword+"%' or descp like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select taxcode,descp,taxrate,taxcoacode,taxcoadescp from cg_gst "+q+" order by taxcode");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static Tax getTax(LoginProfile log, String type) throws Exception{
        
        Tax tx = new Tax();
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cg_gst where taxcode=?");
        stmt.setString(1, type);
        rs = stmt.executeQuery();
        if (rs.next()) {
            tx = getResult(rs);
        }
        
        return tx;
    }
    
    private static Tax getResult(ResultSet rs) throws SQLException {
        Tax comp= new Tax();
        comp.setTaxcode(rs.getString("taxcode"));
        comp.setDescp(rs.getString("descp"));
        comp.setTaxrate(rs.getDouble("taxrate"));
        comp.setTaxcoacode(rs.getString("taxcoacode"));
        comp.setTaxcoadescp(rs.getString("taxcoadescp"));
        return comp;
    }
    
    public static double getTaxRate(LoginProfile log, double taxamt, String taxtype) throws Exception{
        
        double taxRate = 0.0;
        
        if(taxamt > 0){
            taxRate = 6.0;
        }else{
            taxRate = TaxDAO.getTax(log, taxtype).getTaxrate();
        }
        
        return taxRate;
        
    }
    
    public static boolean isTaxGstAfterZeroRate(Date date) throws ParseException{
        
        boolean i = false;
        
        Date gstLastDate = AccountingPeriod.convertStringtoDate("2018-05-31");
        
        if(date.compareTo(gstLastDate) > 0){
            
            i = true;
            
        }else if(date.compareTo(gstLastDate) < 0){
            
            i = false;
            
        }else if(date.compareTo(gstLastDate) == 0){
            
            i = false;
            
        }
        
        return i;
        
    }
    
    public static String getTaxCode(String type, String datex) throws ParseException{
        
        Date date = AccountingPeriod.convertStringtoDate(datex);
        
        String taxCode = "";
        
        if(type.equals("SR")){
            if(isTaxGstAfterZeroRate(date)){    
                taxCode = "SR-0";    
            }else{   
                taxCode = "SR";
            }
        }else if(type.equals("TX")){
            if(isTaxGstAfterZeroRate(date)){
                taxCode = "TX-0";
            }else{
                taxCode = "TX";
            }
        }
        
        return taxCode;
        
    }
    
}
