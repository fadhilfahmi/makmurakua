/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;


import com.lcsb.makmurakua.util.model.Cheque;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ChequeDAO {
    
    public static List<Cheque> getAllCheque(LoginProfile log, String keyword, String bankcode) throws Exception {
        
        Connection con = log.getCon();
        
        Statement stmt = null;
        List<Cheque> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = " and nocek like '%"+keyword+"%'";
        }
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cekbook_cek where bankcode = '"+bankcode+"' and flag='NO' and nocekbook = '"+ChequeDAO.noChequeActive(log, bankcode)+"' "+q+"  order by nocek");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static String noChequeActive(LoginProfile log, String bankcode) throws Exception{
        
        Statement stmt = null;
        
        String noCek = "";
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cekbook where bankcode = '"+bankcode+"' and active = 'Yes'");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                noCek = rs.getString("no");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return noCek;

        
    }
    
    private static Cheque getResult(ResultSet rs) throws SQLException {
        Cheque comp= new Cheque();
        comp.setChequeNo(rs.getString("nocek"));
        return comp;
    }
    
}
