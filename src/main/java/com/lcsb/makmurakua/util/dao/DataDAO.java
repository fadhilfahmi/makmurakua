/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.Data;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.MessageIO;
import com.lcsb.makmurakua.util.model.Parameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class DataDAO {

    public static void saveData(LoginProfile log, List<Data> datax, String table) throws Exception {

        String query = "INSERT INTO " + table;
        String column = "(";
        String value = "(";
        int i = 0;
        for (Data dt : datax) {
            dt.getColumnName();
            if (i == datax.size() - 1) {
                column += dt.getColumnName() + ")";
                value += "'" + dt.getColumnValue() + "')";
            } else {
                column += dt.getColumnName() + ",";
                value += "'" + dt.getColumnValue() + "',";
            }
            i++;
        }

        query += column + " VALUES " + value;
        Logger.getLogger(DataDAO.class.getName()).log(Level.INFO, query);
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();

        MessageIO.setMessage("<img src=\"image/icon/1388246235_OK.png\" width=\"18px\"> Successfully saved.");

    }

    public static void updateData(LoginProfile log, List<Data> datax, String table, String refno, String colRefer) throws Exception {

        Connection con = log.getCon();
        String query = "UPDATE " + table + " SET ";
        String column = "";
        int i = 0;
        for (Data dt : datax) {
            dt.getColumnName();
            if (i == datax.size() - 1) {
                column += dt.getColumnName() + " = '" + dt.getColumnValue() + "'";
            } else {
                column += dt.getColumnName() + " = '" + dt.getColumnValue() + "',";
            }
            i++;
        }

        query += column + " WHERE  " + colRefer + "='" + refno + "'";
        Logger.getLogger(DataDAO.class.getName()).log(Level.INFO, query);
        PreparedStatement ps = con.prepareStatement(query);
        ps.executeUpdate();

    }

    public static String retrieveData(LoginProfile log, String referenceno, String cols, String table, String colRefer) throws Exception {

        String output = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select " + cols + " from " + table + " where " + colRefer + "=?");
        stmt.setString(1, referenceno);

        Logger.getLogger(DataDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            output = rs.getString(1);
        }
        Logger.getLogger(DataDAO.class.getName()).log(Level.INFO, output);
        return output;
    }

    public static void deleteData(LoginProfile log, String referenceno, String table, String colRefer) throws Exception {

        String query = "delete from " + table + " where " + colRefer + " = ?";
        
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.setString(1, referenceno);
            ps.executeUpdate();
            Logger.getLogger(DataDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
        }
    }

    public static void deleteAllData(LoginProfile log, String table) throws Exception {

        String query = "delete from " + table + "";
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.executeUpdate();
            ps.close();
        }
    }

    public static void deleteUsingWildCard(LoginProfile log, String refer, String table, String colRefer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from " + table + " where " + colRefer + " LIKE ?";
        try (PreparedStatement ps = log.getCon().prepareStatement(deleteQuery_2)) {

            ps.setString(1, refer + "%");
            ps.executeUpdate();
            ps.close();
        }
        //updateAmount(refer);
    }
    
    public static void deleteDataDoubleReferral(LoginProfile log, String referenceno1, String referenceno2, String table, String colRefer1, String colRefer2) throws Exception {

        String query = "delete from " + table + " where " + colRefer1 + " = ? and " + colRefer2 + " = ?";
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.setString(1, referenceno1);
            ps.setString(2, referenceno2);
            ps.executeUpdate();
            ps.close();
        }
    }
    
    public static void deleteDataTripleReferral(LoginProfile log, String referenceno1, String referenceno2, String referenceno3, String table, String colRefer1, String colRefer2, String colRefer3) throws Exception {

        String query = "delete from " + table + " where " + colRefer1 + " = ? and " + colRefer2 + " = ? and " + colRefer3 + " = ?";
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.setString(1, referenceno1);
            ps.setString(2, referenceno2);
            ps.setString(3, referenceno3);
            ps.executeUpdate();
            ps.close();
        }
    }

}
