/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Product;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ersmiv
 */
public class ProductDAO {
    
    public static List<Product> getInfo(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Product> Com;
        Com = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from product_info order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Product getResult(ResultSet rs) throws SQLException {
        Product comp= new Product();
        comp.setCoa(rs.getString("coa"));
        comp.setCoadescp(rs.getString("coadescp"));
        comp.setCode(rs.getString("code"));
        comp.setDescp(rs.getString("descp"));
        comp.setEntercode(rs.getString("entercode"));
        comp.setMajor(rs.getString("major"));
        comp.setMeasure(rs.getString("measure"));
        comp.setSpesifik(rs.getString("spesifik"));
        return comp;
    }
    
    public static String productList(LoginProfile log, String selected) throws Exception {
        
        Statement stmt = null;
        String drop = "";
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from product_info where parameter = ''  order by  dorder");

            while (rs.next()) {
                if(rs.getString("value").equals(selected) && !selected.equals("")){
                    drop += "<option value = \""+rs.getString("value")+"\" selected>"+rs.getString("value")+"</option>";
                }else{
                    drop += "<option value = \""+rs.getString("value")+"\">"+rs.getString("value")+"</option>";
                }    
                
            }
            
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
       
        return drop;
    }
    
}
