/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Output;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class OutputDAO {
    
    public static List<Output> getOutput(LoginProfile log, String by, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Output> Com;
        Com = new ArrayList();
        
        String q = "";
        String q1 = "";
        
        if(by.equals("Product")){
            q1 = "where type = 'Product'";
        }else if(by.equals("Productivity")){
            q1 = "where type = 'Productivity'";
        }
        
        if(keyword!=null){
            q = " and outputcode like '%"+keyword+"%' or outputdesc like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select outputcode,outputdesc,measure from ac_job_output "+q1+" "+q+" order by outputcode limit 10");
Logger.getLogger(OutputDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Output getResult(ResultSet rs) throws SQLException {
        Output comp= new Output();
        comp.setMeasure(rs.getString("measure"));
        comp.setOutputcode(rs.getString("outputcode"));
        comp.setOutputdesc(rs.getString("outputdesc"));
        return comp;
    }
    
}
