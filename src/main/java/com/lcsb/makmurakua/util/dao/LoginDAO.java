/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.general.AccountingPeriod;
import com.lcsb.makmurakua.general.DateAndTime;
import com.lcsb.makmurakua.general.GeneralTerm;
import com.lcsb.makmurakua.model.administration.om.UserAccess;
import com.lcsb.makmurakua.model.administration.om.UserAccount;
import com.lcsb.makmurakua.sys.log.SysLogDAO;
import com.lcsb.makmurakua.util.model.ListTable;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class LoginDAO {

    public static Boolean checkUser(String username, String password, String estatecode, Connection con) throws Exception {

        ResultSet rs = null;
        String passdb = "";
        String passlogin = "";

        Boolean check = false;

        PreparedStatement stmt = con.prepareStatement("SELECT *,password('" + password + "') as pass FROM om_user_account WHERE user = ?");
        stmt.setString(1, username);

        rs = stmt.executeQuery();
        if (rs.next()) {

            passlogin = rs.getString("pass");
            passdb = rs.getString("password");
            Logger.getLogger(LoginDAO.class.getName()).log(Level.INFO, password + "-------password----" + passdb);

            if (passdb.equals(passlogin)) {
                check = true;
                //setGeneralUsage(username,estatecode,rs.getString("NoPekerja"));

            }
        }

        return check;

    }

    public static Boolean checkUserUsingGoogleAuth(String userID, String email, String company, Connection con) throws Exception {

        ResultSet rs = null;
        String passdb = "";
        String passlogin = "";

        Boolean check = false;

        PreparedStatement stmt = con.prepareStatement("SELECT *  FROM om_user_account WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {
            check = true;
        }

        return check;

    }

    public static Boolean checkUserUsingEmail(String email, String estatecode, Connection con) throws Exception {

        ResultSet rs = null;

        Boolean check = false;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM om_user_account WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {

            check = true;

        }

        return check;

    }

    public static String userID(String username, Connection con) throws Exception {

        ResultSet rs = null;

        String userID = "";
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM om_user_account WHERE user = ?");
        stmt.setString(1, username);

        rs = stmt.executeQuery();
        if (rs.next()) {
            userID = rs.getString("staff_id");

        }

        return userID;

    }
    
    public static String getUserIDbyEmail(String email, Connection con) throws Exception {

        ResultSet rs = null;

        String userID = "";
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM om_user_account WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {
            userID = rs.getString("staff_id");

        }

        return userID;

    }

    public static UserAccount userInfo(Connection con, String username) throws Exception {

        UserAccount ua = new UserAccount();
        ResultSet rs = null;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM om_user_account WHERE user = ?");
        stmt.setString(1, username);

        rs = stmt.executeQuery();
        if (rs.next()) {
            ua.setLevel(rs.getString("level"));
            ua.setLevelId(rs.getInt("level_id"));
            ua.setPassword(rs.getString("password"));
            ua.setStaffId(rs.getString("staff_id"));
            ua.setStaffName(rs.getString("staff_name"));
            ua.setUser(rs.getString("user"));

        }

        return ua;

    }
    
    public static UserAccount userInfoUsingGoogleAuth(Connection con, String email) throws Exception {

        UserAccount ua = new UserAccount();
        ResultSet rs = null;

        PreparedStatement stmt = con.prepareStatement("SELECT * FROM om_user_account WHERE email = ?");
        stmt.setString(1, email);

        rs = stmt.executeQuery();
        if (rs.next()) {
            ua.setLevel(rs.getString("level"));
            ua.setLevelId(rs.getInt("level_id"));
            ua.setPassword(rs.getString("password"));
            ua.setStaffId(rs.getString("staff_id"));
            ua.setStaffName(rs.getString("staff_name"));
            ua.setUser(rs.getString("user"));

        }

        return ua;

    }

    public static LoginProfile setLogin(String username, String estatecode, String sessionID, Connection con) throws Exception {

        ResultSet rs = null;

        LoginProfile log = new LoginProfile();

        String name = "";
        String staff_id = "";
        String position = "";

        PreparedStatement stmt = con.prepareStatement("select * from executive where id=?");
        stmt.setString(1, userID(username, con));

        rs = stmt.executeQuery();
        if (rs.next()) {
            name = rs.getString("name");
            staff_id = rs.getString("id");
            position = rs.getString("pposition");

            log.setFullname(name);
            log.setUserID(staff_id);

        }

        String estateDescp = GeneralTerm.getEstateName(con, estatecode);
        LoginProfile login = new LoginProfile();
        login.setUserName(username);
        login.setEstateCode(estatecode);
        login.setEstateDescp(estateDescp);
        login.setUserID(staff_id);
        login.setFullname(name);
        login.setPosition(position);
        login.setAccessLevel(LoginDAO.userInfo(con, username).getLevelId());
        login.setListUserAccess(LoginDAO.getUserAccess(con, LoginDAO.userInfo(con, username).getStaffId()));
        login.setCon(con);

        setLoginActivity(con, staff_id, username, sessionID);

        return login;
    }
    
    public static LoginProfile setLoginUsingGoogleAUth(String name, String email, String companycode, String sessionID, Connection con) throws Exception {

        ResultSet rs = null;

        LoginProfile log = new LoginProfile();

        String staff_id = "";
        String position = "";

        PreparedStatement stmt = con.prepareStatement("select * from executive where id=?");
        stmt.setString(1, getUserIDbyEmail(email, con));

        rs = stmt.executeQuery();
        if (rs.next()) {
            name = rs.getString("name");
            staff_id = rs.getString("id");
            position = rs.getString("pposition");

            log.setFullname(name);
            log.setUserID(staff_id);

        }
        
        UserAccount ua = LoginDAO.userInfoUsingGoogleAuth(con, email);

        String estateDescp = GeneralTerm.getEstateName(con, companycode);
        LoginProfile login = new LoginProfile();
        login.setUserName(ua.getUser());
        login.setEstateCode(companycode);
        login.setEstateDescp(estateDescp);
        login.setUserID(staff_id);
        login.setFullname(name);
        login.setPosition(position);
        login.setAccessLevel(ua.getLevelId());
        login.setListUserAccess(LoginDAO.getUserAccess(con, ua.getStaffId()));
        login.setCon(con);

        setLoginActivity(con, staff_id, ua.getUser(), sessionID);

        return login;
    }

    public static void setUserLog(LoginProfile log, String sess) throws Exception {

        Connection con = log.getCon();
        String q = ("insert into sec_usrlog(userid,workerid,workername,estatecode,estatename,logintime,loginstatus,sessionid) values (?,?,?,?,?,?,?,?)");
        PreparedStatement ps = con.prepareStatement(q);
        //ps.setString(1, LoginProfile.getUserName());
        //ps.setString(2, LoginProfile.getUserID());
        //ps.setString(3, LoginDAO.getStaffName());
        // ps.setString(4, LoginProfile.getEstateCode());
        // ps.setString(5, LoginProfile.getEstateDescp());
        //ps.setString(6, String.valueOf(GeneralTerm.getCurrentDateTime()));
        //ps.setString(7, "Success");
        // ps.setString(8, sess);
        // ps.executeUpdate();
        // ps.close();
    }

    public static LoginProfile getStaffDetail(LoginProfile log, String id) throws Exception {
        ResultSet rs = null;
        Connection con = log.getCon();

        LoginProfile logx = new LoginProfile();

        String name = "";
        String staff_id = "";
        PreparedStatement stmt = con.prepareStatement("select * from executive where id=?");
        stmt.setString(1, id);

        rs = stmt.executeQuery();
        if (rs.next()) {
            name = rs.getString("name");
            staff_id = rs.getString("id");

            logx.setFullname(name);
            logx.setUserID(staff_id);

        }
        return log;
    }

    public static List<UserAccess> getUserAccess(Connection con, String id) throws Exception {
        ResultSet rs = null;

        List<UserAccess> ua = new ArrayList();

        String name = "";
        String staff_id = "";
        PreparedStatement stmt = con.prepareStatement("select * from om_user_access where user_id=? order by function_id asc ");
        stmt.setString(1, id);

        rs = stmt.executeQuery();
        while (rs.next()) {
            UserAccess u = new UserAccess();
            u.setAccess(rs.getBoolean("access"));
            u.setFunctionId(rs.getString("function_id"));
            u.setUserId(rs.getString("user_id"));
            ua.add(u);

        }
        return ua;
    }

    private static void setLoginActivity(Connection con, String userID, String userName, String sessionID) throws Exception {

        boolean a = false;
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement("select * from sys_log_login where sessionid=?");
        stmt.setString(1, sessionID);

        rs = stmt.executeQuery();
        if (rs.next()) {
            a = true;
        }

        if (!a) {
            String query = "INSERT INTO sys_log_login(userid,username,sessionid,date,time) values (?,?,?,?,?)";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, userID);
                ps.setString(2, userName);
                ps.setString(3, sessionID);
                ps.setString(4, AccountingPeriod.getCurrentTimeStamp());
                ps.setString(5, DateAndTime.addTime(AccountingPeriod.getCurrentTime()));
                ps.executeUpdate();
            }
        }

    }

    public static void updateLogoutTime(LoginProfile log, String sessionID) throws Exception {

        String query = "update sys_log_login set dateout = ?, timeout = ? WHERE sessionid = '" + sessionID + "'";

        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.setString(1, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(2, DateAndTime.addTime(AccountingPeriod.getCurrentTime()));
            ps.executeUpdate();
        }

        SysLogDAO.logoutUpdate(log);

    }

}
