/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.Company;
import com.lcsb.makmurakua.util.model.ConnectionModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class CompanyDAO {
    
    public static List<Company> getAllCompany() throws Exception {
        
        Connection con = ConnectionUtil.getTemporaryConnection();
        Logger.getLogger(CompanyDAO.class.getName()).log(Level.INFO, String.valueOf(con));
        Statement stmt = null;
        List<Company> Com;
        Com = new ArrayList();
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from account_info order by companycode");

            while (rs.next()) {
                Com.add(getCompany(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Com;
    }
    
    public static Company getCompany(String code) throws SQLException, Exception {
        Company c = null;
        ResultSet rs = null;
        Connection conet = ConnectionUtil.getTemporaryConnection();
        PreparedStatement stmt = conet.prepareStatement("select * from account_info where companycode=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCompany(rs);
        }

        
        return c;
    }
    
    private static Company getCompany(ResultSet rs) throws SQLException {
        Company comp= new Company();
        comp.setCode(rs.getString("companycode"));
        comp.setName(rs.getString("companyname"));
        return comp;
    }
    
}
