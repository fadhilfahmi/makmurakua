/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.Contractor;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ContractorDAO {
    
    public static List<Contractor> getContractor(LoginProfile log, String keyword) throws Exception {
        
        Statement stmt = null;
        List<Contractor> Com;
        Com = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,companyname,address,city,state,postcode,ownername,owneric,gstid from contractor_info order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Contractor getResult(ResultSet rs) throws SQLException {
        Contractor comp= new Contractor();
        comp.setAddress(rs.getString("address"));
        comp.setCity(rs.getString("city"));
        comp.setCode(rs.getString("code"));
        comp.setCompanyname(rs.getString("companyname"));
        comp.setGstid(rs.getString("gstid"));
        comp.setOwneric(rs.getString("owneric"));
        comp.setOwnername(rs.getString("ownername"));
        comp.setPostcode(rs.getString("postcode"));
        comp.setState(rs.getString("state"));
        return comp;
    }
    
}
