/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TypeDAO {
    
    public static List<Type> getType(LoginProfile log, String by, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Type> Com;
        Com = new ArrayList();
        Type ty = (Type) TypeDAO.getColumn(by);
        
        String q = "";
        
        if(keyword!=null){
            q = "where "+ty.getCode()+" like '%"+keyword+"%' or "+ty.getDescp()+" like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from "+ty.getTable()+" "+q+" order by "+ty.getCode()+" limit 10");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs,by));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Type getResult(ResultSet rs,String entity) throws SQLException {
        
        Type typ= new Type();
        Type ty = (Type) TypeDAO.getColumn(entity);
        
        typ.setCode(rs.getString(ty.getCode()));
        typ.setDescp(rs.getString(ty.getDescp()));
        
        if(!ty.getGstid().equals("0")){
            typ.setGstid(rs.getString(ty.getGstid()));
        }
        if(!ty.getAddress().equals("0")){
            typ.setAddress(rs.getString(ty.getAddress()));
        }
        if(!ty.getPostcode().equals("0")){
            typ.setPostcode(rs.getString(ty.getPostcode()));
        }
        if(!ty.getCity().equals("0")){
            typ.setCity(rs.getString(ty.getCity()));
        }
        if(!ty.getState().equals("0")){
            typ.setState(rs.getString(ty.getState()));
        }
        
        
        return typ;
    }
    
    private static Type getColumn(String entity){
        
        Type ty= new Type();
        
        switch (entity) {
            case "Buyer":
                ty.setTable("buyer_info");
                ty.setCode("code");
                ty.setDescp("companyname");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            case "Bank":
                ty.setTable("info_bank_general");
                ty.setCode("code");
                ty.setDescp("name");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            case "Contractor":
                ty.setTable("contractor_info");
                ty.setCode("code");
                ty.setDescp("companyname");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            case "Employee":
                ty.setTable("co_staff");
                ty.setCode("staffid");
                ty.setDescp("name");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("0");
                break;
            case "Estate":
                ty.setTable("estateinfo");
                ty.setCode("estatecode");
                ty.setDescp("estatedescp");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            case "Supplier":
                ty.setTable("supplier_info");
                ty.setCode("code");
                ty.setDescp("companyname");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            case "Accrual":
                ty.setTable("conf_accrual_info");
                ty.setCode("code");
                ty.setDescp("companyname");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("0");
                break;
            case "Board":
                ty.setTable("board_info");
                ty.setCode("code");
                ty.setDescp("name");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("0");
                break;
            case "Headquarters":
                ty.setTable("department");
                ty.setCode("code");
                ty.setDescp("name");
                ty.setAddress("0");
                ty.setPostcode("0");
                ty.setCity("0");
                ty.setState("0");
                ty.setGstid("0");
                break;
            case "Tenant":
                ty.setTable("tenant_info");
                ty.setCode("code");
                ty.setDescp("descp");
                ty.setAddress("address");
                ty.setPostcode("postcode");
                ty.setCity("city");
                ty.setState("state");
                ty.setGstid("gstid");
                break;
            default:
                break;
        }
        
        return ty;
        
    }
    
    
}
