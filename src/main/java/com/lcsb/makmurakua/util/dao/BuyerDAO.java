/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.dao;


import com.lcsb.makmurakua.util.model.Buyer;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ersmiv
 */
public class BuyerDAO {

    public static List<Buyer> getBuyer(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<Buyer> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where code like '%" + keyword + "%' or companyname like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,companyname,address,city,state,postcode,gstid,coa,coadescp from buyer_info " + q + " order by code limit 10");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {

                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static List<Buyer> getBuyerMill(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<Buyer> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where code like '%" + keyword + "%' or descp like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,descp from sl_contractor_info " + q + " order by code limit 15");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                Buyer by = new Buyer();
                by.setBuyerCode(rs.getString("code"));
                by.setBuyerName(rs.getString("descp"));
                by.setHqCodeExist(checkCodeMillFromHQ(log,rs.getString("code")));

                Com.add(by);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    private static Buyer getResult(ResultSet rs) throws SQLException {
        Buyer comp = new Buyer();
        comp.setBuyerAddress(rs.getString("address"));
        comp.setBuyerCity(rs.getString("city"));
        comp.setBuyerCode(rs.getString("code"));
        comp.setBuyerName(rs.getString("companyname"));
        comp.setBuyerPostcode(rs.getString("postcode"));
        comp.setBuyerState(rs.getString("state"));
        comp.setGstID(rs.getString("gstid"));
        comp.setCoa(rs.getString("coa"));
        comp.setCoadescp(rs.getString("coadescp"));

        return comp;
    }

    public static List<Buyer> getBuyerMatchFromMill(LoginProfile log, String bname) throws Exception {

        Statement stmt = null;
        List<Buyer> Com;
        Com = new ArrayList();

        String q = "";
        ArrayList<String> totalCheckCode = new ArrayList<String>();
        StringTokenizer t = new StringTokenizer(bname);
        String word = "";
        while (t.hasMoreTokens()) {
            word = t.nextToken();

            if (!word.contains("SDN") && !word.contains("BHD")) {

                if (bname != null) {
                    q = "where descp like '%" + word + "%'";
                    //Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, ">>>>>>> " + q);
                }

                try {
                    stmt = log.getCon().createStatement();
                    ResultSet rs = stmt.executeQuery("select code,descp from sl_contractor_info " + q + " order by code");
                    Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
                    while (rs.next()) {

                        if (totalCheckCode.size() == 0) {
                            //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "firstinsert=={0}", a.getCoacode());
                            totalCheckCode.add(rs.getString("code"));
                            Buyer by = new Buyer();
                            by.setBuyerCode(rs.getString("code"));
                            by.setBuyerName(rs.getString("descp"));

                            Com.add(by);
                        }

                        int x = 0;
                        for (int i = 0; i < totalCheckCode.size(); i++) {
                            if (totalCheckCode.get(i).equals(rs.getString("code"))) {
                                x++;

                            }
                        }

                        //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "2ndrow=={0}", x);
                        if (x == 0) {
                            totalCheckCode.add(rs.getString("code"));
                            Buyer by = new Buyer();
                            by.setBuyerCode(rs.getString("code"));
                            by.setBuyerName(rs.getString("descp"));

                            Com.add(by);
                        }

                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < totalCheckCode.size(); i++) {
            Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, "********* " + totalCheckCode.get(i));
        }

        return Com;
    }

    private static boolean checkCodeMillFromHQ(LoginProfile log, String code) throws SQLException {
        
        boolean x = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from buyer_info where codemill=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            x = true;
        }
        
        return x;
    }

}
