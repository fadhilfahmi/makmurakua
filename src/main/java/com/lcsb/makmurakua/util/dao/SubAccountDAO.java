/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.SubAccount;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class SubAccountDAO {
    
    public static List<SubAccount> getSubAcc(LoginProfile log, SubAccount su, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<SubAccount> Com;
        Com = new ArrayList();
        
        String q = "";
        String table = "";
        
        
        if(keyword!=null){
            q = "where "+su.getSubCode()+" like '%"+keyword+"%' or "+su.getSubDecs()+" like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select "+su.getSubCode()+","+su.getSubDecs()+" from "+su.getSubTable()+" "+q+" order by "+su.getSubCode()+" limit 10");
Logger.getLogger(SubAccountDAO.class.getName()).log(Level.INFO, "select "+su.getSubCode()+","+su.getSubDecs()+" from "+su.getSubTable()+" "+q+" order by "+su.getSubCode()+" limit 10");
            while (rs.next()) {
                
                Com.add(getResult(rs,su));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static SubAccount getResult(ResultSet rs, SubAccount su) throws SQLException {
        SubAccount comp= new SubAccount();
        comp.setTheCode(rs.getString(su.getSubCode()));
        comp.setTheDesc(rs.getString(su.getSubDecs()));
        return comp;
    }
    
    public static SubAccount setInfo(String type) throws Exception{
        
        SubAccount sub = new SubAccount();
        
        if(type.equals("Worker")){
            
            sub.setSubCode("id");
            sub.setSubDecs("name");
            sub.setSubTable("executive");
            
        }else if(type.equals("Non-Executive")){
            
            sub.setSubCode("id");
            sub.setSubDecs("name");
            sub.setSubTable("executive");
            
        }else if(type.equals("Executive")){
            
            sub.setSubCode("id");
            sub.setSubDecs("name");
            sub.setSubTable("executive");
            
        }else if(type.equals("Sundry Contract")){
            
            sub.setSubCode("suppliercode");
            sub.setSubDecs("companyname");
            sub.setSubTable("snmcontractor_info");
            
        }else if(type.equals("Stage")){
            
            sub.setSubCode("code");
            sub.setSubDecs("descp");
            sub.setSubTable("entrestage");
            
        }else if(type.equals("Member")){
            
            sub.setSubCode("noanggota");
            sub.setSubDecs("nama");
            sub.setSubTable("coop_member");
            
        }else if(type.equals("Supplier")){
            
            sub.setSubCode("code");
            sub.setSubDecs("companyname");
            sub.setSubTable("supplier_info");
            
        }else if(type.equals("Buyer")){
            
            sub.setSubCode("code");
            sub.setSubDecs("companyname");
            sub.setSubTable("buyer_info");
            
        }else if(type.equals("Fund")){
            
            sub.setSubCode("code");
            sub.setSubDecs("name");
            sub.setSubTable("ce_manage_bank");
            
        }else if(type.equals("Fund")){
            
            sub.setSubCode("code");
            sub.setSubDecs("name");
            sub.setSubTable("ce_manage_bank");
            
        }else if(type.equals("Investment")){
            
            sub.setSubCode("code");
            sub.setSubDecs("name");
            sub.setSubTable("est_investment");
            
        }else if(type.equals("Accrual")){
            
            sub.setSubCode("code");
            sub.setSubDecs("companyname");
            sub.setSubTable("conf_accrual_info");
            
        }else if(type.equals("Contractor")){
            
            sub.setSubCode("code");
            sub.setSubDecs("companyname");
            sub.setSubTable("contractor_info");
            
        }else if(type.equals("Estate")){
            
            sub.setSubCode("estatecode");
            sub.setSubDecs("estatedescp");
            sub.setSubTable("estateinfo");
            
        }else if(type.equals("Machinery")){
            
            sub.setSubCode("code");
            sub.setSubDecs("descp");
            sub.setSubTable("machinery");
            
        }else if(type.equals("Workshop")){
            
            sub.setSubCode("code");
            sub.setSubDecs("descp");
            sub.setSubTable("workshop");
            
        }else if(type.equals("Staff")){
            
            sub.setSubCode("staffid");
            sub.setSubDecs("name");
            sub.setSubTable("co_staff");
            
        }else if(type.equals("Share Portfolio")){
            
            sub.setSubCode("code");
            sub.setSubDecs("descp");
            sub.setSubTable("cf_portfolio");
            
        }else if(type.equals("Lot Member")){
            
            sub.setSubCode("mem_Code");
            sub.setSubDecs("mem_Name");
            sub.setSubTable("lot_memberinfo");
            
        }else if(type.equals("Agent")){
            
            sub.setSubCode("code");
            sub.setSubDecs("descp");
            sub.setSubTable("co_agent");
            
        }else if(type.equals("Lot")){
            
            sub.setSubCode("lot_Code");
            sub.setSubDecs("lot_Descp");
            sub.setSubTable("lot_info");
            
        }else if(type.equals("Board")){
            
            sub.setSubCode("code");
            sub.setSubDecs("name");
            sub.setSubTable("board_info");
            
        }else if(type.equals("Department")){
            
            sub.setSubCode("code");
            sub.setSubDecs("name");
            sub.setSubTable("department");
            
        }
        
        return sub;
    }
    
}
