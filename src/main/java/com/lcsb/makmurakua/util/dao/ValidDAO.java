/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Dell
 */
public class ValidDAO {
    
    public static int checkCOACode(LoginProfile log, String keyword) throws Exception{
        
        int cnt = 0;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from chartofacccount where code=?");
            stmt.setString(1, keyword);
            rs = stmt.executeQuery();
            if (rs.next()) {
                cnt = 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
       
        return cnt;
        
    }
    
}
