/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.util.dao;

import com.lcsb.makmurakua.util.model.Estate;
import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lcsb.makmurakua.util.model.ServerList;
import com.lcsb.makmurakua.util.model.Tax;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class EstateDAO {

    public static List<Estate> getAllEstate(LoginProfile log, String keyword) throws Exception {


        Statement stmt = null;
        List<Estate> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where estatecode like '%" + keyword + "%' or estatedescp like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from estateinfo " + q + " order by estatecode");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {

                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static Estate getResult(ResultSet rs) throws SQLException {
        Estate comp = new Estate();
        comp.setAddress(rs.getString("address"));
        comp.setCity(rs.getString("city"));
        comp.setDatestart(rs.getDate("datestart"));
        comp.setDistrict(rs.getString("district"));
        comp.setEmail(rs.getString("email"));
        comp.setEstatecode(rs.getString("estatecode"));
        comp.setEstatedescp(rs.getString("estatedescp"));
        comp.setFax(rs.getString("fax"));
        comp.setHectar(rs.getString("hectar"));
        comp.setHqcurrent(rs.getString("hqcurrent"));
        comp.setHqcurrentdescp(rs.getString("hqcurrentdescp"));
        comp.setHqsuspence(rs.getString("hqsuspence"));
        comp.setHqsuspencedescp(rs.getString("hqsuspencedescp"));
        comp.setMajor(rs.getString("major"));
        comp.setManagecode(rs.getString("managecode"));
        comp.setManagedescp(rs.getString("managedescp"));
        comp.setMapref(rs.getString("mapref"));
        comp.setPhone(rs.getString("phone"));
        comp.setPostcode(rs.getString("postcode"));
        comp.setRegion(rs.getString("region"));
        comp.setState(rs.getString("state"));
        comp.setGstid(rs.getString("gstid"));
        comp.setLogopath(rs.getString("logopath"));
        comp.setRegisterno(rs.getString("registerno"));
        comp.setAccountno(rs.getString("accountno"));
        comp.setBank(rs.getString("bank"));
        comp.setSstid(rs.getString("sstid"));

        return comp;
    }

    public static Estate getEstateInfo(LoginProfile log, String code, String columnRefer) throws Exception, SQLException {

        Estate es = new Estate();
        
        Logger.getLogger(EstateDAO.class.getName()).log(Level.INFO, "select * from estateinfo where " + columnRefer + " = '"+code+"' ");

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from estateinfo where " + columnRefer + " = ? ");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                es = getResult(rs);
            }
            
            stmt.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return es;

    }

    public static List<ServerList> getAllServer(LoginProfile log, String keyword) throws Exception {


        Statement stmt = null;
        List<ServerList> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where estatecode like '%" + keyword + "%' or descp like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from server_list " + q + " order by estatecode");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {

                Com.add(getServer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static ServerList getServer(ResultSet rs) throws SQLException {
        ServerList sl = new ServerList();
        sl.setDescp(rs.getString("descp"));
        sl.setEstatecode(rs.getString("estatecode"));
        sl.setFlag(rs.getString("flag"));
        sl.setFolderldg(rs.getString("folderldg"));
        sl.setFoldername(rs.getString("foldername"));
        sl.setPassword(rs.getString("password"));
        sl.setShortName(rs.getString("short_name"));
        sl.setStatus(rs.getString("status"));
        sl.setSvrip(rs.getString("svrip"));
        sl.setSvrname(rs.getString("svrname"));
        sl.setUsername(rs.getString("username"));

        return sl;
    }

}
