/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class Search {
    
    private String code,decsp;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDecsp() {
        return decsp;
    }

    public void setDecsp(String decsp) {
        this.decsp = decsp;
    }
    
    
}
