/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.model;

/**
 *
 * @author fadhilfahmi
 */
public class ListTable {
    
    private String columnName;
    private String titleName;
    private boolean boolView;
    private int colWidth;

    public int getColWidth() {
        return colWidth;
    }

    public void setColWidth(int colWidth) {
        this.colWidth = colWidth;
    }
    
    public boolean isBoolView() {
        return boolView;
    }

    public void setBoolView(boolean boolView) {
        this.boolView = boolView;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }
    
}
