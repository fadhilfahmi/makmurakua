/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ersmiv
 */
@Entity
@Table(name = "product_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p"),
    @NamedQuery(name = "Product.findByCode", query = "SELECT p FROM Product p WHERE p.code = :code"),
    @NamedQuery(name = "Product.findByDescp", query = "SELECT p FROM Product p WHERE p.descp = :descp"),
    @NamedQuery(name = "Product.findByCoa", query = "SELECT p FROM Product p WHERE p.coa = :coa"),
    @NamedQuery(name = "Product.findByCoadescp", query = "SELECT p FROM Product p WHERE p.coadescp = :coadescp"),
    @NamedQuery(name = "Product.findBySpesifik", query = "SELECT p FROM Product p WHERE p.spesifik = :spesifik"),
    @NamedQuery(name = "Product.findByEntercode", query = "SELECT p FROM Product p WHERE p.entercode = :entercode"),
    @NamedQuery(name = "Product.findByMajor", query = "SELECT p FROM Product p WHERE p.major = :major"),
    @NamedQuery(name = "Product.findByMeasure", query = "SELECT p FROM Product p WHERE p.measure = :measure")})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "spesifik")
    private String spesifik;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "entercode")
    private String entercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "major")
    private String major;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "measure")
    private String measure;

    public Product() {
    }

    public Product(String code) {
        this.code = code;
    }

    public Product(String code, String descp, String coa, String coadescp, String spesifik, String entercode, String major, String measure) {
        this.code = code;
        this.descp = descp;
        this.coa = coa;
        this.coadescp = coadescp;
        this.spesifik = spesifik;
        this.entercode = entercode;
        this.major = major;
        this.measure = measure;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getSpesifik() {
        return spesifik;
    }

    public void setSpesifik(String spesifik) {
        this.spesifik = spesifik;
    }

    public String getEntercode() {
        return entercode;
    }

    public void setEntercode(String entercode) {
        this.entercode = entercode;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.util.model.Product[ code=" + code + " ]";
    }
    
}
