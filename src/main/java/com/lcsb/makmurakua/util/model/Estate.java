/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "estateinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estate.findAll", query = "SELECT e FROM Estate e"),
    @NamedQuery(name = "Estate.findByManagecode", query = "SELECT e FROM Estate e WHERE e.managecode = :managecode"),
    @NamedQuery(name = "Estate.findByManagedescp", query = "SELECT e FROM Estate e WHERE e.managedescp = :managedescp"),
    @NamedQuery(name = "Estate.findByEstatecode", query = "SELECT e FROM Estate e WHERE e.estatecode = :estatecode"),
    @NamedQuery(name = "Estate.findByEstatedescp", query = "SELECT e FROM Estate e WHERE e.estatedescp = :estatedescp"),
    @NamedQuery(name = "Estate.findByDatestart", query = "SELECT e FROM Estate e WHERE e.datestart = :datestart"),
    @NamedQuery(name = "Estate.findByHqsuspence", query = "SELECT e FROM Estate e WHERE e.hqsuspence = :hqsuspence"),
    @NamedQuery(name = "Estate.findByHqcurrent", query = "SELECT e FROM Estate e WHERE e.hqcurrent = :hqcurrent"),
    @NamedQuery(name = "Estate.findByHqsuspencedescp", query = "SELECT e FROM Estate e WHERE e.hqsuspencedescp = :hqsuspencedescp"),
    @NamedQuery(name = "Estate.findByHqcurrentdescp", query = "SELECT e FROM Estate e WHERE e.hqcurrentdescp = :hqcurrentdescp"),
    @NamedQuery(name = "Estate.findByPostcode", query = "SELECT e FROM Estate e WHERE e.postcode = :postcode"),
    @NamedQuery(name = "Estate.findByCity", query = "SELECT e FROM Estate e WHERE e.city = :city"),
    @NamedQuery(name = "Estate.findByDistrict", query = "SELECT e FROM Estate e WHERE e.district = :district"),
    @NamedQuery(name = "Estate.findByState", query = "SELECT e FROM Estate e WHERE e.state = :state"),
    @NamedQuery(name = "Estate.findByPhone", query = "SELECT e FROM Estate e WHERE e.phone = :phone"),
    @NamedQuery(name = "Estate.findByFax", query = "SELECT e FROM Estate e WHERE e.fax = :fax"),
    @NamedQuery(name = "Estate.findByEmail", query = "SELECT e FROM Estate e WHERE e.email = :email"),
    @NamedQuery(name = "Estate.findByMapref", query = "SELECT e FROM Estate e WHERE e.mapref = :mapref"),
    @NamedQuery(name = "Estate.findByHectar", query = "SELECT e FROM Estate e WHERE e.hectar = :hectar"),
    @NamedQuery(name = "Estate.findByMajor", query = "SELECT e FROM Estate e WHERE e.major = :major"),
    @NamedQuery(name = "Estate.findByRegion", query = "SELECT e FROM Estate e WHERE e.region = :region")})
public class Estate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "managecode")
    private String managecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "managedescp")
    private String managedescp;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatedescp")
    private String estatedescp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datestart")
    @Temporal(TemporalType.DATE)
    private Date datestart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqsuspence")
    private String hqsuspence;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqcurrent")
    private String hqcurrent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqsuspencedescp")
    private String hqsuspencedescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqcurrentdescp")
    private String hqcurrentdescp;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "district")
    private String district;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mapref")
    private String mapref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hectar")
    private String hectar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "major")
    private String major;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "region")
    private String region;
    private String gstid;
    private String logopath;
    private String registerno;
    private String accountno;
    private String bank;
    private String sstid;

    public Estate() {
    }

    public Estate(String estatecode) {
        this.estatecode = estatecode;
    }

    public Estate(String estatecode, String managecode, String managedescp, String estatedescp, Date datestart, String hqsuspence, String hqcurrent, String hqsuspencedescp, String hqcurrentdescp, String address, String postcode, String city, String district, String state, String phone, String fax, String email, String mapref, String hectar, String major, String region, String gstid, String logopath, String registerno, String accountno,String bank, String sstid) {
        this.estatecode = estatecode;
        this.managecode = managecode;
        this.managedescp = managedescp;
        this.estatedescp = estatedescp;
        this.datestart = datestart;
        this.hqsuspence = hqsuspence;
        this.hqcurrent = hqcurrent;
        this.hqsuspencedescp = hqsuspencedescp;
        this.hqcurrentdescp = hqcurrentdescp;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.district = district;
        this.state = state;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.mapref = mapref;
        this.hectar = hectar;
        this.major = major;
        this.region = region;
        this.gstid = gstid;
        this.logopath = logopath;
        this.registerno = registerno;
        this.accountno = accountno;
        this.bank = bank;
        this.sstid = sstid;
    }

    public String getSstid() {
        return sstid;
    }

    public void setSstid(String sstid) {
        this.sstid = sstid;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getRegisterno() {
        return registerno;
    }

    public void setRegisterno(String registerno) {
        this.registerno = registerno;
    }

    public String getLogopath() {
        return logopath;
    }

    public void setLogopath(String logopath) {
        this.logopath = logopath;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getManagecode() {
        return managecode;
    }

    public void setManagecode(String managecode) {
        this.managecode = managecode;
    }

    public String getManagedescp() {
        return managedescp;
    }

    public void setManagedescp(String managedescp) {
        this.managedescp = managedescp;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatedescp() {
        return estatedescp;
    }

    public void setEstatedescp(String estatedescp) {
        this.estatedescp = estatedescp;
    }

    public Date getDatestart() {
        return datestart;
    }

    public void setDatestart(Date datestart) {
        this.datestart = datestart;
    }

    public String getHqsuspence() {
        return hqsuspence;
    }

    public void setHqsuspence(String hqsuspence) {
        this.hqsuspence = hqsuspence;
    }

    public String getHqcurrent() {
        return hqcurrent;
    }

    public void setHqcurrent(String hqcurrent) {
        this.hqcurrent = hqcurrent;
    }

    public String getHqsuspencedescp() {
        return hqsuspencedescp;
    }

    public void setHqsuspencedescp(String hqsuspencedescp) {
        this.hqsuspencedescp = hqsuspencedescp;
    }

    public String getHqcurrentdescp() {
        return hqcurrentdescp;
    }

    public void setHqcurrentdescp(String hqcurrentdescp) {
        this.hqcurrentdescp = hqcurrentdescp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMapref() {
        return mapref;
    }

    public void setMapref(String mapref) {
        this.mapref = mapref;
    }

    public String getHectar() {
        return hectar;
    }

    public void setHectar(String hectar) {
        this.hectar = hectar;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estatecode != null ? estatecode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estate)) {
            return false;
        }
        Estate other = (Estate) object;
        if ((this.estatecode == null && other.estatecode != null) || (this.estatecode != null && !this.estatecode.equals(other.estatecode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.util.model.Estate[ estatecode=" + estatecode + " ]";
    }
    
}
