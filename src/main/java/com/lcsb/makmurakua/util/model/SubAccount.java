/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class SubAccount {
    
    private String subCode,subDecs,subTable,theCode, theDesc;

    public String getTheCode() {
        return theCode;
    }

    public void setTheCode(String theCode) {
        this.theCode = theCode;
    }

    public String getTheDesc() {
        return theDesc;
    }

    public void setTheDesc(String theDesc) {
        this.theDesc = theDesc;
    }

    public String getSubTable() {
        return subTable;
    }

    public void setSubTable(String subTable) {
        this.subTable = subTable;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubDecs() {
        return subDecs;
    }

    public void setSubDecs(String subDecs) {
        this.subDecs = subDecs;
    }
    
    
    
}
