/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.util.model;

import com.lcsb.makmurakua.model.administration.om.UserAccess;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Dell
 */
public class LoginProfile {
    
    private  String userName;
    private  String estateCode;
    private  String userID;
    private  String estateDescp;
    private  String fullname;
    private  String currentModule;
    private  String currentProcess;
    private  String position;
    private  int accessLevel;
    private List<UserAccess> listUserAccess;
    private Connection con;
    private String path;
    private String imgURL;

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public List<UserAccess> getListUserAccess() {
        return listUserAccess;
    }

    public void setListUserAccess(List<UserAccess> listUserAccess) {
        this.listUserAccess = listUserAccess;
    }

    
    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEstateCode() {
        return estateCode;
    }

    public void setEstateCode(String estateCode) {
        this.estateCode = estateCode;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEstateDescp() {
        return estateDescp;
    }

    public void setEstateDescp(String estateDescp) {
        this.estateDescp = estateDescp;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCurrentModule() {
        return currentModule;
    }

    public void setCurrentModule(String currentModule) {
        this.currentModule = currentModule;
    }

    public String getCurrentProcess() {
        return currentProcess;
    }

    public void setCurrentProcess(String currentProcess) {
        this.currentProcess = currentProcess;
    }

    
    
    
    
}
