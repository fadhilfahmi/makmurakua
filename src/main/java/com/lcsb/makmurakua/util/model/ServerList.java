/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.util.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "server_list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServerList.findAll", query = "SELECT s FROM ServerList s"),
    @NamedQuery(name = "ServerList.findBySvrip", query = "SELECT s FROM ServerList s WHERE s.svrip = :svrip"),
    @NamedQuery(name = "ServerList.findByEstatecode", query = "SELECT s FROM ServerList s WHERE s.estatecode = :estatecode"),
    @NamedQuery(name = "ServerList.findByStatus", query = "SELECT s FROM ServerList s WHERE s.status = :status"),
    @NamedQuery(name = "ServerList.findByFoldername", query = "SELECT s FROM ServerList s WHERE s.foldername = :foldername"),
    @NamedQuery(name = "ServerList.findByFolderldg", query = "SELECT s FROM ServerList s WHERE s.folderldg = :folderldg"),
    @NamedQuery(name = "ServerList.findByPassword", query = "SELECT s FROM ServerList s WHERE s.password = :password"),
    @NamedQuery(name = "ServerList.findByUsername", query = "SELECT s FROM ServerList s WHERE s.username = :username"),
    @NamedQuery(name = "ServerList.findByDescp", query = "SELECT s FROM ServerList s WHERE s.descp = :descp"),
    @NamedQuery(name = "ServerList.findByFlag", query = "SELECT s FROM ServerList s WHERE s.flag = :flag"),
    @NamedQuery(name = "ServerList.findBySvrname", query = "SELECT s FROM ServerList s WHERE s.svrname = :svrname"),
    @NamedQuery(name = "ServerList.findByShortName", query = "SELECT s FROM ServerList s WHERE s.shortName = :shortName")})
public class ServerList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "svrip")
    private String svrip;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "foldername")
    private String foldername;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "folderldg")
    private String folderldg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "svrname")
    private String svrname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "short_name")
    private String shortName;

    public ServerList() {
    }

    public ServerList(String estatecode) {
        this.estatecode = estatecode;
    }

    public ServerList(String estatecode, String svrip, String status, String foldername, String folderldg, String password, String username, String descp, String flag, String svrname, String shortName) {
        this.estatecode = estatecode;
        this.svrip = svrip;
        this.status = status;
        this.foldername = foldername;
        this.folderldg = folderldg;
        this.password = password;
        this.username = username;
        this.descp = descp;
        this.flag = flag;
        this.svrname = svrname;
        this.shortName = shortName;
    }

    public String getSvrip() {
        return svrip;
    }

    public void setSvrip(String svrip) {
        this.svrip = svrip;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFoldername() {
        return foldername;
    }

    public void setFoldername(String foldername) {
        this.foldername = foldername;
    }

    public String getFolderldg() {
        return folderldg;
    }

    public void setFolderldg(String folderldg) {
        this.folderldg = folderldg;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSvrname() {
        return svrname;
    }

    public void setSvrname(String svrname) {
        this.svrname = svrname;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estatecode != null ? estatecode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServerList)) {
            return false;
        }
        ServerList other = (ServerList) object;
        if ((this.estatecode == null && other.estatecode != null) || (this.estatecode != null && !this.estatecode.equals(other.estatecode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.util.model.ServerList[ estatecode=" + estatecode + " ]";
    }
    
}
