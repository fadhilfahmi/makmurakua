/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class Module {
    
    private String ModuleID,ModuleDesc,Hyperlink,FinalLevel,Disable,MainTable,SubTable,Abb,referID_Master, referID_Sub, pathcontrol, approve, posttype, post,check;
    private boolean gl;

    public String getCheck() {
        return check;
    }

    public boolean isGl() {
        return gl;
    }

    public void setGl(boolean gl) {
        this.gl = gl;
    }

    public void setCheck(String check) {
        this.check = check;
    }
    private boolean access;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getPathcontrol() {
        return pathcontrol;
    }

    public void setPathcontrol(String pathcontrol) {
        this.pathcontrol = pathcontrol;
    }

    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }


    public String getMainTable() {
        return MainTable;
    }

    public void setMainTable(String MainTable) {
        this.MainTable = MainTable;
    }

    public String getSubTable() {
        return SubTable;
    }

    public void setSubTable(String SubTable) {
        this.SubTable = SubTable;
    }

    public String getAbb() {
        return Abb;
    }

    public void setAbb(String Abb) {
        this.Abb = Abb;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }

    public String getModuleDesc() {
        return ModuleDesc;
    }

    public void setModuleDesc(String ModuleDesc) {
        this.ModuleDesc = ModuleDesc;
    }

    public String getHyperlink() {
        return Hyperlink;
    }

    public void setHyperlink(String Hyperlink) {
        this.Hyperlink = Hyperlink;
    }

    public String getFinalLevel() {
        return FinalLevel;
    }

    public void setFinalLevel(String FinalLevel) {
        this.FinalLevel = FinalLevel;
    }

    public String getDisable() {
        return Disable;
    }

    public void setDisable(String Disable) {
        this.Disable = Disable;
    }

    public String getReferID_Master() {
        return referID_Master;
    }

    public void setReferID_Master(String referID_Master) {
        this.referID_Master = referID_Master;
    }

    public String getReferID_Sub() {
        return referID_Sub;
    }

    public void setReferID_Sub(String referID_Sub) {
        this.referID_Sub = referID_Sub;
    }

    

    
    
}
