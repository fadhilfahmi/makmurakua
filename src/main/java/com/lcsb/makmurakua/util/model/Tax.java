/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class Tax {
    
    private String taxcode;
    private String descp;
    private Double taxrate;
    private String taxcoacode;
    private String taxcoadescp;

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }
    
    
    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public Double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(Double taxrate) {
        this.taxrate = taxrate;
    }
    
    
    
}
