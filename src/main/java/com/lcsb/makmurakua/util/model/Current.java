/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class Current {
    private  String subject;
    private  String dataLevel;
    private  String masterReferenceNo;
    private  String subReferenceNo;
    private  String flag;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDataLevel() {
        return dataLevel;
    }

    public void setDataLevel(String dataLevel) {
        this.dataLevel = dataLevel;
    }

    public String getMasterReferenceNo() {
        return masterReferenceNo;
    }

    public void setMasterReferenceNo(String masterReferenceNo) {
        this.masterReferenceNo = masterReferenceNo;
    }

    public String getSubReferenceNo() {
        return subReferenceNo;
    }

    public void setSubReferenceNo(String subReferenceNo) {
        this.subReferenceNo = subReferenceNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    
    
    
    
    
    
}
