/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class ModuleItem {
    //"module_id","module_name","position","title","refer_id","column_name","type_field","attribute_1","attribute_2","default_value","param_name","field_size","listview","id_u","field_check","regex_check","date", "listform","autorefer","autovoucher","get_info","basecolumn","get_info_1stcolumn","get_info_2ndcolumn","get_info_table","get_info_table_column1", "get_info_table_column2"
    private String Module_ID;
    private String Module_Name;
    private int Position;
    private String Title;
    private String Refer_ID;
    private String Column_Name;
    private String Type_Field;
    private String Attribute_1;
    private String Attribute_2;
    private String Default_Value;
    private String Param_Name;
    private String Field_Size;
    private String List_View;
    private int Id_u;
    private String Field_Check;
    private String Regex_Check;
    private String Date;
    private String List_Form;
    private String Auto_Refer;
    private String Auto_Voucher;
    private String Auto_4digitno;
    private String Get_info;
    private String Base_Column;
    private String Get_info_1stColumn;
    private String Get_info_2ncColumn;
    private String Get_info_3rdColumn;
    private String Get_info_Table;
    private String Get_info_Table_Column1;
    private String Get_info_Table_Column2;
    private String Get_info_Table_Column3;
    private String convertRM;
    private String convertRM_base;
    private String convertRM_to;
    private String auto_referno;
    private String carry_referno;
    private String column_from;
    private String savedata;
    private String jsfunction;

    public String getJsfunction() {
        return jsfunction;
    }

    public void setJsfunction(String jsfunction) {
        this.jsfunction = jsfunction;
    }
    
    public String getAuto_4digitno() {
        return Auto_4digitno;
    }

    public void setAuto_4digitno(String Auto_4digitno) {
        this.Auto_4digitno = Auto_4digitno;
    }

    public String getGet_info_3rdColumn() {
        return Get_info_3rdColumn;
    }

    public void setGet_info_3rdColumn(String Get_info_3rdColumn) {
        this.Get_info_3rdColumn = Get_info_3rdColumn;
    }

    public String getGet_info_Table_Column3() {
        return Get_info_Table_Column3;
    }

    public void setGet_info_Table_Column3(String Get_info_Table_Column3) {
        this.Get_info_Table_Column3 = Get_info_Table_Column3;
    }
    

    public String getModule_ID() {
        return Module_ID;
    }

    public void setModule_ID(String Module_ID) {
        this.Module_ID = Module_ID;
    }

    public String getModule_Name() {
        return Module_Name;
    }

    public void setModule_Name(String Module_Name) {
        this.Module_Name = Module_Name;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int Position) {
        this.Position = Position;
    }
    
    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }
    public String getRefer_ID() {
        return Refer_ID;
    }

    public void setRefer_ID(String Refer_ID) {
        this.Refer_ID = Refer_ID;
    }

    public String getColumn_Name() {
        return Column_Name;
    }

    public void setColumn_Name(String Column_Name) {
        this.Column_Name = Column_Name;
    }

    public String getType_Field() {
        return Type_Field;
    }

    public void setType_Field(String Type_Field) {
        this.Type_Field = Type_Field;
    }

    public String getAttribute_1() {
        return Attribute_1;
    }

    public void setAttribute_1(String Attribute_1) {
        this.Attribute_1 = Attribute_1;
    }

    public String getAttribute_2() {
        return Attribute_2;
    }

    public void setAttribute_2(String Attribute_2) {
        this.Attribute_2 = Attribute_2;
    }

    public String getDefault_Value() {
        return Default_Value;
    }

    public void setDefault_Value(String Default_Value) {
        this.Default_Value = Default_Value;
    }

    public String getParam_Name() {
        return Param_Name;
    }

    public void setParam_Name(String Param_Name) {
        this.Param_Name = Param_Name;
    }

    public String getField_Size() {
        return Field_Size;
    }

    public void setField_Size(String Field_Size) {
        this.Field_Size = Field_Size;
    }

    public String getList_View() {
        return List_View;
    }

    public void setList_View(String List_View) {
        this.List_View = List_View;
    }

    public int getId_u() {
        return Id_u;
    }

    public void setId_u(int Id_u) {
        this.Id_u = Id_u;
    }

    public String getField_Check() {
        return Field_Check;
    }

    public void setField_Check(String Field_Check) {
        this.Field_Check = Field_Check;
    }

    public String getRegex_Check() {
        return Regex_Check;
    }

    public void setRegex_Check(String Regex_Check) {
        this.Regex_Check = Regex_Check;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getList_Form() {
        return List_Form;
    }

    public void setList_Form(String List_Form) {
        this.List_Form = List_Form;
    }

    public String getAuto_Refer() {
        return Auto_Refer;
    }

    public void setAuto_Refer(String Auto_Refer) {
        this.Auto_Refer = Auto_Refer;
    }

    public String getAuto_Voucher() {
        return Auto_Voucher;
    }

    public void setAuto_Voucher(String Auto_Voucher) {
        this.Auto_Voucher = Auto_Voucher;
    }

    public String getGet_info() {
        return Get_info;
    }

    public void setGet_info(String Get_info) {
        this.Get_info = Get_info;
    }

    public String getBase_Column() {
        return Base_Column;
    }

    public void setBase_Column(String Base_Column) {
        this.Base_Column = Base_Column;
    }

    public String getGet_info_1stColumn() {
        return Get_info_1stColumn;
    }

    public void setGet_info_1stColumn(String Get_info_1stColumn) {
        this.Get_info_1stColumn = Get_info_1stColumn;
    }

    public String getGet_info_2ncColumn() {
        return Get_info_2ncColumn;
    }

    public void setGet_info_2ncColumn(String Get_info_2ncColumn) {
        this.Get_info_2ncColumn = Get_info_2ncColumn;
    }

    public String getGet_info_Table() {
        return Get_info_Table;
    }

    public void setGet_info_Table(String Get_info_Table) {
        this.Get_info_Table = Get_info_Table;
    }

    public String getGet_info_Table_Column1() {
        return Get_info_Table_Column1;
    }

    public void setGet_info_Table_Column1(String Get_info_Table_Column1) {
        this.Get_info_Table_Column1 = Get_info_Table_Column1;
    }

    public String getGet_info_Table_Column2() {
        return Get_info_Table_Column2;
    }

    public void setGet_info_Table_Column2(String Get_info_Table_Column2) {
        this.Get_info_Table_Column2 = Get_info_Table_Column2;
    }

    public String getConvertRM() {
        return convertRM;
    }

    public void setConvertRM(String convertRM) {
        this.convertRM = convertRM;
    }

    public String getConvertRM_base() {
        return convertRM_base;
    }

    public void setConvertRM_base(String convertRM_base) {
        this.convertRM_base = convertRM_base;
    }

    public String getConvertRM_to() {
        return convertRM_to;
    }

    public void setConvertRM_to(String convertRM_to) {
        this.convertRM_to = convertRM_to;
    }

    public String getAuto_referno() {
        return auto_referno;
    }

    public void setAuto_referno(String auto_referno) {
        this.auto_referno = auto_referno;
    }

    public String getCarry_referno() {
        return carry_referno;
    }

    public void setCarry_referno(String carry_referno) {
        this.carry_referno = carry_referno;
    }

    public String getColumn_from() {
        return column_from;
    }

    public void setColumn_from(String column_from) {
        this.column_from = column_from;
    }

    public String getSavedata() {
        return savedata;
    }

    public void setSavedata(String savedata) {
        this.savedata = savedata;
    }
    
    
}
