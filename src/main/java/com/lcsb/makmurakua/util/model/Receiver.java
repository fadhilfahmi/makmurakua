/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.makmurakua.util.model;

/**
 *
 * @author Dell
 */
public class Receiver {
    
    private String receiverCode,receiverDesc;

    public String getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    public String getReceiverDesc() {
        return receiverDesc;
    }

    public void setReceiverDesc(String receiverDesc) {
        this.receiverDesc = receiverDesc;
    }
    
    
}
