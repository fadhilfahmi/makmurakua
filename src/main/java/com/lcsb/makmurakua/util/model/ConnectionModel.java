/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.makmurakua.util.model;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Dell
 */
public class ConnectionModel {
    private static Connection mainConnection;
    private static Connection tempConnection;
    private static Connection HQConnection;
    private static Connection millConnection;
    
    private String serverIP;
    private String dbName;
    private String serverName;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public static Connection getMillConnection() {
        return millConnection;
    }

    public static void setMillConnection(Connection millConnection) {
        ConnectionModel.millConnection = millConnection;
    }

    public static Connection getHQConnection() {
        return HQConnection;
    }

    public static void setHQConnection(Connection HQConnection) {
        ConnectionModel.HQConnection = HQConnection;
    }

    public static Connection getTempConnection() {
        return tempConnection;
    }

    public static void setTempConnection(Connection tempConnection) {
        ConnectionModel.tempConnection = tempConnection;
    }

    public static Connection getMainConnection() {
        return mainConnection;
    }

    public static void setMainConnection(Connection mainConnection) {
        ConnectionModel.mainConnection = mainConnection;
    }
    
    public static void closeConnection(Connection mainConnection) throws SQLException{
        mainConnection.close();
    }
    
   
}
