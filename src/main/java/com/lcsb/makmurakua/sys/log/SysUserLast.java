/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.sys.log;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sys_user_lastupdate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysUserLast.findAll", query = "SELECT s FROM SysUserLast s")
    , @NamedQuery(name = "SysUserLast.findById", query = "SELECT s FROM SysUserLast s WHERE s.id = :id")
    , @NamedQuery(name = "SysUserLast.findByUserid", query = "SELECT s FROM SysUserLast s WHERE s.userid = :userid")
    , @NamedQuery(name = "SysUserLast.findByProcess", query = "SELECT s FROM SysUserLast s WHERE s.process = :process")
    , @NamedQuery(name = "SysUserLast.findByTimeupdate", query = "SELECT s FROM SysUserLast s WHERE s.timeupdate = :timeupdate")
    , @NamedQuery(name = "SysUserLast.findByDateupdate", query = "SELECT s FROM SysUserLast s WHERE s.dateupdate = :dateupdate")
    , @NamedQuery(name = "SysUserLast.findByModuleid", query = "SELECT s FROM SysUserLast s WHERE s.moduleid = :moduleid")})
public class SysUserLast implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "userid")
    private String userid;
    @Size(max = 50)
    @Column(name = "process")
    private String process;
    @Size(max = 50)
    @Column(name = "timeupdate")
    private String timeupdate;
    @Size(max = 50)
    @Column(name = "dateupdate")
    private String dateupdate;
    @Size(max = 10)
    @Column(name = "moduleid")
    private String moduleid;

    public SysUserLast() {
    }

    public SysUserLast(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getTimeupdate() {
        return timeupdate;
    }

    public void setTimeupdate(String timeupdate) {
        this.timeupdate = timeupdate;
    }

    public String getDateupdate() {
        return dateupdate;
    }

    public void setDateupdate(String dateupdate) {
        this.dateupdate = dateupdate;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysUserLast)) {
            return false;
        }
        SysUserLast other = (SysUserLast) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.sys.log.SysUserLast[ id=" + id + " ]";
    }
    
}
