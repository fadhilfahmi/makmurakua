/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.sys.log;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sys_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysLog.findAll", query = "SELECT s FROM SysLog s")
    , @NamedQuery(name = "SysLog.findById", query = "SELECT s FROM SysLog s WHERE s.id = :id")
    , @NamedQuery(name = "SysLog.findByModuleid", query = "SELECT s FROM SysLog s WHERE s.moduleid = :moduleid")
    , @NamedQuery(name = "SysLog.findByUserid", query = "SELECT s FROM SysLog s WHERE s.userid = :userid")
    , @NamedQuery(name = "SysLog.findByIp", query = "SELECT s FROM SysLog s WHERE s.ip = :ip")
    , @NamedQuery(name = "SysLog.findByHostname", query = "SELECT s FROM SysLog s WHERE s.hostname = :hostname")
    , @NamedQuery(name = "SysLog.findByReferno", query = "SELECT s FROM SysLog s WHERE s.referno = :referno")
    , @NamedQuery(name = "SysLog.findByProcess", query = "SELECT s FROM SysLog s WHERE s.process = :process")
    , @NamedQuery(name = "SysLog.findByDate", query = "SELECT s FROM SysLog s WHERE s.date = :date")
    , @NamedQuery(name = "SysLog.findByTime", query = "SELECT s FROM SysLog s WHERE s.time = :time")
    , @NamedQuery(name = "SysLog.findByRemark", query = "SELECT s FROM SysLog s WHERE s.remark = :remark")})
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "moduleid")
    private String moduleid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "userid")
    private String userid;
    @Size(max = 50)
    @Column(name = "ip")
    private String ip;
    @Size(max = 100)
    @Column(name = "hostname")
    private String hostname;
    @Size(max = 20)
    @Column(name = "referno")
    private String referno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "process")
    private String process;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "time")
    private String time;
    @Size(max = 200)
    @Column(name = "remark")
    private String remark;
    private boolean log;

    public SysLog() {
    }

    public SysLog(Integer id) {
        this.id = id;
    }

    public SysLog(Integer id, String moduleid, String userid, String process, String date, String time, boolean log) {
        this.id = id;
        this.moduleid = moduleid;
        this.userid = userid;
        this.process = process;
        this.date = date;
        this.time = time;
        this.log = log;
    }

    public boolean isLog() {
        return log;
    }

    public void setLog(boolean log) {
        this.log = log;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getReferno() {
        return referno;
    }

    public void setReferno(String referno) {
        this.referno = referno;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysLog)) {
            return false;
        }
        SysLog other = (SysLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.makmurakua.sys.log.SysLog[ id=" + id + " ]";
    }
    
}
