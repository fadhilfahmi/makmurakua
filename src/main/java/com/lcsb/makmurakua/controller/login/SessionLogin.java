/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller.login;

import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.model.ConnectionModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "SessionLogin", urlPatterns = {"/SessionLogin"})
public class SessionLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SessionLogin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SessionLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            // load the driver
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            throw new UnavailableException(
                    "Login init() ClassNotFoundException: " + e.getMessage());
        } catch (IllegalAccessException e) {
            throw new UnavailableException(
                    "Login init() IllegalAccessException: " + e.getMessage());
        } catch (InstantiationException e) {
            throw new UnavailableException(
                    "Login init() InstantiationException: " + e.getMessage());
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");
        Connection connection = null;
        if (sessionConnection != null) {
            //connection = sessionConnection.getConnection();
            session.removeAttribute("sessionconnection");
            session.removeAttribute("sessionuser");
            session.removeAttribute("sessionpassword");
            session.removeAttribute("sessioncompany");

        }

        Logger.getLogger(ConnectionUtil.class.getName()).log(Level.INFO, "==========" + connection);
        if (connection == null) {
//            String userName = request.getParameter("user");
//            String password = request.getParameter("password");
//            String estate = request.getParameter("estate");
            //if (userName == null || password == null || estate.equals("none")) {
            String userID = request.getParameter("userID");
            String email = request.getParameter("email");
            String name = request.getParameter("name");
            String imageurl = request.getParameter("imageurl");

            Connection initialCon = null;
            initialCon = ConnectionModel.getTempConnection();
            try {
                connection = ConnectionUtil.getConnection(initialCon);

            } catch (Exception ex) {
                Logger.getLogger(SessionLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (connection != null) {
                // store the connection
                sessionConnection = new SessionConnection();
                sessionConnection.setConnection(connection);
                session.setAttribute("sessionconnection", sessionConnection);
//                    session.setAttribute("sessionuser", request.getParameter("user"));
//                    session.setAttribute("sessionpassword", request.getParameter("password"));
//                    session.setAttribute("sessioncompany", request.getParameter("estate"));
//                    session.setAttribute("sessionemail", request.getParameter("email"));

                session.setAttribute("sessionuserID", request.getParameter("userID"));
                session.setAttribute("sessionemail", request.getParameter("email"));
                session.setAttribute("sessionname", request.getParameter("name"));
                session.setAttribute("sessionimageurl", request.getParameter("imageurl"));
                session.setAttribute("sessioncompany", request.getParameter("company"));

                Logger.getLogger(SessionLogin.class.getName()).log(Level.INFO, "-------email----" + request.getParameter("email"));

                response.sendRedirect("Login");
                return;
            }

        } else {
            String logout = request.getParameter("logout");
            if (logout == null) {

                response.sendRedirect("Login?estate=" + request.getParameter("estate") + "&user=" + request.getParameter("user") + "&password=" + request.getParameter("password"));
                return;
                // test the connection
//                Statement statement = null;
//                ResultSet resultSet = null;
//                String userName = null;
//                try {
//                    statement = connection.createStatement();
//                    resultSet = statement
//                            .executeQuery("select initcap(user) from sys.dual");
//                    if (resultSet.next()) {
//                        userName = resultSet.getString(1);
//                    }
//                } catch (SQLException e) {
//                    out.println("Login doGet() SQLException: " + e.getMessage()
//                            + "<p>");
//                } finally {
//                    if (resultSet != null) {
//                        try {
//                            resultSet.close();
//                        } catch (SQLException ignore) {
//                        }
//                    }
//                    if (statement != null) {
//                        try {
//                            statement.close();
//                        } catch (SQLException ignore) {
//                        }
//                    }
//                }
//                out.println("Hello " + userName + "!<p>");
//                out.println("Your session ID is " + session.getId() + "<p>");
//                out
//                        .println("It was created on "
//                                + new java.util.Date(session.getCreationTime())
//                                + "<p>");
//                out.println("It was last accessed on "
//                        + new java.util.Date(session.getLastAccessedTime())
//                        + "<p>");
//                out.println("<form method=\"get\" action=\"SessionLogin\">");
//                out.println("<input type=\"submit\" name=\"logout\" "
//                        + "value=\"Logout\">");
//                out.println("</form>");
            } else {
                // close the connection and remove it from the session
                try {
                    connection.close();
                } catch (SQLException ignore) {
                }
                session.removeAttribute("sessionconnection");
                out.println("You have been logged out.");
            }
        }
        out.println("</body>");
        out.println("</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }
}

class SessionConnection implements HttpSessionBindingListener {

    Connection connection;

    public SessionConnection() {
        connection = null;
    }

    public SessionConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void valueBound(HttpSessionBindingEvent event) {
        if (connection != null) {
            System.out.println("Binding a valid connection");
        } else {
            System.out.println("Binding a null connection");
        }
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
        if (connection != null) {
            System.out
                    .println("Closing the bound connection as the session expires");
            try {
                connection.close();
            } catch (SQLException ignore) {
            }
        }
    }

}
