/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller.login;

import com.lcsb.makmurakua.general.AccountingPeriod;
import com.lcsb.makmurakua.general.GeneralTerm;
//import com.lcsb.makmurakua.sys.headview.SysHeadviewDAO;
import com.lcsb.makmurakua.sys.log.SysLogDAO;
import com.lcsb.makmurakua.sys.log.SysUserLast;
import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.dao.LoginDAO;
import com.lcsb.makmurakua.util.model.ConnectionModel;
import com.lcsb.makmurakua.util.model.LogError;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell
 */
@WebServlet("/Login")
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            try {
                HttpSession session = request.getSession();
                SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");

                Connection connection = null;
                if (sessionConnection != null) {
                    connection = sessionConnection.getConnection();
                    Logger.getLogger(Login.class.getName()).log(Level.INFO, "-------connection----" + connection);
                }
                String urlsend = "";

                String userID = (String) session.getAttribute("sessionuserID");
                String email = (String) session.getAttribute("sessionemail");
                String name = (String) session.getAttribute("sessionname");
                String imgURL = (String) session.getAttribute("sessionimageurl");
                
//                session.setAttribute("sessionuserID", request.getParameter("userID"));
//                    session.setAttribute("sessionemail", request.getParameter("email"));
//                    session.setAttribute("sessionname", request.getParameter("name"));
//                    session.setAttribute("sessionimageurl", request.getParameter("imageurl"));
//                    session.setAttribute("sessioncompany", request.getParameter("company"));

              
                    ConnectionUtil.closeTemporaryConnection(ConnectionModel.getTempConnection());//attempt to close temporary connection created in login page

                    String thisSession = session.getId();

                   // Boolean check = LoginDAO.checkUser(username, password, company, connection);
                    Boolean check = LoginDAO.checkUserUsingGoogleAuth(userID, email, "4311", connection);
                    Logger.getLogger(Login.class.getName()).log(Level.INFO, "-------password----" + check);
                    //String userID = LoginDAO.userID(username, connection);

                    if (check == true) {

                        LoginProfile login = (LoginProfile) LoginDAO.setLoginUsingGoogleAUth(name,email, "4311", session.getId(), connection);
                        
                        login.setPath("/fms_v1");//local
                        //login.setPath("/");//server
                        login.setImgURL(imgURL);

                        session.setAttribute("login_detail", login);
                        session.removeAttribute("login_error");

                        SysUserLast ul = new SysUserLast();

                        ul.setUserid(login.getUserID());
                        ul.setModuleid("Login");
                        ul.setProcess("loginsucceed");
                        ul.setDateupdate(AccountingPeriod.getCurrentTimeStamp());
                        ul.setTimeupdate(AccountingPeriod.getCurrentTime());
                        SysLogDAO.saveLastTimeUpdate(login, ul);
                        
                        if(request.getParameter("action")==null){
                            
                        }else{
                            session.setAttribute("todo", "yes");
                        }
                        
                        
                            urlsend = "/main.jsp";
                       

                        

                    } else {

                        LogError err = new LogError();
                        err.setHaveError(true);
                        err.setMessageid("0001");
                        err.setMessage("Incorrect username or password!");
                        err.setType("Danger");
                        session.setAttribute("login_error", err);
                        urlsend = "/index.jsp";

                    }
               
                RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
                dispatcher.forward(request, response);

            } catch (Exception ex) {
                String urlerror = "/errorPage.jsp";
                RequestDispatcher dispatcher_error = getServletContext().getRequestDispatcher(urlerror);
                dispatcher_error.forward(request, response);
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
