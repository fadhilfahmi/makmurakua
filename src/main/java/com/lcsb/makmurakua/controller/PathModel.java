/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller;

import com.lcsb.makmurakua.general.ErrorIO;

/**
 *
 * @author fadhilfahmi
 */
public class PathModel {
    
    private String urlSend;
    private ErrorIO io;

    public String getUrlSend() {
        return urlSend;
    }

    public void setUrlSend(String urlSend) {
        this.urlSend = urlSend;
    }

    public ErrorIO getIo() {
        return io;
    }

    public void setIo(ErrorIO io) {
        this.io = io;
    }
    
    
    
}
