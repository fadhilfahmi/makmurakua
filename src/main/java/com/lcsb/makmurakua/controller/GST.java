/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller;

import com.lcsb.makmurakua.util.model.LoginProfile;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "GST", urlPatterns = {"/GST"})
public class GST extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DocumentException, SQLException {
        response.setContentType("application/pdf");

        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String uri = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort())
                + request.getContextPath()
                + "/tx_gst03_view.PDF";
        System.out.println(uri);
        URL u = new URL(uri);
        URLConnection uc = u.openConnection();

        PdfReader pdfReader = new PdfReader(uc.getInputStream());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfStamper stamper = new PdfStamper(pdfReader, baos);
        //AcroFields acroFields = pdfReader.getAcroFields();
        AcroFields acroFields = stamper.getAcroFields();
        Map<String, AcroFields.Item> fields = acroFields.getFields();
        Set<Entry<String, AcroFields.Item>> entrySet = fields.entrySet();

        String[] fieldname = {"GSTNo", "NameBusiness", "StartDate", "EndDate", "PaymentDate", "totalVal", "totalOut", "GSTNo1", "totalAcquisition", "totalInputTax", "amountPayable", "amountClaimable", "chkBox1", "chkBox2", "totalSupp", "totalExport", "totalExempt", "totalGranted", "totalGoods", "totalSuspend", "totalCapital", "totalRelief", "totalRecovered", "code1", "code2", "code3", "code4", "code5", "output1", "output2", "output3", "output4", "output5", "output6", "output7", "prcnt1", "prcnt2", "prcnt3", "prcnt4", "prcnt5", "prcnt6", "NameApp", "NewIC", "OldIC", "PassNo", "Nationality", "Date", "Signature"};

        String[] valueitem = {"gstid", "locname", "taxstart", "taxend", "taxdue", "c1", "c2", "gstid", "c3", "c4", "pay", "claim", "", "", "c6", "c7", "c8", "c9", "c10", "", "c11", "c12", "c13", "i14", "i16", "i18", "i20", "i22", "c15", "c17", "c19", "c21", "c23", "c24", "", "prcnt1", "prcnt2", "prcnt3", "prcnt4", "prcnt5", "prcnt6", "appname", "NewIC", "OldIC", "PassNo", "nationality", "appdate", "", ""};

        for (int x = 0; x < fieldname.length; x++) {

            if (!valueitem[x].equalsIgnoreCase("")) {

                //System.out.println("select "+valueitem[x]+" from tx_gst03 where refer='"+request.getParameter("refer")+"'");
                Statement stmt = log.getCon().createStatement();
                ResultSet st = stmt.executeQuery("select " + valueitem[x] + " from tx_gst03 where refer='" + request.getParameter("refer") + "'");
                //System.out.println("select "+valueitem[x]+" from tx_gst03 where refer='"+request.getParameter("refer")+"';");
                if (st.next()) {
                    //System.out.println(st.getString(valueitem[x]).concat(" ").concat(String.valueOf(st.getMetaData().getColumnType(1))));
                    //if(st.getMetaData().getColumnClassName(column))
                    //st.getMetaData().get
                    if (st.getMetaData().getColumnType(1) == 91) {
                        try {
                            valueitem[x] = sdf.format(st.getDate(valueitem[x]));
                        } catch (Exception ex) {
                            valueitem[x] = "";
                        }
                    } else {
                        valueitem[x] = st.getString(valueitem[x]);
                    }
                }
            }

            acroFields.setField(fieldname[x], valueitem[x]);
            //acroFields.setField(fieldname[x], valueitem[x]+"/"+fieldname[x]);	

        } //end for

        stamper.close();
        pdfReader.close();
        OutputStream os = response.getOutputStream();
        baos.writeTo(os);
        os.flush();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(GST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GST.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DocumentException ex) {
            Logger.getLogger(GST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GST.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
