/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller;

import com.lcsb.makmurakua.general.DataTable;
import com.lcsb.makmurakua.general.ErrorDAO;
import com.lcsb.makmurakua.general.ErrorIO;
import com.lcsb.makmurakua.general.SearchDAO;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_GeneralPath", urlPatterns = {"/Path_GeneralPath"})
public class Path_GeneralPath extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "datatable":
                    DataTable dt = new DataTable();
                    
                    dt.setModuleTypeID(request.getParameter("moduletypeID"));
                    dt.setViewBy(request.getParameter("view"));
                    dt.setYear(((request.getParameter("year")== null) ? "" : request.getParameter("year")));
                    dt.setPeriod(((request.getParameter("period")== null) ? "" : request.getParameter("period")));
                    session.setAttribute("dtparam", dt);
                    urlsend = "/datatable.jsp?moduleid="+request.getParameter("moduletypeID");
                    break;
                case "viewgl":
                    urlsend = "/gl_viewledger.jsp?refer=" + request.getParameter("refer");
                    break;
                case "quickaccess":

                    String type = request.getParameter("type");
                    String url = "";

                    switch (type) {
                        case "coa":
                            url = "cf_coa_list.jsp";
                            break;
                        case "freport":
                            url = "gl_Financial_Report.jsp";
                            break;
                        case "snv":
                            url = "ar_inv_list.jsp";
                            break;
                        case "or":
                            url = "cb_official_list.jsp";
                            break;
                        case "pv":
                            url = "cb_pv_list.jsp";
                            break;
                        default:
                            break;
                    }

                    urlsend = "/"+url;
                    break;
                case "search":
                    
                    urlsend = "/"+SearchDAO.searchInput(log, request.getParameter("keyword"))+".jsp?refer=" + request.getParameter("keyword");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("200");//nullpointer
                    ErrorIO.setType("data");
                    ErrorIO.setComcode(log.getEstateCode());
                    ErrorIO.setLoggedbyid(log.getUserID());
                    ErrorIO.setLoggedbyname(log.getFullname());
                    ErrorIO.setModuleid(moduleid);
                    ErrorIO.setProcess(process);
                    ErrorDAO.logError(log);
                    urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(Path_GeneralPath.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_GeneralPath.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_GeneralPath.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
