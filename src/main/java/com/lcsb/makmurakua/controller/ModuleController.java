/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.makmurakua.controller;

import com.lcsb.makmurakua.controller.login.Login;
import com.lcsb.makmurakua.general.ErrorIO;
import com.lcsb.makmurakua.util.dao.ConnectionUtil;
import com.lcsb.makmurakua.util.dao.ModuleDAO;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dell
 */
@WebServlet(name = "ModuleController", urlPatterns = {"/ModuleController"})
public class ModuleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException,  Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ModuleC</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ModuleC at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {       
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();

                HttpSession session = request.getSession();
                LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
                
                String moduleid = request.getParameter("moduleid");
                String urlsend = "";
                String errorMsg = "Module not found";
                LoginProfile login = new LoginProfile();
                login.setCurrentModule(moduleid);
               
                if(moduleid!=null){

                    urlsend = "/"+ModuleDAO.getModule(log, moduleid).getHyperlink()+"?moduleid="+moduleid;
                    login.setCurrentProcess("");

                }else{

                    //urlsend = "/errorPage.jsp?error="+errorMsg;
                    ErrorIO.setError(String.valueOf("Error : Invalid module or module not found."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";

                }
                request.setAttribute("login",login);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(urlsend);
                dispatcher.forward(request, response);
                
            } catch (Exception ex) {
                Logger.getLogger(ModuleController.class.getName()).log(Level.SEVERE, null, ex);
                ErrorIO.setError(String.valueOf(ex));
                ErrorIO.setCode("100");//nullpointer
                ErrorIO.setType("data");
                String urlerror = "/error.jsp";
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(urlerror);
                dispatcher.forward(request, response);
            }
       
    } 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
                
               
           
            
           
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
