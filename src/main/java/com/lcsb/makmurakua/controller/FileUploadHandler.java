/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller;

import com.lcsb.makmurakua.general.AccountingPeriod;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "FileUploadHandler", urlPatterns = {"/FileUploadHandler"})
public class FileUploadHandler extends HttpServlet {
    
    //private final String UPLOAD_DIRECTORY = "/Users/fadhilfahmi/Google Drive/fms_v1/target/fms_v1-1.0/uploadedFiles";
    private final String UPLOAD_DIRECTORY = "/usr/local/tomcat/webapps/fms_v1/uploadedFiles";
    //String path = getServletContext().getRealPath( "/uploadedFiles" );
    
    //private final String UPLOAD_DIRECTORY = getServletContext().getRealPath( "/" )+"uploadedFiles";
    
    

   

    @Override

    protected void doPost(HttpServletRequest request, HttpServletResponse response)

            throws ServletException, IOException {

       //out.println( "context = " + getServletContext().getRealPath( "/" ) );
        //process only if its multipart content

        if(ServletFileUpload.isMultipartContent(request)){

            try {

                List<FileItem> multiparts = new ServletFileUpload(
                new DiskFileItemFactory()).parseRequest(request);

                for(FileItem item : multiparts){

                    if(!item.isFormField()){

                        //String name = new File(item.getName()).getName();
                        String name = new File(item.getName()).getName();
                        
                        String ext = FilenameUtils.getExtension(name);
                        
                        name = request.getParameter("refer")+"-"+AccountingPeriod.getCurrentTimeStamp()+"."+ext;
                        item.write( new File(UPLOAD_DIRECTORY + File.separator + name));

                    }

                }

            

               //File uploaded successfully
               
               System.out.print("Hello World");

               

            } catch (Exception ex) {
                System.out.print("Hello World");


            }         

          

        }else{

            request.setAttribute("message","Sorry this Servlet only handles file upload request");

        }

     

        //request.getRequestDispatcher("/result.jsp").forward(request, response);

      

    }

    
}
