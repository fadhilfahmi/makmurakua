/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.makmurakua.controller.administration.OnM;

import com.lcsb.makmurakua.controller.PathModel;
import com.lcsb.makmurakua.dao.administration.om.UserDAO;
import com.lcsb.makmurakua.model.administration.om.UserAccount;
import com.lcsb.makmurakua.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_UserAccount", urlPatterns = {"/Path_UserAccount"})
public class Path_UserAccount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/om_user_add.jsp?id=" + moduleid;
                    break;
                case "view":
                    urlsend = "/om_user_view.jsp?id=" + moduleid;
                    break;
                case "addprocess": {
                    UserAccount ua = new UserAccount();
                    ua.setLevel(request.getParameter("level"));
                    ua.setPassword(request.getParameter("password"));
                    ua.setStaffId(request.getParameter("staff_id"));
                    ua.setStaffName(request.getParameter("staff_name"));
                    ua.setUser(request.getParameter("user"));
                    ua.setLevelId(Integer.parseInt(request.getParameter("level_id")));
                    UserDAO.saveData(log, ua);
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                }
                case "edit":
                    urlsend = "/om_user_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess": {
                    UserAccount ua = new UserAccount();
                    ua.setId(Integer.parseInt(request.getParameter("id")));
                    ua.setLevel(request.getParameter("level"));
                    ua.setPassword(request.getParameter("password"));
                    ua.setStaffId(request.getParameter("staff_id"));
                    ua.setStaffName(request.getParameter("staff_name"));
                    ua.setUser(request.getParameter("user"));
                    UserDAO.updateItem(log, ua);
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    //urlsend = "/cp_board_list.jsp?referenceno="+request.getParameter("referenceno");
                    break;
                }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    UserDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "accessright":
                    UserDAO.updateAccess(log, request.getParameter("userID"), request.getParameterValues("function"));
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "checkmodule":
                    UserDAO.checkModule(log, request.getParameter("user_id"), request.getParameter("modulechecked"));
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "uncheckmodule":
                    UserDAO.uncheckModule(log, request.getParameter("user_id"), request.getParameter("modulechecked"));
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "frommodal":
                    urlsend = "/user_dashboard_modal.jsp?moduleid=" + request.getParameter("tomodule") + "&status=" + request.getParameter("status") + "&count=" + request.getParameter("tomodule");
                    break;
                default:
                    break;
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_UserAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_UserAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
