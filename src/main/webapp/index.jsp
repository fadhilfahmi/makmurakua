<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="google-signin-client_id" content="505672834966-59mr186ec5dmilmf4m2l0spj694lqbsf.apps.googleusercontent.com"><!--local-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <style>
            
            
            .auth-head-icon {
                position: relative;
                height: 60px;
                width: 60px;
                display: inline-flex;
                align-items: center;
                justify-content: center;
                font-size: 30px;
                background-color: #fff;
                color: #5c6bc0;
                box-shadow: 0 5px 20px #d6dee4;
                border-radius: 50%;
                transform: translateY(-50%);
                z-index: 2;
            }
            .g-signin2{
                width: 100%;
            }

            .g-signin2 > div{
                margin: 0 auto;
            }
        </style>
    <script type="text/javascript" charset="utf-8">
            function onSignIn(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This isnull if the 'email' scope is not present.

                var auth2 = gapi.auth2.init();
                if (auth2.isSignedIn.get()) {
                    console.log('signedin');
                    window.location.href = 'SessionLogin?userID=' + profile.getId() + '&email=' + profile.getEmail() + '&name=' + profile.getName() + '&imageurl=' + profile.getImageUrl();
                    //console.log();

                }
            }


            function onFailure(error) {
                console.log(error);
            }

        </script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-1">Welcome!</h1>
                                        <h4 class="h5 text-gray-600 mb-4">Makmur Akua</h4>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="exampleInputPassword" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>
                                        <a href="index.html" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </a>
                                        <hr>
                                       
                                        <div class="g-signin2" data-onsuccess="onSignIn" data-theme="light" data-width="270" data-height="50" data-longtitle="true"  data-prompt="select_account"></div>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>